<?php
include ("../config/config.inc.php");
include ("../config/Database.class.php");
include ("../config/Application.class.php");

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
// staff details
$jsonProject 	= 	array();

$projectQry 	= 	"select a.ID,
					 a.projectName,
					 a.projectType,
					 a.technology,
					 b.staffName,                                         
					 c.projectStatus,
					 c.parkedReason,
					 a.dateFrom,
					 a.dateTo
					 from ".TABLE_PROJECT." a,".TABLE_STAFF." b,".TABLE_STATUS." c
					 where a.leader=b.ID
					 and a.ID=c.projectId";
$projectRes		=	mysql_query($projectQry);
$projectNum 	= 	mysql_num_rows($projectRes);

if($projectNum>0)
{		
	while($projectRow = mysql_fetch_assoc($projectRes))
	{
		$rows1['projectId'] 			= 	$projectId	=	$projectRow['ID'];
		$rows1['projectName'] 			= 	$projectRow['projectName'];
		$rows1['projectType'] 			= 	$projectRow['projectType'];
		$rows1['projectTechnology']		=	$projectRow['technology']; 
		$rows1['projectLeadName']		=	$projectRow['staffName']; 
		$rows1['projectStatus']			=	$projectRow['projectStatus']; 
		$rows1['parkedReason']			=	$projectRow['parkedReason']; 
		$rows1['startingDate']			=	$projectRow['dateFrom']; 
		$rows1['deliveryDate']			=	$projectRow['dateTo'];	
		
		$teamQry 	= 	"select b.staffName,b.ID,
                                         b.technology,
								b.designation							 
							 from ".TABLE_TEAM." a,".TABLE_STAFF." b
							 where a.staffId=b.ID
							 and a.projectId='$projectId'";
		$teamRes		=	mysql_query($teamQry);
		$teamList		=	array();
		while($teamRow = mysql_fetch_assoc($teamRes))
		{			
			$rows2['name'] 		        = 	$teamRow['staffName'];	
			$rows2['designation'] 		= 	$teamRow['designation'];	
                        $rows2['employeeId'] 		= 	$teamRow['ID'];	
			$rows2['technology'] 		= 	$teamRow['technology'];					
			array_push($teamList,$rows2);			
		}
		
		$rows1['teamList']				=	$teamList;
		array_push($jsonProject,$rows1);
	}
	$status = "Success";
	$message = "Project existed";
	
	//print_r($rows1);
}
else{
	$status = "Success";
	$message = "Project not existed";
}

$response = array();
$response['projectModels'] = 	$jsonProject;
$response['status'] 		= 	$status;
$response['message'] 		= 	$message;
echo json_encode($response);
?>