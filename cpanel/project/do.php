<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['linLogId'] ==''){
	header("location:../../logout.php");
} 

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
    // NEW SECTION
    case 'index':

        if (!$_REQUEST['projectName']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
			
            $projectName	= $App->capitalize($_REQUEST['projectName']);                      
			

            $existId = $db->existValuesId(TABLE_PROJECT, "projectName='$projectName'");
            if ($existId > 0) 
            {
                $_SESSION['msg'] = $App->sessionMsgCreate('error', "Details already exist. Please try again with correct details.");
                header("location:index.php");
            }
            else 
            {
                $data['projectName']	=	$projectName;
                $data['projectType']	=	$App->convert($_REQUEST['projectType']);
                $data['technology']		=	$App->convert($_REQUEST['technology']);
                $data['leader']			=	$App->convert($_REQUEST['leader']);
                $data['dateFrom']		=	$App->dbformat_date($_REQUEST['dateFrom']);
                $data['dateTo']			=	$App->dbformat_date($_REQUEST['dateTo']);
           
                $success = $db->query_insert(TABLE_PROJECT, $data);                
                for($p=1;$p<$_POST['teamCount'];$p++)
				{	
				   $data2['projectId']	=	$success;			 					   
				   $teamMember			=	"teamMember_".$p;
				   $staffId				=	$App->convert($_REQUEST[$teamMember]);
				   if($staffId!=0)
				   {
				   		$data2['staffId'] =	$staffId;
				   		$db->query_insert(TABLE_TEAM, $data2);
				   }
				   else{
				   	continue;
				   }				   
				}
				$data3['projectId']	=	$success;	
                $data3['projectStatus']	=	$App->convert($_REQUEST['projectStatus']);
                $db->query_insert(TABLE_STATUS, $data3);            
                $db->close();

                if ($success) 
                {
                    //$_SESSION['msg'] = " Details Added Successfully";
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");                
                } 
                else 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");                   
                }
                 header("location:index.php");
            }
        }
        break;
  // EDIT SECTION
    case 'edit':
    	$editId = $_REQUEST['editId'];
    	if (!$_REQUEST['service']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
			
            $service			= $App->capitalize($_REQUEST['service']);                      
			

            $existId = $db->existValuesId(TABLE_SERVICES, "service='$service' and ID!=$editId");
            if ($existId > 0) 
            {
                $_SESSION['msg'] = $App->sessionMsgCreate('error', "Details already exist. Please try again with correct details.");
                header("location:index.php");
            }
            else 
            {
            	$data['contentId']	=	$App->convert($_REQUEST['contentId']);
                $data['service']	=	$service;
                $data['english']	=	$App->convert($_REQUEST['english']);
                $data['arabic']		=	$App->convert($_REQUEST['arabic']);
              
                $success = $db->query_update(TABLE_SERVICES, $data , "ID=$editId" );
                $db->close();

                if ($success) 
                {
                    //$_SESSION['msg'] = " Details Added Successfully";
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details updated successfully");                
                } 
                else 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to update details. Please try again.");                   
                }
                 header("location:index.php");
            }
        }
        break; 
    // DELETE SECTION
    case 'delete':
        $deleteId = $_REQUEST['id'];//echo $deleteId;die;
        $success1 =$success2=$success3= 0;

        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db->connect();
  
			$success1= @mysql_query("DELETE FROM `".TABLE_PROJECT."` WHERE ID='{$deleteId}'");
			$success2= @mysql_query("DELETE FROM `".TABLE_TEAM."` WHERE projectId='{$deleteId}'");
			$success3= @mysql_query("DELETE FROM `".TABLE_STATUS."` WHERE projectId='{$deleteId}'");
			
        $db->close();
        if ($success1) {
            $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Deleted Successfully");	
        } else {
            $_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");
        }
        header("location:index.php");
        break;
}
?>