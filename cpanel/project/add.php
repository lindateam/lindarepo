<?php
require('../admin_header.php');
if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();
?>

<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=index" class="default_form" >
                <div class="bd_panel_head">
                    <h3>PROJECT REGISTRATION</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2">
                        	<div class="form_block">
                                <label>Project Name<span class="valid">*</span></label>
                                <input type="text" name="projectName" id="projectName">
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                        	<div class="form_block">
                                <label>Project Type<span class="valid">*</span></label>                
                                 <select id="projectType" name="projectType">
                                 	<option value="">Select</option>                               
								 	<option value="Web">Web</option>																<option value="Android">Android</option>
								 	<option value="Desktop">Desktop</option>
                                </select>
                            </div>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Technology<span class="valid">*</span></label>
                                <input type="text" name="technology" id="technology">
                            </div>
                         </div>
                      </div>
                      <div class="row">
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                            <label>Team lead<span class="valid">*</span></label>
                            <select id="leader" name="leader">
                                <option value="">Select</option>   
                            <?php
                                $selectAll = "select  * from ".TABLE_STAFF."";//echo $selectAll;
                                $result = $db->query($selectAll);
                                $result2 = $db->query($selectAll);
                                while($res=mysql_fetch_array($result))
                                {
                                	?>
									<option value="<?php echo $res['ID']; ?>"><?php echo $res['staffName']; ?></option>
									<?php
								}
                                ?>
                            </select>
                            </div>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Date From<span class="valid">*</span></label>
                                <input type="text" name="dateFrom" id="dateFrom" class="user_date">
                            </div>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Date To<span class="valid">*</span></label>
                                <input type="text" name="dateTo" id="dateTo"  class="user_date">
                            </div>
                         </div>
                     </div> 
                     <div class="row">
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                            <label>Project Status<span class="valid">*</span></label>
                            <select id="projectStatus" name="projectStatus">
                                <option value="">Select</option>   
                            	<option value="PRE-SALES">PRE-SALES</option>
                            	<option value="DEEP DIVE">DEEP DIVE</option>
                            	<option value="DEVELOPMENT">DEVELOPMENT</option>
                            	<option value="UAT">UAT</option>
                            	<option value="PRODUCTION">PRODUCTION</option>
                            	<option value="PARKED">PARKED</option>
                                <option value="SERVICE">SERVICE</option>
                            </select>
                            </div>
                         </div>
                      </div>
                    <div class="row"> 
                     <div class="col-lg-2 col-md-2 col-sm-2">
                        <div class="form_block">
                            <label>Team members<span class="valid">*</span></label>
                            <?php
                            $i=1;
                            while($res2=mysql_fetch_array($result2))
                                {
                                	?>
									<input type="checkbox" name="teamMember_<?php echo $i;?>" id="teamMember<?php echo $i; $i++;?>" value="<?php echo $res2['ID'] ?>"><?php echo $res2['staffName'] ?></br>
									<?php
								}
								?>
                        </div>
                     </div>  
                   </div>                         
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                        	<input type="hidden" name="teamCount" value="<?php echo $i; ?>">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>


