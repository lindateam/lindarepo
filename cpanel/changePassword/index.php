<?php
require('../admin_header.php');

if(@isset($_SESSION['msg'])){
	echo $_SESSION['msg'];
}
unset($_SESSION['msg']);

?>
<script>
function valid()
{

flag=false;

jPassword=document.getElementById('newPassword').value;
jConfirmPassword=document.getElementById('conPassword').value;		
	if(jConfirmPassword=="" || jConfirmPassword!=jPassword)
	{																			///for password
	document.getElementById('pwddiv').innerHTML="Password And Confirm Password are Not Equal";
	flag=true;
	}

if(flag==true)
	{
		return false;
	}

}

function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}

</script>
<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6">
		<div class="bd_panel bd_panel_default bd_panel_shadow">
			<form method="post" action="do.php?op=index" class="default_form" onsubmit=" return valid()">
				<div class="bd_panel_head">
					<h3>Change Password</h3>
				</div>
				<div class="bd_panel_body">
					<div class="panel_row">
						<div class="panel_col">
							<label>Existing User name</label>
						</div>
						<div class="panel_col">
							<input type="text" autocomplete="off" name="oldUserName" id="oldUserName" required="">
						</div>
					</div>
					<div class="panel_row">
						<div class="panel_col">
							<label>Existing Password</label>
						</div>
						<div class="panel_col">
							<input autocomplete="new-password" type="password" name="oldPassword" id="oldPassword" required="" >
						</div>
					</div>
					<div class="panel_row">
						<div class="panel_col">
							<label>New Password</label>
						</div>
						<div class="panel_col">
							<input type="password" name="newPassword" id="newPassword" required="">
						</div>
					</div>
					<div class="panel_row">
						<div class="panel_col">
							<label>Confirm Password</label>
						</div>
						<div class="panel_col">
							<input type="password" name="conPassword" id="conPassword" required="" onfocus="clearbox('pwddiv')">
							 <div id="pwddiv" class="valid" style="color:#FF9900;"></div>
						</div>
					</div>
				</div>
				<div class="bd_panel_footer">
					<div class="panel_row">
						<div class="panel_col_full">
							<input type="submit" name="form" value="SAVE">
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
