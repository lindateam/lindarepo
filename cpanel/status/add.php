<?php
require('../admin_header.php');
if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();
?>

<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=index" class="default_form" >
                <div class="bd_panel_head">
                    <h3>SERVICES</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 form_block_row">
                        	<div class="form_block">
                                <label>Contents<span class="valid">*</span></label>                
                                 <select id="contentId" name="contentId">
                                 <option value="">Select</option>
                                <?php
                                $selectAll = "select  * from ".TABLE_CONTENTS."";//echo $selectAll;
                                $result = $db->query($selectAll);
                                while($res=mysql_fetch_array($result))
                                {
                                	?>
									<option value="<?php echo $res['ID']; ?>"><?php echo $res['content']; ?></option>
									<?php
								}
                                ?>
                                </select>
                            </div>
                           <div class="form_block">
                                <label>Services<span class="valid">*</span></label>
                                <input type="text" name="service" id="service">
                            </div>
                            <div class="form_block">
                                <label>English<span class="valid">*</span></label>
                                <textarea name="english" id="english" class="mytextarea"></textarea>
                            </div>
                             <div class="form_block">
                                <label>Arabic<span class="valid">*</span></label>
                                <textarea name="arabic" id="arabic" class="mytextarea"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
