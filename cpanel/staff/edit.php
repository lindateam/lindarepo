<?php
require('../admin_header.php');
if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);

$editId		=	$_REQUEST['id'];
$editSelect = 	"select  * from ". TABLE_STAFF." where ID='$editId'";
$editResult = 	$db->query($editSelect);
$editRow 	= 	mysql_fetch_array($editResult);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=edit" class="default_form">
             <input type="hidden" name="editId" value="<?php echo $editId; ?>">
                <div class="bd_panel_head">
                    <h3>STAFF REGISTRATION</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Name <span class="valid">*</span></label>
                                <input type="text" name="name" id="name" value="<?php echo $editRow['staffName']; ?>" required="">
                         	</div>
                                
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Mobile <span class="valid">*</span></label>
                                <input type="text" name="mobile" id="mobile"  value="<?php echo $editRow['mobile']; ?>" required="">
                            </div>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Email</label>
                                <input type="text" name="email" id="email"  value="<?php echo $editRow['email']; ?>">
                            </div>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            
                            <div class="form_block">
                                <label>User Type</label>
                                 <input type="text" name="type" id="type" value="<?php echo $editRow['userType']; ?>">
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="Update">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
