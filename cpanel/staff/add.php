<?php
require('../admin_header.php');
if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
	function valid()
	{
		flag=false;
		jPassword	=	document.getElementById('password').value;
		jConfirm	=	document.getElementById('cpassword').value;
		if(jPassword!=jConfirm)
		{
			document.getElementById('passdiv').innerHTML="Password is Mismatches with Confirm Password";
			flag=true;
		}
		if(flag==true)
		{
		return false;
		}
	}
	//clear the validation msg
	function clearbox(Element_id)
	{
	document.getElementById(Element_id).innerHTML="";
	}
</script>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=index" class="default_form" onsubmit="return valid()">
                <div class="bd_panel_head">
                    <h3>STAFF REGISTRATION</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Name <span class="valid">*</span></label>
                                <input type="text" name="name" id="name" required="">
                            </div>
                         </div>
                         
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Designation</label>
                                <input type="text" name="designation" id="designation" >
                            </div>
                            
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>UserName <span class="valid">*</span></label>
                                <input type="text" name="userName" id="userName" required="">
                            </div>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2">                   
                            <div class="form_block">
                                <label>Password <span class="valid">*</span></label>
                                <input type="password" name="password" id="password" required="">
                            </div>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Re enter Password <span class="valid">*</span></label>
                                <input type="password" name="cpassword" id="cpassword" required="" onfocus="clearbox('passdiv')">
                                <div id="passdiv" class="valid" style="color:#FF6600;"></div>
                            </div>
                         </div>
                   </div>      
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
                
                
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
