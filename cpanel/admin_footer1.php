</div>
</div>
<!-- Popups -->
<div class="pop_overlay">
    <img src="../../images/aj_loader.gif" alt="">
</div>
<!-- Error -->
<div id="error_pop" class="modal fade session_modal_error session_modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Error</h4></div>
            <div class="modal-body"><p></p></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
  tinymce.init({
    selector: '.mytextarea'
  });
  </script>