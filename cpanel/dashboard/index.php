<?php
require('../admin_header.php');

if($_SESSION['qLogId'] ==''){
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
    unset($_SESSION['msg']);
}


?>

<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4">
        <h3 class="dash_section_heads">Account Report</h3>
        <a class="info-box" href="../account_reports/profit_report.php">
            <span class="info-box-icon bg-aqua"><i class="ion ion-android-checkmark-circle"></i></span>

            <div class="info-box-content">
                <!--<span class="info-box-text">Profit</span>
                <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                <span class="info-box-number">Profit</span>
            </div>
            <!-- /.info-box-content -->
        </a>
        <a class="info-box" href="../account_reports/payment_report.php">
            <span class="info-box-icon bg-red"><i class="ion ion-log-out"></i></span>
            <div class="info-box-content">
                <!--<span class="info-box-text">Payment</span>
                <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                <span class="info-box-number">Payment</span>
            </div>
            <!-- /.info-box-content -->
        </a>
        <a class="info-box" href="../account_reports/receipt_report.php">
            <span class="info-box-icon bg-green"><i class="ion ion-log-in"></i></span>
            <div class="info-box-content">
                <!--<span class="info-box-text">Receipt</span>
                <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                <span class="info-box-number">Receipt</span>
            </div>
            <!-- /.info-box-content -->
        </a>
        <a class="info-box" href="../account_reports/payable_due.php">
            <span class="info-box-icon bg-yellow"><i class="ion ion-arrow-return-right"></i></span>
            <div class="info-box-content">
                <!--<span class="info-box-text">Payment Due</span>
                <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                <span class="info-box-number">Payment Due</span>
            </div>
            <!-- /.info-box-content -->
        </a>
        <a class="info-box" href="../account_reports/receivable_due.php">
            <span class="info-box-icon bg-purple"><i class="ion ion-arrow-return-left"></i></span>
            <div class="info-box-content">
                <!--<span class="info-box-text">Receipt Due</span>
                <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                <span class="info-box-number">Receipt Due</span>
            </div>
            <!-- /.info-box-content -->
        </a>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4">
        <h3 class="dash_section_heads">Sale</h3>
        <a class="info-box" href="../invoice_report/airline.php">
            <span class="info-box-icon bg-aqua"><i class="ion ion-android-plane"></i></span>

            <div class="info-box-content">
                <!--<span class="info-box-text">Airline</span>
                <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                <span class="info-box-number">Airline</span>
            </div>
            <!-- /.info-box-content -->
        </a>
        <a class="info-box" href="../invoice_report/visa.php">
            <span class="info-box-icon bg-red"><i class="ion ion-document-text"></i></span>
            <div class="info-box-content">
                <!--<span class="info-box-text">Visa</span>
                <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                <span class="info-box-number">Visa</span>
            </div>
            <!-- /.info-box-content -->
        </a>
        <a class="info-box" href="../invoice_report/hotel.php">
            <span class="info-box-icon bg-green info-box-icon-fa"><i class="fa fa-building"></i></span>
            <div class="info-box-content">
                <!--<span class="info-box-text">Hotel</span>
                <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                <span class="info-box-number">Hotel</span>
            </div>
            <!-- /.info-box-content -->
        </a>
        <a class="info-box" href="../invoice_report/transport.php">
            <span class="info-box-icon bg-yellow"><i class="ion ion-android-car"></i></span>
            <div class="info-box-content">
                <!--<span class="info-box-text">Transport</span>
                <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 10071204</span>-->
                <span class="info-box-number">Transport</span>
            </div>
            <!-- /.info-box-content -->
        </a>
        <a class="info-box" href="../invoice_report/misc.php">
            <span class="info-box-icon bg-purple"><i class="ion ion-grid"></i></span>
            <div class="info-box-content">
                <!--<span class="info-box-text">miscellaneous</span>
                <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 10071204</span>-->
                <span class="info-box-number">Miscellaneous</span>
            </div>
            <!-- /.info-box-content -->
        </a>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4">
        <h3 class="dash_section_heads">Purchase</h3>
        <a class="info-box" href="../purchase/airline.php">
            <span class="info-box-icon bg-aqua"><i class="ion ion-android-plane"></i></span>

            <div class="info-box-content">
                <!--<span class="info-box-text">Airline</span>
                <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                <span class="info-box-number">Airline</span>
            </div>
            <!-- /.info-box-content -->
        </a>
        <a class="info-box" href="../purchase/visa.php">
            <span class="info-box-icon bg-red"><i class="ion ion-document-text"></i></span>
            <div class="info-box-content">
                <!--<span class="info-box-text">Visa</span>
                <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                <span class="info-box-number">Visa</span>
            </div>
            <!-- /.info-box-content -->
        </a>
        <a class="info-box" href="../purchase/hotel.php">
            <span class="info-box-icon bg-green info-box-icon-fa"><i class="fa fa-building"></i></span>
            <div class="info-box-content">
                <!--<span class="info-box-text">Hotel</span>
                <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                <span class="info-box-number">Hotel</span>
            </div>
            <!-- /.info-box-content -->
        </a>
        <a class="info-box" href="../purchase/transport.php">
            <span class="info-box-icon bg-yellow"><i class="ion ion-android-car"></i></span>
            <div class="info-box-content">
                <!--<span class="info-box-text">Transport</span>
                <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 10071204</span>-->
                <span class="info-box-number">Transport</span>
            </div>
            <!-- /.info-box-content -->
        </a>
        <a class="info-box" href="../purchase/misc.php">
            <span class="info-box-icon bg-purple"><i class="ion ion-grid"></i></span>
            <div class="info-box-content">
                <!--<span class="info-box-text">miscellaneous</span>
                <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 10071204</span>-->
                <span class="info-box-number">Miscellaneous</span>
            </div>
            <!-- /.info-box-content -->
        </a>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>