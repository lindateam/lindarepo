<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['linLogId'] ==''){
	header("location:../../logout.php");
} 
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Linda</title>
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap-theme.css">
	<link rel="stylesheet" type="text/css" href="../../css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../../css/perfect-scrollbar.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.structure.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.theme.css">
	<link rel="stylesheet/less" type="text/css" href="../../css/timepicker.less">


	<link rel="stylesheet" type="text/css" href="../../css/style.css">

	<script src="../../js/jquery-2.2.1.min.js"></script>
	<script src="../../js/jquery-ui.min.js"></script>
	<script src="../../js/bootstrap.min.js"></script>
	<script src="../../js/perfect-scrollbar.jquery.js"></script>
	<script src="../../js/bootstrap-timepicker.js"></script>
	<script src="../../js/script.js"></script>
	<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>	
</head>
<body>
<div class="side_panel opened">
	<div class="app_logo">
		<img src="../../images/logo.jpg" alt="" />
	</div>
	<div class="user_det">

	</div>
	<div class="side_nav_wrapper">
			<ul class="side_nav">
				<li>
					<a href="../dashboard/">
						<span class="side_nav_ico"><i class="ion ion-ios-speedometer"></i></span>
						<span class="side_nav_text">Home</span>
					</a>
				</li>
				<!--<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-compose"></i></span>
						<span class="side_nav_text">Contents</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						<li>
							<a href="../ac_group">
								<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
								<span class="side_nav_text">Project Consultancy</span>
							</a>
						</li>
						<li>
							<a href="../ac_group">
								<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
								<span class="side_nav_text">Man Power Service</span>
							</a>
						</li>
						<li>
							<a href="../ac_group">
								<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
								<span class="side_nav_text">Management Service</span>
							</a>
						</li>
						<li>
							<a href="../ac_group">
								<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
								<span class="side_nav_text">Civil &amp; Interior Decor</span>
							</a>
						</li>
						<li>
							<a href="../ac_group">
								<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
								<span class="side_nav_text">Brands &amp; Franchises</span>
							</a>
						</li>
					</ul>
				</li>-->
				<li>
					<a href="../staff/">
						<span class="side_nav_ico"><i class="ion ion-ios-speedometer"></i></span>
						<span class="side_nav_text">Staff</span>
					</a>
				</li>
				<li>
					<a href="../project/">
						<span class="side_nav_ico"><i class="ion ion-ios-speedometer"></i></span>
						<span class="side_nav_text">Project</span>
					</a>
				</li>
				<li>
					<a href="../status/">
						<span class="side_nav_ico"><i class="ion ion-images"></i></span>
						<span class="side_nav_text">Status</span>
					</a>
				</li>				
			</ul>
	</div>
</div>
<div class="wrapper give_way">
	<div class="header">
		<div class="container-fluid">
			<div class="nav_toggler">
				<span class="ion ion-android-menu"></span>
			</div>
			<div class="header_user_controls">
				<ul class="header_main_nav">
					<li class="has_child">
						<a href="#">
							<span class="nav_text">Settings</span>

							<span class="drop_ico"><i class="ion ion-ios-arrow-down"></i></span>
							<div class="bd_clear"></div>
						</a>
						<ul class="header_sub_nav">
							<li><a href="../changePassword">Change Password</a></li>
							<li><a href="../../logout.php">Log Out</a></li>
						</ul>
					</li>
				</ul>
			</div>
			<div class="bd_clear"></div>
		</div>
	</div>
	<div class="container-fluid">

