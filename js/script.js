/*jslint browser: true*/
/*global $, jQuery, alert*/
$(window).load(function () {
    'use strict';
    // Session Message
    $('#session_modal').modal('show');
});
$(function () {
    'use strict';
    // Global functions
    // Error message
    function triggerError(type, msg) {
        if (type == "error") {
            $('#error_pop .modal-body p').text(msg);
        }
        $('#error_pop').modal('show');
    }

    // Passenger table count
    function getPassengerRowCount() {
        var passengerRowCount = $('#passenger_table > tbody > tr').length;
        $('#passenger_row_count').val(passengerRowCount);
    }

    function getVisaPassengerRowCount() {
        var passengerRowCount = $('#visa_passenger_table > tbody > tr').length;
        $('#visa_passenger_row_count').val(passengerRowCount);
    }

    // Hotel table count
    function getHotelRowCount() {
        var hotelRowCount = $('#hotel_room_table > tbody > tr').length;
        $('#hotel_row_count').val(hotelRowCount);
    }

    // Invoice package Passenger Row Count
    function getInvPackParrRowCount() {
        $('#inv_pack_passenger_count').val($('#inv_package_paasenger_table > tbody > tr').length);
    }

    // Net amount calculation in airline invoice
    function netAirlineAmt() {
        var invFormTarget = $('#invoice_airline'),
            invAirlineTable = $('#passenger_table'),
            invAirlineRowCount = invAirlineTable.find('tbody > tr').length,
            totalPassAmt = 0,
            mainTds = 0,
            mainProCharge = 0,
            mainOtherCharge = 0,
            mainTax = 0,
            mainDiscount = 0,
            taxPerc = 0,
            invAirlineNetAmt = 0;
        if ((invFormTarget.find('input[name="tds"]').val() != "") && ($.isNumeric(invFormTarget.find('input[name="tds"]').val()))) {
            mainTds = parseFloat(invFormTarget.find('input[name="tds"]').val());
        }
        if ((invFormTarget.find('input[name="mainProcCharge"]').val() != "") && ($.isNumeric(invFormTarget.find('input[name="mainProcCharge"]').val()))) {
            mainProCharge = parseFloat(invFormTarget.find('input[name="mainProcCharge"]').val());
        }
        if ((invFormTarget.find('input[name="mainOtherCharge"]').val() != "") && ($.isNumeric(invFormTarget.find('input[name="mainOtherCharge"]').val()))) {
            mainOtherCharge = parseFloat(invFormTarget.find('input[name="mainOtherCharge"]').val());
        }
        if (invFormTarget.find('input[name="mainTax"]').val() != "") {
            mainTax = invFormTarget.find('input[name="mainTax"]').val();
        }
        if ((invFormTarget.find('input[name="discount"]').val() != "") && ($.isNumeric(invFormTarget.find('input[name="discount"]').val()))) {
            mainDiscount = parseFloat(invFormTarget.find('input[name="discount"]').val());
        }
        if (invAirlineRowCount > 0) {
            $(invAirlineTable.find('tbody > tr')).each(function () {
                var curRow = $(this),
                    curRowIndex = curRow.index(),
                    airRowCount = parseInt(curRow.find('td:nth-child(2) input[name="pass' + (curRowIndex + 1) + '_airlineRowCount"]').val()),
                    totalRowValue = 0,
                    k;
                for (k = 1; k <= airRowCount; k++) {
                    var indFare = 0,
                        indYq = 0,
                        indProcharge = 0,
                        indTaxText = curRow.find('td:nth-child(2) input[data-field_name="tax_' + k + '"]').val(),
                        totalAirValue;
                    if ((curRow.find('td:nth-child(2) input[data-field_name="fare_' + k + '"]').val() != "") && ($.isNumeric(curRow.find('td:nth-child(2) input[data-field_name="fare_' + k + '"]').val()))) {
                        indFare = parseFloat(curRow.find('td:nth-child(2) input[data-field_name="fare_' + k + '"]').val());
                    }
                    if ((curRow.find('td:nth-child(2) input[data-field_name="yq_' + k + '"]').val() != "") && ($.isNumeric(curRow.find('td:nth-child(2) input[data-field_name="yq_' + k + '"]').val()))) {
                        indYq = parseFloat(curRow.find('td:nth-child(2) input[data-field_name="yq_' + k + '"]').val());
                    }
                    if ((curRow.find('td:nth-child(2) input[data-field_name="procCharge_' + k + '"]').val() != "") && ($.isNumeric(curRow.find('td:nth-child(2) input[data-field_name="procCharge_' + k + '"]').val()))) {
                        indProcharge = parseFloat(curRow.find('td:nth-child(2) input[data-field_name="procCharge_' + k + '"]').val());
                    }
                    if (indTaxText && indTaxText.toString().indexOf("%") != -1) {
                        var indTaxTextLength = indTaxText.toString().length,
                            airTaxPerc = indTaxText.toString().substr(0, (indTaxTextLength - 1));
                        totalAirValue = (indFare + indYq + indProcharge) + ((parseFloat(airTaxPerc) / 100) * (indFare + indYq + indProcharge));
                    } else if (indTaxText && indTaxText.toString().indexOf("%") == -1) {
                        totalAirValue = indFare + indYq + indProcharge + parseFloat(indTaxText);
                    } else if (!indTaxText) {
                        totalAirValue = indFare + indYq + indProcharge
                    }
                    totalRowValue = totalRowValue + totalAirValue;
                }
                totalPassAmt = totalPassAmt + totalRowValue;
            });
        }
        if (mainTax.toString().indexOf('%') != -1) {
            var mainTaxLength = mainTax.toString().length,
                mainTax = mainTax.toString().substr(0, (mainTaxLength - 1));
            invAirlineNetAmt = ((totalPassAmt + mainTds + mainProCharge + mainOtherCharge) + (((totalPassAmt + mainTds + mainProCharge + mainOtherCharge) / 100) * parseFloat(mainTax))) - mainDiscount;
        } else {
            invAirlineNetAmt = (totalPassAmt + mainTds + mainProCharge + mainOtherCharge + parseFloat(mainTax) - mainDiscount).toFixed(2);
        }
        invFormTarget.find('input[name="netAmount"]').val(parseFloat(invAirlineNetAmt).toFixed(2));
    }

    // Visa net amount
    function netVisaAmount() {
        var invFormTarget = $('#invoice_visa'),
            invVisaTable = $('#visa_passenger_table'),
            invVisaRowCount = invVisaTable.find('tbody > tr').length,
            totalPassAmount = 0,
            mainCharge = 0,
            mainDiscount = 0,
            visaNetAmount = 0;
        if ((invFormTarget.find('input[name="total"]').val() != "") && (!isNaN(parseFloat(invFormTarget.find('input[name="total"]').val())))) {
            mainCharge = parseFloat(invFormTarget.find('input[name="total"]').val());
        }
        if ((invFormTarget.find('input[name="discount"]').val() != "") && (!isNaN(parseFloat(invFormTarget.find('input[name="discount"]').val())))) {
            mainDiscount = parseFloat(invFormTarget.find('input[name="discount"]').val());
        }
        if (invVisaRowCount > 0) {
            var i;
            for (i = 1; i <= invVisaRowCount; i++) {
                var curVisaTableRow = invVisaTable.find('tbody > tr:nth-child(' + i + ')'),
                    totalRowValue = parseFloat(curVisaTableRow.find('input[name="pass' + i + '_amount"]').val());
                totalPassAmount = totalPassAmount + totalRowValue;
            }
        }
        visaNetAmount = totalPassAmount + mainCharge - mainDiscount;
        $('#invoice_visa input[name="netAmount"]').val(parseFloat(visaNetAmount).toFixed(2));
    }

    /*$('#invoice_visa input[data-visa_net="visa_net_trigger"]').each(function () {
     $(this).keyup(function () {
     netVisaAmount();
     });
     });*/

    // Visa pop up total amount calculation
    function visaPopupAmtUpdator() {
        var popTarget = $('#visa_multi_det_add'),
            visaFee = 0,
            vfs = 0,
            ddCharge = 0,
            servCharge = 0,
            couriorCharge = 0,
            otherCharge = 0,
            totalCharge = 0;
        if ((popTarget.find('input[name="visaFee"]').val() != "") && (!isNaN(parseFloat(popTarget.find('input[name="visaFee"]').val()))) && (parseFloat(popTarget.find('input[name="visaFee"]').val()) > 0)) {
            visaFee = parseFloat(popTarget.find('input[name="visaFee"]').val());
        }
        if ((popTarget.find('input[name="vfs"]').val() != "") && (!isNaN(parseFloat(popTarget.find('input[name="vfs"]').val()))) && (parseFloat(popTarget.find('input[name="vfs"]').val()) > 0)) {
            vfs = parseFloat(popTarget.find('input[name="vfs"]').val());
        }
        if ((popTarget.find('input[name="ddCharge"]').val() != "") && (!isNaN(parseFloat(popTarget.find('input[name="ddCharge"]').val()))) && (parseFloat(popTarget.find('input[name="ddCharge"]').val()) > 0)) {
            ddCharge = parseFloat(popTarget.find('input[name="ddCharge"]').val());
        }
        if ((popTarget.find('input[name="serviceCharge"]').val() != "") && (!isNaN(parseFloat(popTarget.find('input[name="serviceCharge"]').val()))) && (parseFloat(popTarget.find('input[name="serviceCharge"]').val()) > 0)) {
            servCharge = parseFloat(popTarget.find('input[name="serviceCharge"]').val());
        }
        if ((popTarget.find('input[name="courierCharge"]').val() != "") && (!isNaN(parseFloat(popTarget.find('input[name="courierCharge"]').val()))) && (parseFloat(popTarget.find('input[name="courierCharge"]').val()) > 0)) {
            couriorCharge = parseFloat(popTarget.find('input[name="courierCharge"]').val());
        }
        if ((popTarget.find('input[name="otherCharge"]').val() != "") && (!isNaN(parseFloat(popTarget.find('input[name="otherCharge"]').val()))) && (parseFloat(popTarget.find('input[name="otherCharge"]').val()) > 0)) {
            otherCharge = parseFloat(popTarget.find('input[name="otherCharge"]').val());
        }
        totalCharge = visaFee + vfs + ddCharge + servCharge + couriorCharge + otherCharge;
        popTarget.find('input[name="amount"]').val(totalCharge.toFixed(2));
    }

    // Miscellaneous amount calculations
    function netMiscellaneousAmount() {
        var miscForm = $('#misc_form'),
            amount = 0,
            otherCharges = 0,
            tds = 0,
            proCharge = 0,
            discount = 0,
            taxText = miscForm.find('input[name="tax"]').val(),
            tax = 0,
            netAmount = 0;
        if ((miscForm.find('input[name="basicAmount"]').val() != "") && (!isNaN(miscForm.find('input[name="basicAmount"]').val()))) {
            amount = parseFloat(miscForm.find('input[name="basicAmount"]').val());
        }
        if ((miscForm.find('input[name="otherCharge"]').val() != "") && (!isNaN(miscForm.find('input[name="otherCharge"]').val()))) {
            otherCharges = parseFloat(miscForm.find('input[name="otherCharge"]').val());
        }
        if ((miscForm.find('input[name="tds"]').val() != "") && (!isNaN(miscForm.find('input[name="tds"]').val()))) {
            tds = parseFloat(miscForm.find('input[name="tds"]').val());
        }
        if ((miscForm.find('input[name="procCharge"]').val() != "") && (!isNaN(miscForm.find('input[name="procCharge"]').val()))) {
            proCharge = parseFloat(miscForm.find('input[name="procCharge"]').val());
        }
        if ((miscForm.find('input[name="discount"]').val() != "") && (!isNaN(miscForm.find('input[name="discount"]').val()))) {
            discount = parseFloat(miscForm.find('input[name="discount"]').val());
        }
        if (taxText.toString().includes('%')) {
            var taxTextLength = taxText.toString().length,
                taxText = taxText.toString().substr(0, (taxTextLength - 1)),
                totalAmt = amount + otherCharges + tds + proCharge;
            tax = (parseInt(taxText) / 100) * totalAmt;
        } else if ((!isNaN(parseFloat(taxText))) && (parseFloat(taxText) > 0)) {
            tax = parseFloat(taxText);
            //console.log(tax);
        }
        netAmount = amount + otherCharges + tds + tax + proCharge - discount;
        miscForm.find('input[name="netAmount"]').val(netAmount.toFixed(2));
    }

    // Hotel Invoice net amount calculation
    function netHotelAmt () {
        var hotelFormTarget = $('#invoice_hotel'),
            hotelRoomTable = $('#hotel_room_table'),
            totalTableAmount = 0,
            mainTds = 0,
            mainOtherCharge = 0,
            mainDiscount = 0,
            mainTaxText = hotelFormTarget.find('input[name="tax"]').val(),
            mainTax = 0,
            withoutTax = 0,
            netAmount = 0;
        if ((hotelFormTarget.find('input[name="tds"]').val() != "") && (!isNaN(parseFloat(hotelFormTarget.find('input[name="tds"]').val())))) {
            mainTds = parseFloat(hotelFormTarget.find('input[name="tds"]').val());
        }
        if ((hotelFormTarget.find('input[name="otherCharge"]').val() != "") && (!isNaN(parseFloat(hotelFormTarget.find('input[name="otherCharge"]').val())))) {
            mainOtherCharge = parseFloat(hotelFormTarget.find('input[name="otherCharge"]').val());
        }
        if ((hotelFormTarget.find('input[name="discount"]').val() != "") && (!isNaN(parseFloat(hotelFormTarget.find('input[name="discount"]').val())))) {
            mainDiscount = parseFloat(hotelFormTarget.find('input[name="discount"]').val());
        }
        // getting the total of table
        hotelRoomTable.find('tbody > tr').each(function () {
            var curTr = $(this),
                roomTotal = 0;
            if ((curTr.find('input[data-hotel_net="hotel_net_trigger"]').val() != "") && (!isNaN(parseInt(curTr.find('input[data-hotel_net="hotel_net_trigger"]').val())))) {
                roomTotal = parseFloat(curTr.find('input[data-hotel_net="hotel_net_trigger"]').val());
            }
            totalTableAmount = totalTableAmount + roomTotal;
        });
        withoutTax = mainTds + mainOtherCharge + totalTableAmount;
        if (mainTaxText.indexOf('%') != -1 && mainTaxText.length > 1) {
            var mainTaxTextLength = mainTaxText.length,
                mainTaxPerc = parseFloat(mainTaxText.toString().substr(0, (mainTaxTextLength - 1)));
            netAmount = withoutTax + ((withoutTax / 100) * (parseFloat(mainTaxPerc))) - mainDiscount;
        } else if (mainTaxText.indexOf('%') == -1 && mainTaxText != "" && !isNaN(parseFloat(mainTaxText))) {
            netAmount = withoutTax + parseFloat(mainTaxText) - mainDiscount;
        } else if (mainTaxText.length == 0) {
            netAmount = withoutTax - mainDiscount;
        }
        hotelFormTarget.find('input[name="netAmount"]').val(netAmount);
    }
    function netHotelTableAmt () {
        var hotelFormTarget = $('#invoice_hotel'),
            hotelRoomTable = $('#hotel_room_table'),
            noOfDays = 1;
        if (hotelFormTarget.find('#noOfNight').val() != "" && !isNaN(parseInt(hotelFormTarget.find('#noOfNight').val()))) {
            noOfDays = parseInt(hotelFormTarget.find('#noOfNight').val());
        } else {
            noOfDays = 1;
        }
        hotelRoomTable.find('tbody > tr').each(function () {
            var curTr = $(this),
                noOfRooms = 1,
                roomRate = 0,
                taxAmount = 0,
                roomTotal = 0;
            if (curTr.find('input[name="noOfRooms_'+ (1 + curTr.index()) +'"]').val() != "" && !isNaN(parseInt(curTr.find('input[name="noOfRooms_'+ (1 + curTr.index()) +'"]').val()))) {
                noOfRooms = parseInt(curTr.find('input[name="noOfRooms_'+ (1 + curTr.index()) +'"]').val());
            }
            if (curTr.find('input[name="rate_'+ (1 + curTr.index()) +'"]').val() != "" && !isNaN(parseFloat(curTr.find('input[name="rate_'+ (1 + curTr.index()) +'"]').val()))) {
                roomRate = parseFloat(curTr.find('input[name="rate_'+ (1 + curTr.index()) +'"]').val());
            }
            if (curTr.find('input[name="tax_'+ (1 + curTr.index()) +'"]').val() != "" && !isNaN(parseFloat(curTr.find('input[name="tax_'+ (1 + curTr.index()) +'"]').val()))) {
                taxAmount = parseFloat(curTr.find('input[name="tax_'+ (1 + curTr.index()) +'"]').val());
            }
            roomTotal = ((roomRate + taxAmount) * noOfRooms) * noOfDays;
            curTr.find('input[name="totalAmount_'+ (1 + curTr.index()) +'"]').val(roomTotal);
        });
    }

    // Age based rate change in invoice package
    function ageBasedRateTrigger(age) {
        console.log("sdf");
        var curForm = $('#inv_package_passenger_pop'),
            curAirTarget = curForm.find('.inv_pack_dynamic_airline'),
            curHotelTarget = curForm.find('.inv_pack_dynamic_hotel'),
            curTransportationTarget = curForm.find('.inv_pack_dynamic_transportation');
        if (age < 2) {
            curAirTarget.find('input[name="airInvoice"]').val(curAirTarget.find('input[name="airInvoice"]').attr('data-infant_total'));
            curAirTarget.find('input[name="airPurchase"]').val(curAirTarget.find('input[name="airPurchase"]').attr('data-infant_total'));
            //curHotelTarget.find('input[type="checkbox"]').removeAttr('disabled');
            //curHotelTarget.find('select[name="roomType"]').attr('disabled', 'disabled');
            /*if (curHotelTarget.find('input[type="checkbox"]').prop('checked') == true) {
             curHotelTarget.find('input[name="hotelInvoice"]').val(curHotelTarget.find('input[name="hotelInvoice"]').attr('data-child_wb_total'));
             curHotelTarget.find('input[name="hotPurchase"]').val(curHotelTarget.find('input[name="hotelPurchase"]').attr('data-child_wb_total'));
             } else {
             curHotelTarget.find('input[name="hotInvoice"]').val(curHotelTarget.find('input[name="hotInvoice"]').attr('data-child_wob_total'));
             curHotelTarget.find('input[name="hotPurchase"]').val(curHotelTarget.find('input[name="hotPurchase"]').attr('data-child_wob_total'));
             }*/
            /*curHotelTarget.find('input[name="hotInvoice"]').val(curHotelTarget.find('select[name="roomType"] option:selected').attr('data-room_rate'));
             curHotelTarget.find('input[name="hotPurchase"]').val(curHotelTarget.find('select[name="roomType"] option:selected').attr('data-room_pur_total'));*/
            curHotelTarget.find('input[name="hotInvoice"]').val(0);
            curHotelTarget.find('input[name="hotPurchase"]').val(0);
            curHotelTarget.find('select[name="roomType"] option:first').not(curHotelTarget.find('select[name="roomType"] option:first')).removeAttr('selected');
            curHotelTarget.find('select[name="roomType"] option:first').attr('selected', 'selected');
            curHotelTarget.find('select[name="roomType"]').attr('disabled', 'disabled');

            curTransportationTarget.find('input[name="seatStatus"]').prop('checked', false).attr('disabled', 'disabled');
            curTransportationTarget.find('input[name="transInvoice"]').val(0);
            curTransportationTarget.find('input[name="transPurchase"]').val(0);

        } else if (age >= 2 && age < 12) {
            curAirTarget.find('input[name="airInvoice"]').val(curAirTarget.find('input[name="airInvoice"]').attr('data-child_total'));
            curAirTarget.find('input[name="airPurchase"]').val(curAirTarget.find('input[name="airPurchase"]').attr('data-child_total'));
            //curHotelTarget.find('input[type="checkbox"]').removeAttr('disabled');
            //curHotelTarget.find('select[name="roomType"]').attr('disabled', 'disabled');
            /*if (curHotelTarget.find('input[type="checkbox"]').prop('checked') == true) {
             curHotelTarget.find('input[name="hotInvoice"]').val(curHotelTarget.find('input[name="hotInvoice"]').attr('data-child_wb_total'));
             curHotelTarget.find('input[name="hotPurchase"]').val(curHotelTarget.find('input[name="hotPurchase"]').attr('data-child_wb_total'));
             } else {
             curHotelTarget.find('input[name="hotInvoice"]').val(curHotelTarget.find('input[name="hotInvoice"]').attr('data-child_wob_total'));
             curHotelTarget.find('input[name="hotPurchase"]').val(curHotelTarget.find('input[name="hotPurchase"]').attr('data-child_wob_total'));
             }*/
            curHotelTarget.find('input[name="hotInvoice"]').val(curHotelTarget.find('select[name="roomType"] option:selected').attr('data-room_rate'));
            curHotelTarget.find('input[name="hotPurchase"]').val(curHotelTarget.find('select[name="roomType"] option:selected').attr('data-room_pur_total'));
        } else if (age >= 12) {
            curAirTarget.find('input[name="airInvoice"]').val(curAirTarget.find('input[name="airInvoice"]').attr('data-adult_total'));
            curAirTarget.find('input[name="airPurchase"]').val(curAirTarget.find('input[name="airPurchase"]').attr('data-adult_total'));
            curHotelTarget.find('input[name="hotInvoice"]').val(curHotelTarget.find('input[name="hotInvoice"]').attr('data-adult_total'));
            curHotelTarget.find('input[name="hotPurchase"]').val(curHotelTarget.find('input[name="hotPurchase"]').attr('data-adult_total'));
            curHotelTarget.find('input[type="checkbox"]').attr('disabled', 'disabled');
            curHotelTarget.find('select[name="roomType"]').removeAttr('disabled');
            curHotelTarget.find('input[name="hotInvoice"]').val(curHotelTarget.find('select[name="roomType"] option:selected').attr('data-room_rate'));
            curHotelTarget.find('input[name="hotPurchase"]').val(curHotelTarget.find('select[name="roomType"] option:selected').attr('data-room_pur_total'));
        }
    }

    // Live calculation of cash payment
    function netCahsPaymentAmt() {
        var curForm = $('#cash_payment'),
            curTable = curForm.find('table#tbl_cash_payment'),
            curNetTarget = curForm.find('input#amount'),
            netAmount = 0;
        if (curTable.find('tbody > tr').length > 0) {
            curTable.find('tbody > tr').each(function () {
                var paying = 0,
                    discount = 0;
                if (($(this).find('input[name="payingAmount_' + ($(this).index() + 1) + '"]').val()) &&  ($.isNumeric($(this).find('input[name="payingAmount_' + ($(this).index() + 1) + '"]').val()))) {
                    paying = parseFloat($(this).find('input[name="payingAmount_' + ($(this).index() + 1) + '"]').val());
                }
                if (($(this).find('input[name="discount_' + ($(this).index() + 1) + '"]').val()) &&  ($.isNumeric($(this).find('input[name="discount_' + ($(this).index() + 1) + '"]').val()))) {
                    discount = parseFloat($(this).find('input[name="discount_' + ($(this).index() + 1) + '"]').val());
                }
                netAmount = netAmount + paying - discount;
                curNetTarget.val(netAmount.toFixed(2));
            });
        } else {
            curNetTarget.val(0.00);
        }
    }

    // Live calculation of cash receipt
    function netCahsReceiptAmt() {
        var curForm = $('#cash_receipt'),
            curTable = curForm.find('table#tbl_cash_receipt'),
            curNetTarget = curForm.find('input#amount'),
            netAmount = 0;


        if (curTable.find('tbody > tr').length > 0) {
            curTable.find('tbody > tr').each(function () {
                var paying = 0,
                    discount = 0;
                if (($(this).find('input[name="payingAmount_' + ($(this).index() + 1) + '"]').val().length > 0) && ($.isNumeric($(this).find('input[name="payingAmount_' + ($(this).index() + 1) + '"]').val()))) {
                    paying = parseFloat($(this).find('input[name="payingAmount_' + ($(this).index() + 1) + '"]').val());
                }
                if (($(this).find('input[name="discount_' + ($(this).index() + 1) + '"]').val().length > 0) && ($.isNumeric($(this).find('input[name="discount_' + ($(this).index() + 1) + '"]').val()))) {
                    discount = parseFloat($(this).find('input[name="discount_' + ($(this).index() + 1) + '"]').val());
                }
                netAmount = netAmount + paying - discount;
                curNetTarget.val(netAmount.toFixed(2));
            });
        } else {
            curNetTarget.val(0.00);
        }
    }

    // Live calculation of bank payment
    function netBankPaymentAmt() {
        var curForm = $('#bank_payment'),
            curTable = curForm.find('table#tbl_bank_payment'),
            curNetTarget = curForm.find('input#amount'),
            netAmount = 0;
        if (curTable.find('tbody > tr').length > 0) {
            curTable.find('tbody > tr').each(function () {
                var paying = 0,
                    discount = 0;
                if (($(this).find('input[name="payingAmount_' + ($(this).index() + 1) + '"]').val().length > 0) && ($.isNumeric($(this).find('input[name="payingAmount_' + ($(this).index() + 1) + '"]').val()))) {
                    paying = parseFloat($(this).find('input[name="payingAmount_' + ($(this).index() + 1) + '"]').val());
                }
                if (($(this).find('input[name="discount_' + ($(this).index() + 1) + '"]').val().length > 0) && ($.isNumeric($(this).find('input[name="discount_' + ($(this).index() + 1) + '"]').val()))) {
                    discount = parseFloat($(this).find('input[name="discount_' + ($(this).index() + 1) + '"]').val());
                }
                netAmount = netAmount + paying - discount;
                curNetTarget.val(netAmount.toFixed(2));
            });
        } else {
            curNetTarget.val(0.00);
        }
    }

    // Live calculation of bank receipt
    function netBankReceiptAmt() {
        var curForm = $('#bank_receipt'),
            curTable = curForm.find('table#tbl_bank_receipt'),
            curNetTarget = curForm.find('input#amount'),
            netAmount = 0;
        if (curTable.find('tbody > tr').length > 0) {
            curTable.find('tbody > tr').each(function () {
                var paying = 0,
                    discount = 0;
                if (($(this).find('input[name="payingAmount_' + ($(this).index() + 1) + '"]').val().length > 0) && ($.isNumeric($(this).find('input[name="payingAmount_' + ($(this).index() + 1) + '"]').val()))) {
                    paying = parseFloat($(this).find('input[name="payingAmount_' + ($(this).index() + 1) + '"]').val());
                }
                if (($(this).find('input[name="discount_' + ($(this).index() + 1) + '"]').val().length > 0) && ($.isNumeric($(this).find('input[name="discount_' + ($(this).index() + 1) + '"]').val()))) {
                    discount = parseFloat($(this).find('input[name="discount_' + ($(this).index() + 1) + '"]').val());
                }
                netAmount = netAmount + paying - discount;
                curNetTarget.val(netAmount.toFixed(2));
            });
        } else {
            curNetTarget.val(0.00);
        }
    }
    // Live calculation for package invoice
    function netPackageInvoiceAmt() {
        var packInvFormTarget = $('#invoice_package'),
            packInvTable = packInvFormTarget.find('#inv_package_paasenger_table'),
            packInvTableRowCount = packInvTable.find('tbody > tr').length,
            totalPassAmt = 0,
            mainTds = 0,
            mainProCharge = 0,
            mainOtherCharge = 0,
            mainDiscount = 0,
            mainTaxText = packInvFormTarget.find('input[name="mainTax"]').val(),
            netAmount = 0;
        if ((packInvFormTarget.find('input[name="tds"]').val() != "") && ($.isNumeric(packInvFormTarget.find('input[name="tds"]').val()))) {
            mainTds = parseFloat(packInvFormTarget.find('input[name="tds"]').val());
        }
        if ((packInvFormTarget.find('input[name="mainProcCharge"]').val() != "") && ($.isNumeric(packInvFormTarget.find('input[name="mainProcCharge"]').val()))) {
            mainProCharge = parseFloat(packInvFormTarget.find('input[name="mainProcCharge"]').val());
        }
        if ((packInvFormTarget.find('input[name="mainOtherCharge"]').val() != "") && ($.isNumeric(packInvFormTarget.find('input[name="mainOtherCharge"]').val()))) {
            mainOtherCharge = parseFloat(packInvFormTarget.find('input[name="mainOtherCharge"]').val());
        }
        if ((packInvFormTarget.find('input[name="discount"]').val() != "") && ($.isNumeric(packInvFormTarget.find('input[name="discount"]').val()))) {
            mainDiscount = parseFloat(packInvFormTarget.find('input[name="discount"]').val());
        }
        if (packInvTableRowCount > 0) {
            packInvTable.find('tbody tr').each(function () {
                var curRow = $(this),
                    curRowIndex = curRow.index(),
                    rowTotal = 0,
                    additionalExtra = parseFloat(curRow.attr('data-additional_extra')),
                    additionalDiscount = parseFloat(curRow.attr('data-additional_discount'));
                if ((curRow.find('input[name="pass'+ (curRowIndex + 1) +'_invoiceTotal"]').val() != "") && ($.isNumeric(curRow.find('input[name="pass'+ (curRowIndex + 1) +'_invoiceTotal"]').val()))) {
                    rowTotal = parseFloat(curRow.find('input[name="pass'+ (curRowIndex + 1) +'_invoiceTotal"]').val());
                }
                totalPassAmt = totalPassAmt + rowTotal + additionalExtra + additionalDiscount;
            });
        }
        if (mainTaxText.toString().indexOf('%') != -1) {
            var mainTaxTextLength = mainTaxText.length,
                mainTaxPerc = parseFloat(mainTaxText.toString().substr(0, (mainTaxTextLength - 1)));
            netAmount = (mainTds + mainProCharge + mainOtherCharge + totalPassAmt) + (((mainTds + mainProCharge + mainOtherCharge + totalPassAmt) / 100) * (parseFloat(mainTaxPerc))) - mainDiscount;
        } else if (mainTaxText != "" && $.isNumeric(mainTaxText)) {
            netAmount = mainTds + mainProCharge + mainOtherCharge + totalPassAmt + parseFloat(mainTaxText) - mainDiscount;
        } else if (mainTaxText.length == 0) {
            netAmount = mainTds + mainProCharge + mainOtherCharge + totalPassAmt - mainDiscount;
        }
        packInvFormTarget.find('input[name="netAmount"]').val(parseFloat(netAmount).toFixed(2));
    }

    // Live calculation for purchase
    function netPurchaseAmount () {
        var curForm = $('#purchase_form'),
            netAmtTarget = curForm.find('input[name="netAmount"]'),
            basicAmt = 0,
            procCharge = 0,
            otherCharge = 0,
            tds = 0,
            refundable = 0,
            roundedOff = 0,
            tac = 0,
            tax = 0,
            netAmount = 0;
        if (curForm.find('#basicAmount').val() != "" && !isNaN(curForm.find('#basicAmount').val())) {
            basicAmt = parseFloat(curForm.find('#basicAmount').val());
        }
        if (curForm.find('#proCharge').val() != "" && !isNaN(curForm.find('#proCharge').val())) {
            procCharge = parseFloat(curForm.find('#proCharge').val());
        }
        if (curForm.find('#otherCharges').val() != "" && !isNaN(curForm.find('#otherCharges').val())) {
            otherCharge = parseFloat(curForm.find('#otherCharges').val());
        }
        if (curForm.find('#tds').val() != "" && !isNaN(curForm.find('#tds').val())) {
            tds = parseFloat(curForm.find('#tds').val());
        }
        if (curForm.find('#refundable').val() != "" && !isNaN(curForm.find('#refundable').val())) {
            refundable = parseFloat(curForm.find('#refundable').val());
        }
        if (curForm.find('#roundAmount').val() != "" && !isNaN(curForm.find('#roundAmount').val())) {
            roundedOff = parseFloat(curForm.find('#roundAmount').val());
        }
        if (curForm.find('#tax').val() != "" && !isNaN(curForm.find('#tax').val())) {
            tax = parseFloat(curForm.find('#tax').val());
        }
        if (curForm.find('#tac').val() != "" && !isNaN(curForm.find('#tac').val())) {
            tac = parseFloat(curForm.find('#tac').val());
        }
        netAmount = ((basicAmt + procCharge + otherCharge + tds + refundable + tax) - tac) + roundedOff;
        netAmtTarget.val(netAmount);
    }
    $(document).on('keyup input', '#purchase_form input[data-action="live_net"]', function () {
        netPurchaseAmount();
    });

    // Live calculation for package creation
    function netPackageCreationAmt () {
        var curForm = $('#package_reg_form'),
            mainTds = 0,
            mainOtherCharge = 0,
            mainTaxText = curForm.find('input[name="pTax"]').val(),
            airLineTotalAdult = 0,
            airLineTotalChild = 0,
            airLineTotalInfant = 0,
            hotelTotal = 0,
            hotelChildTotal = 0,
            visaTotal = 0,
            transportationTotal = 0,
            mainTotalAdult = 0,
            mainTotalChild = 0,
            mainTotalInfant = 0;
        if (curForm.find('input[name="ptds"]').val() && !isNaN(parseFloat(curForm.find('input[name="ptds"]').val()))) {
            mainTds = parseFloat(curForm.find('input[name="ptds"]').val());
        }
        if (curForm.find('input[name="pOtherCharges"]').val() && !isNaN(parseFloat(curForm.find('input[name="pOtherCharges"]').val()))) {
            mainOtherCharge = parseFloat(curForm.find('input[name="pOtherCharges"]').val());
        }


        // Total amount of airline
        if (curForm.find('input#airline').prop('checked') == true) {
            var airDyn = curForm.find('.pack_dynamic_airline'),
                airAdultFare = 0,
                airChildFare = 0,
                airInfantFare = 0,
                airYq = 0,
                airProCharge = 0,
                airTax = airDyn.find('input[name="airTax"]').val(),
                airAdultAmt = 0,
                airChildAmt = 0,
                airInfantAmt = 0;
            if (airDyn.find('input[name="adultChargeAir"]').val() && !isNaN(airDyn.find('input[name="adultChargeAir"]').val())) {
                airAdultFare = parseFloat(airDyn.find('input[name="adultChargeAir"]').val());
            }
            if (airDyn.find('input[name="childChargeAir"]').val() && !isNaN(airDyn.find('input[name="childChargeAir"]').val())) {
                airChildFare = parseFloat(airDyn.find('input[name="childChargeAir"]').val());
            }
            if (airDyn.find('input[name="infantChargeAir"]').val() && !isNaN(airDyn.find('input[name="infantChargeAir"]').val())) {
                airInfantFare = parseFloat(airDyn.find('input[name="infantChargeAir"]').val());
            }
            if (airDyn.find('input[name="airYq"]').val() && !isNaN(airDyn.find('input[name="airYq"]').val())) {
                airYq = parseFloat(airDyn.find('input[name="airYq"]').val());
            }
            if (airDyn.find('input[name="airProCharge"]').val() && !isNaN(airDyn.find('input[name="airProCharge"]').val())) {
                airProCharge = parseFloat(airDyn.find('input[name="airProCharge"]').val());
            }
            if (airTax.length > 1 && airTax.toString().indexOf('%') != -1) {
                var airTaxLength = airTax.length,
                    airTaxPerc = airTax.toString().substr(0, (airTaxLength - 1));
                airAdultAmt = airAdultFare + airYq + airProCharge + ((airTaxPerc / 100) * airAdultFare);
                airChildAmt = airChildFare + airYq + airProCharge + ((airTaxPerc / 100) * airChildFare);
                airInfantAmt = airInfantFare + airYq + airProCharge + ((airTaxPerc / 100) * airInfantAmt);
            } else if (airTax && !isNaN(parseFloat(airTax))) {
                airAdultAmt = airAdultFare + airYq + airProCharge + parseFloat(airTax);
                airChildAmt = airChildFare + airYq + airProCharge + parseFloat(airTax);
                airInfantAmt = airInfantFare + airYq + airProCharge + parseFloat(airTax);
            } else if (airTax.length == 0) {
                airAdultAmt = airAdultFare + airYq + airProCharge;
                airChildAmt = airChildFare + airYq + airProCharge;
                airInfantAmt = airInfantFare + airYq + airProCharge;
            }
            airLineTotalAdult = airAdultAmt;
            airLineTotalChild = airChildAmt;
            airLineTotalInfant = airInfantAmt;
        }

        // Total amount of hotel
        if (curForm.find('input#hotel').prop('checked') == true) {
            var hotelDyn = curForm.find('.pack_dynamic_hotel'),
                hotelTable = hotelDyn.find('table.hotel_outer_table');
            if (hotelTable.find('tbody:first > tr').length > 0) {
                hotelTable.find('tbody:first > tr').each(function () {
                    var curTr = $(this),
                        curTrIndex = curTr.index();
                    if (curTr.find('tbody > tr:first input[data-cal_field="room_total"]').val() && !isNaN(parseFloat(curTr.find('tbody > tr:first input[data-cal_field="room_total"]').val()))) {
                        hotelTotal = hotelTotal + parseFloat(curTr.find('tbody > tr:first input[data-cal_field="room_total"]').val());
                    }
                    if (curTr.find('tbody > tr input[name="hot'+ (curTrIndex + 1) +'_roomTotal_7"]').val() && !isNaN(parseFloat(curTr.find('tbody > tr input[name="hot'+ (curTrIndex + 1) +'_roomTotal_7"]').val()))) {
                        hotelChildTotal = hotelChildTotal + parseFloat(curTr.find('tbody > tr input[name="hot'+ (curTrIndex + 1) +'_roomTotal_7"]').val());
                    }
                });
            }
        }

        // Total amount ov visa
        if (curForm.find('input#visa').prop('checked') == true) {
            var visaDyn = curForm.find('.pack_dynamic_visa'),
                visaTable = visaDyn.find('table');
            visaTable.find('tbody > tr').each(function () {
                var curVisaTr = $(this),
                    curVisaTrIndex = curVisaTr.index();
                if (curVisaTr.find('input[name="visaTotal_'+ (curVisaTrIndex + 1) +'"]').val() && !isNaN(parseFloat(curVisaTr.find('input[name="visaTotal_'+ (curVisaTrIndex + 1) +'"]').val()))) {
                    visaTotal = visaTotal + parseFloat(curVisaTr.find('input[name="visaTotal_'+ (curVisaTrIndex + 1) +'"]').val());
                }
            });
        }

        // Total amount of transportation
        if (curForm.find('input#transport').prop('checked') == true) {
            var transDyn = curForm.find('.pack_dynamic_transportation'),
                transTable = transDyn.find('table');
            transTable.find('tbody > tr').each(function () {
                var curTransTr = $(this),
                    curTransTrIndex = curTransTr.index();
                if (curTransTr.find('input[name="seatCharge_'+ (curTransTrIndex + 1) +'"]').val() && !isNaN(parseFloat(curTransTr.find('input[name="seatCharge_'+ (curTransTrIndex + 1) +'"]').val()))) {
                    transportationTotal = transportationTotal + parseFloat(curTransTr.find('input[name="seatCharge_'+ (curTransTrIndex + 1) +'"]').val());
                }
            });
        }
        // Conclude
        var mainTotalAdultsWithoutTax = mainTds + mainOtherCharge + airLineTotalAdult + hotelTotal + visaTotal + transportationTotal,
            mainTotalChildWithoutTax = airLineTotalChild + hotelChildTotal + visaTotal + transportationTotal + mainTds + mainOtherCharge,
            mainTotalInfantWithoutTax = airLineTotalInfant + visaTotal + mainTds + mainOtherCharge;
        if (mainTaxText.toString().length > 1 && mainTaxText.toString().indexOf('%') != -1) {
            var mainTaxTextLength = mainTaxText.length,
                mainTaxPerc = mainTaxText.toString().substr(0, (mainTaxTextLength - 1));
            mainTotalAdult = mainTotalAdultsWithoutTax + ((parseFloat(mainTaxPerc) / 100) * mainTotalChildWithoutTax);
            mainTotalChild = mainTotalChildWithoutTax + ((parseFloat(mainTaxPerc) / 100) * mainTotalAdultsWithoutTax);
            mainTotalInfant = mainTotalInfantWithoutTax + ((parseFloat(mainTaxPerc) / 100) * mainTotalInfantWithoutTax);
        } else if (mainTaxText.length > 0 && !isNaN(parseFloat(mainTaxText))) {
            mainTotalAdult = mainTotalAdultsWithoutTax + parseFloat(mainTaxText);
            mainTotalChild = mainTotalChildWithoutTax + parseFloat(mainTaxText);
            mainTotalInfant = mainTotalInfantWithoutTax + parseFloat(mainTaxText);
        } else if (mainTaxText.length == 0) {
            mainTotalAdult = mainTotalAdultsWithoutTax;
            mainTotalChild = mainTotalChildWithoutTax;
            mainTotalInfant = mainTotalInfantWithoutTax;
        }
        curForm.find('input#adultTotal').val(mainTotalAdult);
        curForm.find('input#childTotal').val(mainTotalChild);
        curForm.find('input#infantTotal').val(mainTotalInfant);
        /*if (curForm.find('input#airline').prop('checked') == true) {
         curForm.find('input#childTotal').val(mainTotalChild);
         curForm.find('input#infantTotal').val(mainTotalInfant);
         } else {
         curForm.find('input#childTotal').val(0);
         curForm.find('input#infantTotal').val(0);
         }*/
    }




    // Date picker
    if ($('.user_date').length > 0) {
        $('.user_date').each(function () {
            var curDatePicker = $(this);
            //alterTarget = curDatePicker.nextAll('.date_hidden');
            curDatePicker.datepicker({
                dateFormat: "dd-mm-yy",
                changeMonth: true,
                changeYear: true
            });
        });
    }

    $('.header_main_nav > li.has_child > a').click(function (e) {
        e.preventDefault();
        var targetDropMenu = $(this).next();
        $('.header_sub_nav').not(targetDropMenu).hide();
        targetDropMenu.toggle();
    });
    $('ul.side_nav > li.has_child > a').click(function (e) {
        e.preventDefault();
        var curSideNav = $(this),
            curTargetSubNav = curSideNav.nextAll('ul.side_sub_nav');
        $('ul.side_sub_nav').not(curTargetSubNav).slideUp();
        curTargetSubNav.slideToggle();
    });
    $('.side_panel').perfectScrollbar();
    $('.nav_toggler').click(function () {
        $('.side_panel').toggleClass('opened');
        $('.wrapper').toggleClass('give_way');
    });
    // Login form
    $('.login_form_block_inner input').focusin(function () {
        $(this).addClass('focus');
        $(this).prev().addClass('focus');
    });
    $('.login_form_block_inner input').focusout(function () {
        $(this).removeClass('focus');
        $(this).prev().removeClass('focus');
    });
    // Ajax call for accounts details
    $(document).on('click', '.show_table a[data-view_node="get_com_serv"]', function (e) {
        e.preventDefault();
        $('.pop_overlay').fadeIn();
        var serviceFilter = {
                acs: 'acc_retriever',
                comp: 'comp_view',
                hot: 'hotel_view',
                misc: 'misc_view',
                pass: 'pass_view',
                pack: 'package_view'
            },
            curAcTrigger = $(this),
            curServReq = curAcTrigger.attr('data-com_serv_type'),
            curAcID = curAcTrigger.attr('data-view_id');
        $.ajax({
            type: 'POST',
            url: '../../services/' + serviceFilter[curServReq] + '.php',
            /*contentType: 'application/javascript',*/
            data: {
                'accID': curAcID,
                'apiKey': "api123"
            },
            success: function (result) {
                var accResult = result;
                if (accResult.status == "ok") {
                    $('.acs_view_table tbody').empty();
                    $.each(accResult.details, function (key, value) {
                        $('.acs_view_table tbody').append('<tr><td>' + value.title + '</td><td>' + value.dataValue + '</td></tr>');
                    });
                    $('.pop_overlay').fadeOut(50, function () {
                        $('#acs_view_pop').modal('show');
                    });
                }
                if (accResult.status == "failure") {
                    var failReport = accResult.error;
                    $('.pop_overlay').fadeOut('50');
                    triggerError("error", failReport);
                }
            },
            error: function (error) {
                //console.log(error);
                $('.pop_overlay').fadeOut('50');
                triggerError("error", "Something went wrong. Please try again.");
            }
        });
    });
    // Multiple passenger for Airline Invoice
    // Adding multiple rows in Airline details table
    $(document).on('click', '#inv_airline_multiple #add_airline_row', function (e) {
        e.preventDefault();
        var tableTarget = $(this).closest('table'),
            curTrCount = tableTarget.find('tbody > tr').length,
            appendTr = $('#airline_select_options table > tbody').html();
        tableTarget.find('tbody').append(appendTr);
        tableTarget.find('tbody > tr:last-child > td:first-child').text(curTrCount + 1);
        tableTarget.find('tbody > tr:last-child .pass_to_table_air').each(function () {
            var curInput = $(this),
                curFieldName = curInput.attr('name');
            curInput.attr('name', curFieldName + '_' + (curTrCount + 1));
            if (curInput.attr('data-dp') == "datepick") {
                curInput.addClass('user_date');
                curInput.datepicker({
                    dateFormat: "dd-mm-yy",
                    changeMonth: true,
                    changeYear: true
                });
            }
        });
    });
    // Deleting a row in Airline details table in Airline Invoice
    $(document).on('click', '#inv_airline_multiple button[data-action="del_airline_row"]', function (e) {
        e.preventDefault();
        var curDelBtn = $(this),
            curTable = curDelBtn.closest('table'),
            curRow = curDelBtn.parents('tr'),
            curSlNo = curRow.find('td:first-child').text(),
            curRowIndex = curRow.index(),
            curRowCount = curTable.find('tbody > tr').length,
            appendTr = $('#airline_select_options table > tbody').html();
        if (curRowCount == 1) {
            curRow.remove();
            curTable.find('tbody').append(appendTr);
            curTable.find('tbody > tr:first-child .pass_to_table_air').each(function () {
                var curInput = $(this),
                    curFieldName = curInput.attr('name');
                curInput.attr('name', curFieldName + '_1');
                if (curInput.attr('data-dp') == "datepick") {
                    curInput.addClass('user_date');
                    curInput.datepicker({
                        dateFormat: "dd-mm-yy",
                        changeMonth: true,
                        changeYear: true
                    });
                }
            });
            curTable.find('tbody > tr:first > td:first').text(1);

        } else if (curRowCount == (curRowIndex + 1)) {
            curRow.remove();
        }
        else {
            curRow.remove();
            var i;
            for (i = curRowIndex; i <= (curRowCount - 1); i++) {
                var tr = curTable.find('tbody tr:nth-child(' + i + ')');
                tr.find('td:first-child').text(i);
                tr.find('.pass_to_table_air').each(function () {
                    var curFieldName = $(this).attr('name'),
                        curFieldNameLength = curFieldName.length,
                        usIndex = curFieldName.indexOf('_'),
                        curFieldNameArray = curFieldName.split('_');
                    $(this).attr('name', curFieldNameArray[0] + '_' + i);

                });
            }
        }
    });
    $(document).on('click', '.passenger_modal_trigger', function (e) {
        e.preventDefault();
        var passTarget = $($(this).attr('data-modal_target'));
        passTarget.find('.pass_to_table').val('').removeAttr('value');
        passTarget.find('#trigger_pass_table').addClass('add_to_pass_table');
        if (passTarget.find('#inv_airline_multiple > tbody > tr').length > 1) {
            passTarget.find('#inv_airline_multiple > tbody > tr').not(passTarget.find('#inv_airline_multiple > tbody > tr:first-child')).remove();
        }
        passTarget.find('#inv_airline_multiple .pass_to_table_air').each(function () {
            $(this).val('').removeAttr('value');
        });
        passTarget.modal('show');
    });
    $(document).on('click', '#multi_det_add .add_to_pass_table', function (e) {
        e.preventDefault();
        var curCloseBtn = $(this),
            curPop = curCloseBtn.parents('.modal'),
            curAirlineTable = curPop.find('#inv_airline_multiple'),
            passTable = $('#passenger_table'),
            trCount = passTable.find('tbody tr').length,
            passengerFields = {},
            airlineObj = {
                count: curAirlineTable.find('tbody > tr').length
            },
            airlineArray = [],
            flag = false,
            regDate = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
        /*if (curPop.find('input[name="passengerName"]').val()) {
         flag = true;
         curPop.find('input[name="passengerName"]').after('<p class="validation_mes active">Please fill this field</p>');
         }
         if (!curPop.find('input[name="dob"]').val().match(regDate)) {
         flag = true;
         curPop.find('input[name="age"]').after('<p class="validation_mes active">Please enter a valid age</p>');
         }
         if (isNaN(parseInt(curPop.find('input[name="age"]').val()))) {
         flag = true;
         curPop.find('input[name="age"]').after('<p class="validation_mes active">Please enter a valid age</p>');
         }
         curAirlineTable.find('tbody > tr').each(function () {
         var curValidTr = $(this),
         curValidTrIndex = curValidTr.index(),
         if (curValidTr.find('input[]'))
         });*/


        curAirlineTable.find('tbody > tr').each(function () {
            var curAirRow = $(this),
                curRowIndex = curAirRow.index(),
                airLineRowObj = {};
            curAirRow.find('.pass_to_table_air').each(function () {
                if ($(this).val() != "" && $(this).val() != null) {
                    var fieldName = $(this).attr('name'),
                        fieldValue = $(this).val();
                    airLineRowObj[fieldName] = fieldValue;
                }
            });
            airlineArray.push(airLineRowObj);
            Object.keys(airLineRowObj).length = 0;
            airlineObj.airlineDetails = airlineArray;
        });
        $(".pass_to_table").each(function () {
            if ($(this).val() != "" && $(this).val() != null) {
                var fieldName = $(this).attr('name'),
                    fieldValue = $(this).val();
                passengerFields[fieldName] = fieldValue;
            }
        });
        if (Object.keys(passengerFields).length > 0 && airlineObj.count > 0 && trCount == 0) {
            passTable.find('tbody').append(
                '<tr>' +
                '<td><span class="sl_no">1</span></td>' +
                '<td>' + passengerFields.passengerName + '</td>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td>' +
                '<a class="show_table_lnk show_table_lnk_view" href="#">View</a>' +
                '<a class="show_table_lnk show_table_lnk_edit" href="#">Edit</a>' +
                '<a class="show_table_lnk show_table_lnk_del" href="#">Delete</a>' +
                '</td>' +
                '</tr>'
            );
            $.each(passengerFields, function (k, v) {
                passTable.find('tbody tr:first td:first').append('<input type="hidden" data-field_name="' + k + '" name="pass1_' + k + '" value="' + v + '">');
            });
            $.each(airlineObj.airlineDetails, function (keyTxt, keyVal) {
                $.each(keyVal, function (k, v) {
                    var kArray = k.split('_');
                    if (kArray[0] == "ticketNo") {
                        passTable.find('tbody > tr:first > td:nth-child(3)').append('<span class="air_block">' + v + '</span>');
                    } else if (kArray[0] == "sectorFrom") {
                        passTable.find('tbody > tr:first > td:nth-child(4)').append('<span class="air_block">' + v + '</span>');
                    } else if (kArray[0] == "sectorTo") {
                        passTable.find('tbody > tr:first > td:nth-child(5)').append('<span class="air_block">' + v + '</span>');
                    } else if (kArray[0] == "flightNo") {
                        passTable.find('tbody > tr:first > td:nth-child(6)').append('<span class="air_block">' + v + '</span>');
                    } else if (kArray[0] == "journeyDate") {
                        passTable.find('tbody > tr:first > td:nth-child(7)').append('<span class="air_block">' + v + '</span>');
                    } else if (kArray[0] == "fare") {
                        passTable.find('tbody > tr:first > td:nth-child(8)').append('<span class="air_block">' + v + '</span>');
                    } else if (kArray[0] == "tax") {
                        passTable.find('tbody > tr:first > td:nth-child(9)').append('<span class="air_block">' + v + '</span>');
                    } else if (kArray[0] == "yq") {
                        passTable.find('tbody > tr:first > td:nth-child(10)').append('<span class="air_block">' + v + '</span>');
                    }
                    passTable.find('tbody > tr:first td:nth-child(2)').append('<input type="hidden" data-field_name="' + k + '" name="pass1_' + k + '" value="' + v + '" />');
                });
                passTable.find('tbody > tr:first td:nth-child(2)').append('<input type="hidden" name="pass1_airlineRowCount" value="' + airlineObj.count + '" />');
            });
            /*curAirlineTable.find('tr:first .pass_to_table_air').val("")*/
            curCloseBtn.parents('.modal').modal('hide');
            netAirlineAmt();
            getPassengerRowCount();
        } else if (Object.keys(passengerFields).length > 0 && airlineObj.count > 0 && trCount > 0) {
            passTable.find('tbody').append(
                '<tr>' +
                '<td><span class="sl_no">' + (trCount + 1) + '</span></td>' +
                '<td>' + passengerFields.passengerName + '</td>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td>' +
                '<a class="show_table_lnk show_table_lnk_view" href="#">View</a>' +
                '<a class="show_table_lnk show_table_lnk_edit" href="#">Edit</a>' +
                '<a class="show_table_lnk show_table_lnk_del" href="#">Delete</a>' +
                '</td>' +
                '</tr>'
            );
            $.each(passengerFields, function (k, v) {
                passTable.find('tbody tr:last td:first').append('<input type="hidden" data-field_name="' + k + '" name="pass' + (trCount + 1) + '_' + k + '" value="' + v + '">');
            });
            $.each(airlineObj.airlineDetails, function (keyTxt, keyVal) {
                $.each(keyVal, function (k, v) {
                    var kArray = k.split('_');
                    if (kArray[0] == "ticketNo") {
                        passTable.find('tbody > tr:last > td:nth-child(3)').append('<span class="air_block">' + v + '</span>');
                    } else if (kArray[0] == "sectorFrom") {
                        passTable.find('tbody > tr:last > td:nth-child(4)').append('<span class="air_block">' + v + '</span>');
                    } else if (kArray[0] == "sectorTo") {
                        passTable.find('tbody > tr:last > td:nth-child(5)').append('<span class="air_block">' + v + '</span>');
                    } else if (kArray[0] == "flightNo") {
                        passTable.find('tbody > tr:last > td:nth-child(6)').append('<span class="air_block">' + v + '</span>');
                    } else if (kArray[0] == "journeyDate") {
                        passTable.find('tbody > tr:last > td:nth-child(7)').append('<span class="air_block">' + v + '</span>');
                    } else if (kArray[0] == "fare") {
                        passTable.find('tbody > tr:last > td:nth-child(8)').append('<span class="air_block">' + v + '</span>');
                    } else if (kArray[0] == "tax") {
                        passTable.find('tbody > tr:last > td:nth-child(9)').append('<span class="air_block">' + v + '</span>');
                    } else if (kArray[0] == "yq") {
                        passTable.find('tbody > tr:last > td:nth-child(10)').append('<span class="air_block">' + v + '</span>');
                    }
                    passTable.find('tbody > tr:last td:nth-child(2)').append('<input type="hidden" data-field_name="' + k + '" name="pass' + (trCount + 1) + '_' + k + '" value="' + v + '" />');
                });
            });
            passTable.find('tbody > tr:nth-child(' + (trCount + 1) + ') td:nth-child(2)').append('<input type="hidden" name="pass' + (trCount + 1) + '_airlineRowCount" value="' + airlineObj.count + '" />');
            // console.log(airlineObj);
            curCloseBtn.parents('.modal').modal('hide');
            netAirlineAmt();
            getPassengerRowCount();
        }
    });
    // View btn action for passenger table
    $(document).on('click', '#passenger_table > tbody > tr > td a.show_table_lnk_view', function (e) {
        e.preventDefault();
        $('#multi_det_add').find('#trigger_pass_table').addClass('clear_pass_pop');
        var curPassViewBtn = $(this),
            curRow = curPassViewBtn.parents('tr'),
            curRowIndex = curRow.index(),
            curPopUp = $('#multi_det_add'),
            curPopUpAirlineTable = curPopUp.find('#inv_airline_multiple'),
            appendTr = $('#airline_select_options table > tbody').html(),
            curPassObj = {},
            curAirlineObj = {
                count: curRow.find('td:nth-child(2) input[name="pass' + (curRowIndex + 1) + '_airlineRowCount"]').val(),
                airLineDetails: {}
            },
            i;
        curPopUpAirlineTable.find('tbody').empty();
        Object.keys(curPassObj).length = 0;
        curRow.find('td:first input[type="hidden"]').each(function () {
            var keyName = $(this).attr('data-field_name'),
                keyValue = $(this).val();
            curPassObj[keyName] = keyValue;
            curPopUp.find('.pass_to_table').each(function () {
                $(this).attr('disabled', 'disabled');
            });
        });
        $.each(curPassObj, function (key, value) {
            curPopUp.find('.pass_to_table[name="' + key + '"]').val(value);
        });
        curRow.find('td:nth-child(2) input[type=hidden]').not(curRow.find('td:nth-child(2) input[name="pass' + (curRowIndex + 1) + '_airlineRowCount"]')).each(function () {
            var k = $(this).attr('data-field_name'),
                v = $(this).val();
            curAirlineObj.airLineDetails[k] = v;
        });
        for (i = 1; i <= curAirlineObj.count; i++) {
            curPopUpAirlineTable.find('tbody').append(appendTr);
        }
        curPopUpAirlineTable.find('tbody tr').each(function () {
            var curAirTr = $(this);
            curAirTr.find('td:first').text(curAirTr.index() + 1);
            curAirTr.find('.pass_to_table_air').each(function () {
                var curViewInput = $(this),
                    curViewInputName = curViewInput.attr('name');
                curViewInput.attr('name', curViewInputName + '_' + (curAirTr.index() + 1)).attr('disabled', 'disabled');
            });
        });
        $.each(curAirlineObj.airLineDetails, function (k, v) {
            curPopUpAirlineTable.find('.pass_to_table_air[name="' + k + '"]').val(v);
        });
        curPopUp.modal('show');
    });
    $(document).on('click', '#multi_det_add .clear_pass_pop', function () {
        $(this).parents('#multi_det_add').modal('hide');
    });
    // Edit passenger details
    $(document).on('click', '#passenger_table > tbody > tr > td a.show_table_lnk_edit', function (e) {
        e.preventDefault();

        var curPassEdtBtn = $(this),
            curPassRow = curPassEdtBtn.parents('tr'),
            curPassRowIndex = curPassRow.index(),
            curPopUp = $('#multi_det_add'),
            curPopUpAirlineTable = curPopUp.find('#inv_airline_multiple'),
            appendTr = $('#airline_select_options table > tbody').html(),
            curPassEdtObj = {},
            curAirlineObj = {
                count: curPassRow.find('td:nth-child(2) input[name="pass' + (curPassRowIndex + 1) + '_airlineRowCount"]').val(),
                airLineDetails: {}
            },
            i;
        curPopUpAirlineTable.find('tbody').empty();
        Object.keys(curPassEdtObj).length = 0;
        curPopUp.find('#trigger_pass_table').addClass('edit_pass_det');
        curPopUp.attr('data-edit_row_index', curPassRowIndex);
        curPassRow.find('td:first input[type="hidden"]').each(function () {
            var keyName = $(this).attr('data-field_name'),
                keyValue = $(this).val();
            curPassEdtObj[keyName] = keyValue;
            $.each(curPassEdtObj, function (key, value) {
                $('#multi_det_add').find('.pass_to_table[name="' + key + '"]').val(value);
            });
        });
        curPassRow.find('td:nth-child(2) input[type=hidden]').not(curPassRow.find('td:nth-child(2) input[name="pass' + (curPassRowIndex + 1) + '_airlineRowCount"]')).each(function () {
            var k = $(this).attr('data-field_name'),
                v = $(this).val();
            curAirlineObj.airLineDetails[k] = v;
        });
        for (i = 1; i <= curAirlineObj.count; i++) {
            curPopUpAirlineTable.find('tbody').append(appendTr);
        }
        curPopUpAirlineTable.find('tbody tr').each(function () {
            var curAirTr = $(this);
            curAirTr.find('td:first').text(curAirTr.index() + 1);
            curAirTr.find('.pass_to_table_air').each(function () {
                var curViewInput = $(this),
                    curViewInputName = curViewInput.attr('name');
                curViewInput.attr('name', curViewInputName + '_' + (curAirTr.index() + 1));
                if (curViewInput.attr('data-dp') == "datepick") {
                    curViewInput.addClass('user_date');
                    curViewInput.datepicker({
                        dateFormat: "dd-mm-yy",
                        changeMonth: true,
                        changeYear: true
                    });
                }
            });
        });
        $.each(curAirlineObj.airLineDetails, function (k, v) {
            curPopUpAirlineTable.find('.pass_to_table_air[name="' + k + '"]').val(v);
        });
        curPopUp.modal('show');
    });
    $(document).on('click', '#multi_det_add .edit_pass_det', function () {
        var curEdtPassBtn = $(this),
            passModal = curEdtPassBtn.parents('#multi_det_add'),
            curAirlineTable = curEdtPassBtn.parents('.modal').find('#inv_airline_multiple'),
            passTable = $('#passenger_table'),
            edtRowIndex = parseInt(passModal.attr('data-edit_row_index')),
            passengerFields = {},
            airlineObj = {
                count: curAirlineTable.find('tbody > tr').length
            },
            airlineArray = [];
        curAirlineTable.find('tbody > tr').each(function () {
            var curAirRow = $(this),
                curRowIndex = curAirRow.index(),
                airLineRowObj = {};
            curAirRow.find('.pass_to_table_air').each(function () {
                if ($(this).val() != "" && $(this).val() != null) {
                    var fieldName = $(this).attr('name'),
                        fieldValue = $(this).val();
                    airLineRowObj[fieldName] = fieldValue;
                }
            });
            airlineArray.push(airLineRowObj);
            //Object.keys(airLineRowObj).length = 0;
            airlineObj.airlineDetails = airlineArray;
        });
        passModal.find(".pass_to_table").each(function () {
            if ($(this).val() != "") {
                var fieldName = $(this).attr('name'),
                    fieldValue = $(this).val();
                passengerFields[fieldName] = fieldValue;
            }
        });
        $('#passenger_table > tbody > tr:nth-child(' + (edtRowIndex + 1) + ')').empty();
        passTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ')').append(
            '<td><span class="sl_no">' + (edtRowIndex + 1) + '</span></td>' +
            '<td>' + passengerFields.passengerName + '</td>' +
            '<td></td>' +
            '<td></td>' +
            '<td></td>' +
            '<td></td>' +
            '<td></td>' +
            '<td></td>' +
            '<td></td>' +
            '<td></td>' +
            '<td>' +
            '<a class="show_table_lnk show_table_lnk_view" href="#">View</a>' +
            '<a class="show_table_lnk show_table_lnk_edit" href="#">Edit</a>' +
            '<a class="show_table_lnk show_table_lnk_del" href="#">Delete</a>' +
            '</td>'
        );
        $.each(passengerFields, function (k, v) {
            passTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') td:first').append('<input type="hidden" data-field_name="' + k + '" name="pass' + (edtRowIndex + 1) + '_' + k + '" value="' + v + '">');
        });
        $.each(airlineObj.airlineDetails, function (keyTxt, keyVal) {
            $.each(keyVal, function (k, v) {
                var kArray = k.split('_');
                if (kArray[0] == "ticketNo") {
                    passTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') > td:nth-child(3)').append('<span class="air_block">' + v + '</span>');
                } else if (kArray[0] == "sectorFrom") {
                    passTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') > td:nth-child(4)').append('<span class="air_block">' + v + '</span>');
                } else if (kArray[0] == "sectorTo") {
                    passTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') > td:nth-child(5)').append('<span class="air_block">' + v + '</span>');
                } else if (kArray[0] == "flightNo") {
                    passTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') > td:nth-child(6)').append('<span class="air_block">' + v + '</span>');
                } else if (kArray[0] == "journeyDate") {
                    passTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') > td:nth-child(7)').append('<span class="air_block">' + v + '</span>');
                } else if (kArray[0] == "fare") {
                    passTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') > td:nth-child(8)').append('<span class="air_block">' + v + '</span>');
                } else if (kArray[0] == "tax") {
                    passTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') > td:nth-child(9)').append('<span class="air_block">' + v + '</span>');
                } else if (kArray[0] == "yq") {
                    passTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') > td:nth-child(10)').append('<span class="air_block">' + v + '</span>');
                }
                passTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') td:nth-child(2)').append('<input type="hidden" data-field_name="' + k + '" name="pass' + (edtRowIndex + 1) + '_' + k + '" value="' + v + '" />');
            });
            passTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') td:nth-child(2)').append('<input type="hidden" name="pass' + (edtRowIndex + 1) + '_airlineRowCount" value="' + airlineObj.count + '" />');
        });
        passModal.modal('hide');
        netAirlineAmt();
    });
    // Delete passenger data
    $(document).on('click', '#passenger_table > tbody > tr > td > a.show_table_lnk_del', function (e) {
        e.preventDefault();
        var curPassDelBtn = $(this),
            curTable = curPassDelBtn.parents('table'),
            curTblFor = curTable.attr('data-tbl_for'),
            curRow = curPassDelBtn.parents('tr'),
            curRowIndex = curRow.index(),
            trCount = $('#passenger_table > tbody > tr').length;
        if ((trCount == 1) || ((trCount - 1) == curRowIndex)) {
            curRow.remove();
            netAirlineAmt();
            getPassengerRowCount();
        } else {
            curRow.remove();
            var newTrCount = trCount - 1;
            var i;
            for (i = curRowIndex; i <= (newTrCount - 1); i++) {
                var hiddenFieldList = {},
                    slNo = parseInt($('#passenger_table > tbody > tr:nth-child(' + (i + 1) + ') > td:first span.sl_no').text()) - 1;
                Object.keys(hiddenFieldList).length = 0;
                $('#passenger_table > tbody > tr:nth-child(' + (i + 1) + ') > td:first input[type="hidden"]').each(function () {
                    var curHiddenFields = $(this),
                        fieldName = curHiddenFields.attr('name'),
                        fieldNameStriped = fieldName.slice(4),
                        fieldSplitArray = fieldNameStriped.split('_'),
                        fieldNameFinal = fieldSplitArray[1],
                        fieldValue = $(this).val();
                    hiddenFieldList[fieldNameFinal] = fieldValue;
                    $.each(hiddenFieldList, function (keyText, keyValue) {
                        $('#passenger_table > tbody > tr:nth-child(' + (i + 1) + ') > td:first input[name="pass' + parseInt(slNo + 1) + '_' + keyText + '"]').attr('name', 'pass' + slNo + '_' + keyText);
                    });
                });
                $('#passenger_table > tbody > tr:nth-child(' + (i + 1) + ') > td:nth-child(2) input[type="hidden"]').each(function () {
                    var curHiddenFields = $(this),
                        fieldName = curHiddenFields.attr('name'),
                        fieldSplitArray = fieldName.split('_');
                    if (fieldSplitArray.length == 3) {
                        curHiddenFields.attr('name', 'pass' + (slNo) + '_' + fieldSplitArray[1] + '_' + fieldSplitArray[2]);
                    } else if (fieldSplitArray.length == 2) {
                        curHiddenFields.attr('name', 'pass' + (slNo) + '_' + fieldSplitArray[1]);
                    }
                });
                $('#passenger_table > tbody > tr:nth-child(' + (i + 1) + ') > td:first span.sl_no').text(slNo);
            }
            getPassengerRowCount();
            netAirlineAmt();
        }
    });
    $('#multi_det_add').on('hidden.bs.modal', function () {
        var passPop = $('#multi_det_add');
        passPop.find('.pass_to_table').each(function () {
            $(this).removeAttr('disabled');
            $(this).val('');
            $(this).removeAttr('value');
        });
        $(this).find('#trigger_pass_table').removeClass('add_to_pass_table clear_pass_pop edit_pass_det');
    });
    /*// Multiple passengers in Visa invoice
     $(document).on('click', '.visa_passenger_modal_trigger', function (e) {
     e.preventDefault();
     var passTarget = $($(this).attr('data-modal_target'));
     passTarget.find('.pass_to_table').val('').removeAttr('value');
     passTarget.find('#trigger_pass_table').addClass('add_to_pass_table');
     passTarget.modal('show');
     });
     // Pass values to visa passenger Table
     $(document).on('click', '#visa_multi_det_add .add_to_pass_table', function (e) {
     e.preventDefault();
     var curCloseBtn = $(this),
     passTable = $('#visa_passenger_table'),
     trCount = passTable.find('tbody tr').length,
     passengerFields = {};
     $('.pass_to_table').each(function () {
     if ($(this).val() != "") {
     var fieldName = $(this).attr('name'),
     fieldValue = $(this).val();
     passengerFields[fieldName] = fieldValue;
     }
     });
     if (Object.keys(passengerFields).length > 0) {
     if (trCount == 0) {
     passTable.find('tbody').append('<tr>' +
     '<td><span class="sl_no">1</span></td>' +
     '<td>' + passengerFields.passengerName + '</td>' +
     '<td>' + passengerFields.mobile + '</td>' +
     '<td>' + passengerFields.passportNo + '</td>' +
     '<td>' + passengerFields.visaFee + '</td>' +
     '<td>' + passengerFields.submissionDate + '</td>' +
     '<td>' + passengerFields.vfs + '</td>' +
     '<td>' + passengerFields.dispatchDate + '</td>' +
     '<td>' +
     '<a class="show_table_lnk show_table_lnk_view" href="#">View</a>' +
     '<a class="show_table_lnk show_table_lnk_edit" href="#">Edit</a>' +
     '<a class="show_table_lnk show_table_lnk_del" href="#">Delete</a>' +
     '</td>' +
     '</tr>');
     $.each(passengerFields, function (key, value) {
     var keyName = key,
     keyValue = value;
     passTable.find('tbody tr:first td:first').append('<input type="hidden" data-field_name="' + keyName + '" name="pass1_' + keyName + '" value="' + keyValue + '">');

     });
     curCloseBtn.parents('.modal').modal('hide');
     getVisaPassengerRowCount();
     netVisaAmount();
     }
     else if (trCount > 0) {
     passTable.find('tbody').append('<tr>' +
     '<td><span class="sl_no">' + (trCount + 1) + '</span></td>' +
     '<td>' + passengerFields.passengerName + '</td>' +
     '<td>' + passengerFields.mobile + '</td>' +
     '<td>' + passengerFields.passportNo + '</td>' +
     '<td>' + passengerFields.visaFee + '</td>' +
     '<td>' + passengerFields.submissionDate + '</td>' +
     '<td>' + passengerFields.vfs + '</td>' +
     '<td>' + passengerFields.dispatchDate + '</td>' +
     '<td>' +
     '<a class="show_table_lnk show_table_lnk_view" href="#">View</a>' +
     '<a class="show_table_lnk show_table_lnk_edit" href="#">Edit</a>' +
     '<a class="show_table_lnk show_table_lnk_del" href="#">Delete</a>' +
     '</td>' +
     '</tr>');
     $.each(passengerFields, function (key, value) {
     var keyName = key,
     keyValue = value;
     passTable.find('tbody tr:last td:first').append('<input type="hidden" data-field_name="' + keyName + '" name="pass' + (trCount + 1) + '_' + keyName + '" value="' + keyValue + '">');

     });
     curCloseBtn.parents('.modal').modal('hide');
     getVisaPassengerRowCount();
     netVisaAmount();
     }
     }
     });
     // View btn action for passenger table in Visa Invoice
     $(document).on('click', '#visa_passenger_table > tbody > tr > td a.show_table_lnk_view', function (e) {
     e.preventDefault();
     $('#visa_multi_det_add').find('#trigger_pass_table').addClass('clear_pass_pop');
     var curPassViewBtn = $(this),
     curPassObj = {};
     Object.keys(curPassObj).length = 0;
     curPassViewBtn.parents('tr').find('td:first input[type="hidden"]').each(function () {
     var keyName = $(this).attr('data-field_name'),
     keyValue = $(this).val();
     curPassObj[keyName] = keyValue;
     $('#visa_multi_det_add .pass_to_table').each(function () {
     $(this).attr('disabled', 'disabled');
     });
     $.each(curPassObj, function (key, value) {
     $('#visa_multi_det_add').find('.pass_to_table[name="' + key + '"]').val(value);
     });
     $('#visa_multi_det_add').modal('show');
     });
     });
     $(document).on('click', '#visa_multi_det_add .clear_pass_pop', function () {
     $(this).parents('#visa_multi_det_add').modal('hide');
     });
     // Edit passenger details in Visa Invoice
     $(document).on('click', '#visa_passenger_table > tbody > tr > td a.show_table_lnk_edit', function (e) {
     e.preventDefault();
     var curPassEdtBtn = $(this),
     curPassRow = curPassEdtBtn.parents('tr'),
     curPassRowIndex = curPassRow.index(),
     curPassEdtObj = {};
     Object.keys(curPassEdtObj).length = 0;
     $('#visa_multi_det_add').find('#trigger_pass_table').addClass('edit_pass_det');
     $('#visa_multi_det_add').attr('data-edit_row_index', curPassRowIndex);
     curPassRow.find('td:first input[type="hidden"]').each(function () {
     var keyName = $(this).attr('data-field_name'),
     keyValue = $(this).val();
     curPassEdtObj[keyName] = keyValue;
     $.each(curPassEdtObj, function (key, value) {
     $('#visa_multi_det_add').find('.pass_to_table[name="' + key + '"]').val(value);
     });
     $('#visa_multi_det_add').modal('show');
     });
     });
     $(document).on('click', '#visa_multi_det_add .edit_pass_det', function (e) {
     e.preventDefault();
     var curEdtPassBtn = $(this),
     passModal = curEdtPassBtn.parents('#visa_multi_det_add'),
     edtRowIndex = parseInt(passModal.attr('data-edit_row_index')),
     passengerFields = {};
     passModal.find(".pass_to_table").each(function () {
     if ($(this).val() != "") {
     var fieldName = $(this).attr('name'),
     fieldValue = $(this).val();
     passengerFields[fieldName] = fieldValue;
     }
     });
     $('#visa_passenger_table > tbody > tr:nth-child(' + (edtRowIndex + 1) + ')').empty();
     $('#visa_passenger_table > tbody > tr:nth-child(' + (edtRowIndex + 1) + ')').append('<td><span class="sl_no">' + (edtRowIndex + 1) + '</span></td>' +
     '<td>' + passengerFields.passengerName + '</td>' +
     '<td>' + passengerFields.mobile + '</td>' +
     '<td>' + passengerFields.passportNo + '</td>' +
     '<td>' + passengerFields.visaFee + '</td>' +
     '<td>' + passengerFields.submissionDate + '</td>' +
     '<td>' + passengerFields.vfs + '</td>' +
     '<td>' + passengerFields.dispatchDate + '</td>' +
     '<td>' +
     '<a class="show_table_lnk show_table_lnk_view" href="#">View</a>' +
     '<a class="show_table_lnk show_table_lnk_edit" href="#">Edit</a>' +
     '<a class="show_table_lnk show_table_lnk_del" href="#">Delete</a>' +
     '</td>');
     $.each(passengerFields, function (key, value) {
     var keyName = key,
     keyValue = value;
     $('#visa_passenger_table tbody tr:nth-child(' + (edtRowIndex + 1) + ') > td:first').append('<input type="hidden" data-field_name="' + keyName + '" name="pass' + (edtRowIndex + 1) + '_' + keyName + '" value="' + keyValue + '">');

     });
     curEdtPassBtn.parents('.modal').modal('hide');
     netVisaAmount();
     });
     // Delete passenger data in Visa Invoice
     $(document).on('click', '#visa_passenger_table > tbody > tr > td a.show_table_lnk_del', function (e) {
     e.preventDefault();
     var curPassDelBtn = $(this),
     curRow = curPassDelBtn.parents('tr'),
     curRowIndex = curRow.index(),
     trCount = $('#visa_passenger_table > tbody > tr').length;
     if ((trCount == 1) || ((trCount - 1) == curRowIndex)) {
     curRow.remove();
     getVisaPassengerRowCount();
     netVisaAmount();
     } else {
     curRow.remove();
     var newTrCount = trCount - 1;
     var i;
     for (i = curRowIndex; i <= (newTrCount - 1); i++) {
     var hiddenFieldList = {},
     slNo = parseInt($('#visa_passenger_table > tbody > tr:nth-child(' + (i + 1) + ') > td:first span.sl_no').text()) - 1;
     Object.keys(hiddenFieldList).length = 0;
     $('#visa_passenger_table > tbody > tr:nth-child(' + (i + 1) + ') > td:first input[type="hidden"]').each(function () {
     var curHiddenFields = $(this),
     fieldName = $(this).attr('name'),
     fieldNameStriped = fieldName.slice(4),
     fieldSplitArray = fieldNameStriped.split('_'),
     fieldNameFinal = fieldSplitArray[1],
     fieldValue = $(this).val();
     hiddenFieldList[fieldNameFinal] = fieldValue;
     $.each(hiddenFieldList, function (keyText, keyValue) {
     $('#visa_passenger_table > tbody > tr:nth-child(' + (i + 1) + ') > td:first input[name="pass' + parseInt(slNo + 1) + '_' + keyText + '"]').attr('name', 'pass' + slNo + '_' + keyText);
     });
     });
     $('#visa_passenger_table > tbody > tr:nth-child(' + (i + 1) + ') > td:first span.sl_no').text(slNo);
     }
     getVisaPassengerRowCount();
     netVisaAmount();
     }
     });*/
    $('#misc_form input[data-net="net_trigger"]').each(function () {
        $(this).keyup(function () {
            netMiscellaneousAmount();
        });
    });
    $('#visa_multi_det_add input[data-visa_net="visa_net_trigger"]').each(function () {
        $(this).keyup(function () {
            visaPopupAmtUpdator();
        });
    });

    // Clear class names in visa pass pop
    $('#visa_multi_det_add').on('hidden.bs.modal', function () {
        var passPop = $('#visa_multi_det_add');
        passPop.find('.pass_to_table').each(function () {
            $(this).removeAttr('disabled');
            $(this).val('');
            $(this).removeAttr('value');
        });
        $(this).find('#trigger_pass_table').removeClass('add_to_pass_table clear_pass_pop edit_pass_det');
    });


    // Adding hotel room details
    $(document).on('click', '.add_hotel_row', function (e) {
        e.preventDefault();
        var tableTarget = $('#hotel_room_table'),
            numRows = tableTarget.find('tbody tr').length,
            lastIndex = numRows - 1,
            appendTr = $('#hotel_common_table_row table > tbody > tr').get(0).outerHTML;
        tableTarget.append(appendTr);
        tableTarget.find('tbody > tr:last td:first').text(numRows + 1);
        tableTarget.find('tbody > tr:last input').each(function () {
            $(this).attr('name', $(this).attr('name') + '_' + (numRows + 1));
        });
        tableTarget.find('tbody > tr:last select').each(function () {
            $(this).attr('name', $(this).attr('name') + '_' + (numRows + 1));
        });
        getHotelRowCount();
    });
    $(document).on('click', '#hotel_room_table a.hotel_row_del', function (e) {
        e.preventDefault();
        var curDelBtn = $(this),
            targetTable = $(this).parents('table'),
            curRowIndex = curDelBtn.parents('tr').index(),
            rowLength = targetTable.find('tbody tr').length,
            appendTr = $('#hotel_common_table_row table > tbody > tr').get(0).outerHTML;
        if (rowLength == 1) {
            curDelBtn.parents('tr').remove();
            targetTable.append(appendTr);
            targetTable.find('tbody > tr:last td:first').text(1);
            targetTable.find('tbody > tr:last input').each(function () {
                $(this).attr('name', $(this).attr('name') + '_1');
            });
            targetTable.find('tbody > tr:last select').each(function () {
                $(this).attr('name', $(this).attr('name') + '_1');
            });
            getHotelRowCount();
            netHotelAmt();
        } else if ((rowLength > 1) && (rowLength == (curRowIndex + 1))) {
            curDelBtn.parents('tr').remove();
            getHotelRowCount();
            netHotelAmt();
        } else {
            curDelBtn.parents('tr').remove();
            var newRowLength = rowLength - 1,
                i;
            for (i = curRowIndex; i <= newRowLength; i++) {
                var slNo = parseInt(targetTable.find('tbody tr:nth-child(' + (i + 1) + ') td:first').text()) - 1;
                //console.log(slNo);
                targetTable.find('tbody tr:nth-child(' + (i + 1) + ') td:first').text(slNo);
                targetTable.find('tbody tr:nth-child(' + (i + 1) + ') td .hotel_input').each(function () {
                    var curInput = $(this),
                        fieldName = curInput.attr('name'),
                        fieldNameArray = fieldName.split('_');
                    curInput.attr('name', fieldNameArray[0] + '_' + slNo);
                });
            }
            getHotelRowCount();
            netHotelAmt();
        }
    });
    // Hotel invoice live calculation trigger
    $(document).on('change', '#invoice_hotel #checkInDate', function () {
        if ($('#invoice_hotel #checkOutDate').val() != "") {
            netHotelTableAmt();
            netHotelAmt();
        }
    });
    $(document).on('change', '#invoice_hotel #checkOutDate', function () {
        if ($('#invoice_hotel #checkInDate').val() != "") {
            netHotelTableAmt();
            netHotelAmt();
        }
    });
    $(document).on('keyup', '#invoice_hotel #hotel_room_table input[data-hotel_net="hotel_net_room_trigger"]', function () {
        netHotelTableAmt();
        netHotelAmt();
    });
    $(document).on('keyup', '#invoice_hotel input[data-hotel_net="hotel_net_trigger"]', function () {
        netHotelAmt();
    });

    // Airline invoice live calculations
    $('#invoice_airline input[name="tds"]').keyup(function () {
        netAirlineAmt();
    });
    $('#invoice_airline input[name="mainProcCharge"]').keyup(function () {
        netAirlineAmt();
    });
    $('#invoice_airline input[name="mainOtherCharge"]').keyup(function () {
        netAirlineAmt();
    });
    $('#invoice_airline input[name="mainTax"]').keyup(function () {
        netAirlineAmt();
    });
    $('#invoice_airline input[name="discount"]').keyup(function () {
        netAirlineAmt();
    });

    // live search for passeneger
    $(document).on('keyup', '.aj_rel_box input[name="passengerName"]', function (e) {
        var curSearchBox = $(this),
            curForm = curSearchBox.closest('form'),
            curParentId = $('input[name="customerId"]').val(),
            curSearchKey = curSearchBox.val(),
            curLiveList = curSearchBox.siblings('ul.aj_live_items'),
            curKey = e.keyCode,
            curLiveSelected = curLiveList.find('li').filter('.selected'),
            current;
        //console.log(curSearchKey);
        if (curKey != 40 && curKey != 38 && curKey != 13 && curSearchKey.length > 0) {
            $.ajax({
                type: 'Post',
                url: '../../services/passenger_live.php',
                data: {
                    apiKey: "api123",
                    searchKey: curSearchKey,
                    parentId: curParentId
                },
                success: function (passResult) {
                    var passObj = passResult;
                    if (passObj.status == "ok") {
                        curLiveList.empty();
                        var passList = passObj.passengerNames;
                        //console.log(passList);
                        $.each(passList, function (key, value) {
                            curLiveList.append('<li data-pass_id="' + value.id + '">' + value.passenger_name + '<span class="pass_mobile"><span></span>' + value.mobile + '</span></li>');
                        });
                    } else if (passObj.status == "failure") {
                        curLiveList.empty();
                    }
                }
            });
        } else if (curSearchKey.length == 0) {
            curLiveList.empty();
        } else if (curKey == 40) {
            curLiveList.find('li').removeClass('selected');
            if (!curLiveSelected.length || curLiveSelected.is(':last-child')) {
                current = curLiveList.find('li').eq(0);
            } else {
                current = curLiveSelected.next();
            }
            $(current).addClass('selected');
        } else if (curKey == 38) {
            curLiveList.find('li').removeClass('selected');
            if (!curLiveSelected.length || curLiveSelected.is(':first-child')) {
                current = curLiveList.find('li').last();
            } else {
                current = curLiveSelected.prev();
            }
            $(current).addClass('selected');
        } else if (curKey == 13) {
            //console.log(curLiveList.find('li.selected').attr('data-pass_name'));
            $('.pop_overlay').fadeIn();
            var passID = curLiveList.find('li.selected').attr('data-pass_id');
            $.ajax({
                type: 'POST',
                url: '../../services/passenger_live.php',
                data: {
                    apiKey: "api123",
                    passengerID: passID
                },
                success: function (passengerDet) {
                    if (passengerDet.status == "ok") {
                        var passengerObj = passengerDet.passengerDetails;
                        $.each(passengerObj, function (keyTerm, keyValue) {
                            if (curForm.find('*[name="' + keyTerm + '"]').length > 0) {
                                curForm.find('*[name="' + keyTerm + '"]').val(keyValue);
                            }
                        });
                        curLiveList.empty();
                        if (curForm.attr('data-form_for') == "age_based") {
                            ageBasedRateTrigger(curForm.find('input[name="age"]').val());
                        }
                        $('.pop_overlay').fadeOut();
                    }
                }
            });
        }
    });

    // Live search for customer
    $(document).on('keyup', '.aj_rel_box input[name="customerName"]', function (e) {
        var curSearchBox = $(this),
            curForm = curSearchBox.closest('form'),
            curSearchKey = curSearchBox.val(),
            curLiveList = curSearchBox.siblings('ul.aj_live_items'),
            curKey = e.keyCode,
            curLiveSelected = curLiveList.find('li').filter('.selected'),
            current;
        if (curKey != 40 && curKey != 38 && curKey != 13 && curSearchKey.length > 0) {
            $.ajax({
                type: 'Post',
                url: '../../services/customer_live.php',
                data: {
                    apiKey: "api123",
                    searchKey: curSearchKey
                },
                success: function (custResult) {
                    if (custResult.status == "ok") {
                        curLiveList.empty();
                        var custObj = custResult.custList;
                        $.each(custObj, function (keyText, keyValue) {
                            curLiveList.append('<li data-cust_id="' + keyValue.id + '">' + keyValue.accountName + '<span class="pass_mobile"><span></span>' + keyValue.id + '</span></li>');
                        });
                    } else if (custResult.status == "failure") {
                        curLiveList.empty();
                    }
                }
            });
        } else if (curSearchKey.length == 0) {
            curLiveList.empty();
        } else if (curKey == 40) {
            curLiveList.find('li').removeClass('selected');
            if (!curLiveSelected.length || curLiveSelected.is(':last-child')) {
                current = curLiveList.find('li').eq(0);
            } else {
                current = curLiveSelected.next();
            }
            $(current).addClass('selected');
        } else if (curKey == 38) {
            curLiveList.find('li').removeClass('selected');
            if (!curLiveSelected.length || curLiveSelected.is(':first-child')) {
                current = curLiveList.find('li').last();
            } else {
                current = curLiveSelected.prev();
            }
            $(current).addClass('selected');
        } else if (curKey == 13) {
            $('.pop_overlay').fadeIn();
            var custID = curLiveList.find('li.selected').attr('data-cust_id');
            $.ajax({
                type: 'POST',
                url: '../../services/customer_live.php',
                data: {
                    apiKey: "api123",
                    custID: custID
                },
                success: function (custDet) {
                    if (custDet.status = "ok") {
                        curSearchBox.val(custDet.customerDetails.accountName);
                        curForm.find('input[name="customerId"]').val(custDet.customerDetails.ID);
                        curLiveList.empty();
                        $('.pop_overlay').fadeOut();
                    }
                }
            });
        }
    });

    $(document).on('mouseenter', '.aj_live_items > li', function () {
        var curLi = $(this);
        $('.aj_live_items > li').not(curLi).removeClass('selected');
        curLi.addClass('selected');
    });
    $(document).on('mouseleave', '.aj_live_items > li', function () {
        var curLi = $(this);
        curLi.removeClass('selected');
    });
    // Search customer with id
    $(document).on('keyup', '.aj_rel_box input[name="customerId"]', function (e) {
        var curSearchBox = $(this),
            curForm = curSearchBox.closest('form'),
            curSearchKey = curSearchBox.val(),
            curLiveList = curSearchBox.siblings('ul.aj_live_items'),
            curKey = e.keyCode,
            curLiveSelected = curLiveList.find('li').filter('.selected'),
            current;
        if (curKey != 40 && curKey != 38 && curKey != 13 && curSearchKey.length > 0 && typeof parseInt(curSearchKey) == "number") {
            $.ajax({
                type: 'Post',
                url: '../../services/customer_live_by_id.php',
                data: {
                    apiKey: "api123",
                    searchKey: curSearchKey
                },
                success: function (custResult) {
                    if (custResult.status == "ok") {
                        curLiveList.empty();
                        var custObj = custResult.custList;
                        $.each(custObj, function (keyText, keyValue) {
                            curLiveList.append('<li data-cust_id="' + keyValue.id + '">' + keyValue.accountName + '<span class="pass_mobile"><span></span>' + keyValue.id + '</span></li>');
                        });
                    }
                }
            });
        } else if (curSearchKey.length == 0) {
            curLiveList.empty();
        } else if (curKey == 40) {
            curLiveList.find('li').removeClass('selected');
            if (!curLiveSelected.length || curLiveSelected.is(':last-child')) {
                current = curLiveList.find('li').eq(0);
            } else {
                current = curLiveSelected.next();
            }
            $(current).addClass('selected');
        } else if (curKey == 38) {
            curLiveList.find('li').removeClass('selected');
            if (!curLiveSelected.length || curLiveSelected.is(':first-child')) {
                current = curLiveList.find('li').last();
            } else {
                current = curLiveSelected.prev();
            }
            $(current).addClass('selected');
        } else if (curKey == 13) {
            $('.pop_overlay').fadeIn();
            var custID = curLiveList.find('li.selected').attr('data-cust_id');
            $.ajax({
                type: 'POST',
                url: '../../services/customer_live_by_id.php',
                data: {
                    apiKey: "api123",
                    custID: custID
                },
                success: function (custDet) {
                    if (custDet.status = "ok") {
                        curSearchBox.val(custDet.customerDetails.ID);
                        //curForm.find('input[name="customerId"]').val(custDet.customerDetails.ID);
                        curLiveList.empty();
                        $('.pop_overlay').fadeOut();
                    }
                }
            });
        }
    });
    // Click event for customer
    $(document).on('click', 'form .aj_customer .aj_live_items li', function () {
        $('.pop_overlay').fadeIn();
        var curCustomer = $(this),
            curForm = curCustomer.closest('form'),
            curCustID = curCustomer.attr('data-cust_id'),
            curLiveList = curCustomer.parent(),
            curSearchBox = curLiveList.siblings('input[name="customerName"]'),
            curCustomerIDBox = curForm.find('input[name="customerId"]');
        $.ajax({
            type: 'POST',
            url: '../../services/customer_live.php',
            data: {
                apiKey: "api123",
                custID: curCustID
            },
            success: function (custDet) {
                if (custDet.status = "ok") {
                    curForm.find('input[name="customerName"]').val(custDet.customerDetails.accountName);
                    curForm.find('input[name="customerId"]').val(custDet.customerDetails.ID);
                    curLiveList.empty();
                    $('.pop_overlay').fadeOut();
                }
            }
        });
    });


    // Click event for Passenger
    $(document).on('click', 'form .aj_passenger .aj_live_items li', function () {
        $('.pop_overlay').fadeIn();
        var curPassenger = $(this),
            curLiveList = curPassenger.parent(),
            curSearchBox = curLiveList.siblings('input[name="passengerName"]'),
            curForm = curSearchBox.closest('form'),
            passID = curPassenger.attr('data-pass_id');
        $.ajax({
            type: 'POST',
            url: '../../services/passenger_live.php',
            data: {
                apiKey: "api123",
                passengerID: passID
            },
            success: function (passengerDet) {
                if (passengerDet.status == "ok") {
                    var passengerObj = passengerDet.passengerDetails;
                    $.each(passengerObj, function (keyTerm, keyValue) {
                        if (curForm.find('*[name="' + keyTerm + '"]').length > 0) {
                            curForm.find('*[name="' + keyTerm + '"]').val(keyValue);
                        }
                    });
                    curLiveList.empty();
                    if (curForm.attr('data-form_for') == "age_based") {
                        ageBasedRateTrigger(curForm.find('input[name="age"]').val());
                    }
                    $('.pop_overlay').fadeOut();
                }
            }
        });
    });

    // Updating row count in cahs payment table
    function cashPaymentRowCountUpdator() {
        var curForm = $('#cash_payment'),
            curTable = curForm.find('#tbl_cash_payment');
        curForm.find('input#cash_payment_row_count').val(curTable.find('tbody > tr').length);
    }

    // Adding multiple rows in cash payment table
    $(document).on('click', '#add_cash_payment_row', function (e) {
        e.preventDefault();
        var curAddBtn = $(this),
            curTable = curAddBtn.parents('table#tbl_cash_payment'),
            curRowLength = curTable.find('tbody > tr').length,
            appendTr = $('#cash_payment_common_row table > tbody').html();
        curTable.find('tbody').append(appendTr);
        curTable.find('tbody > tr:last td:first').text(curRowLength + 1);
        curTable.find('tbody > tr:last input').each(function () {
            var curFieldNameArray = $(this).attr('name').split('_');
            $(this).attr('name', curFieldNameArray[0] + '_' + (curRowLength + 1));
        });
        curTable.find('tbody > tr:last textarea').each(function () {
            var curFieldNameArray = $(this).attr('name').split('_');
            $(this).attr('name', curFieldNameArray[0] + '_' + (curRowLength + 1));
        });
        curTable.find('tbody > tr:last select').each(function () {
            var curFieldNameArray = $(this).attr('name').split('_');
            $(this).attr('name', curFieldNameArray[0] + '_' + (curRowLength + 1));
        });
        curTable.find('tbody > tr:last input[data-dp="datepick"]').each(function () {
            $(this).datepicker({
                dateFormat: "dd-mm-yy",
                changeMonth: true,
                changeYear: true
            });
        });
        cashPaymentRowCountUpdator();
    });
    // Delete row in cash payment table
    $(document).on('click', '#tbl_cash_payment > tbody > tr > td > button[data-action="del_cash_payment_row"]', function (e) {
        e.preventDefault();
        var curDelBtn = $(this),
            curRow = curDelBtn.parents('tr'),
            curTable = curDelBtn.parents('table#tbl_cash_payment'),
            curRowIndex = curRow.index(),
            curRowLength = curTable.find('tbody > tr').length,
            appendTr = $('#cash_payment_common_row table > tbody').html();
        if ((curRowLength == 1) || (curRowLength == (curRowIndex + 1))) {
            curRow.remove();
            /*curTable.find('tbody').append(appendTr);
             curTable.find('tbody > tr:last input').each(function () {
             $(this).attr('name', $(this).attr('name') + '_1');
             });
             curTable.find('tbody > tr:last textarea').each(function () {
             $(this).attr('name', $(this).attr('name') + '_1');
             });
             curTable.find('tbody > tr:last select').each(function () {
             $(this).attr('name', $(this).attr('name') + '_1');
             });
             curTable.find('tbody > tr:last input[data-dp="datepick"]').each(function () {
             $(this).datepicker({
             dateFormat: "dd-mm-yy",
             changeMonth: true,
             changeYear: true
             });
             });*/
            netCahsPaymentAmt();
            cashPaymentRowCountUpdator();
        } else if ((curRowLength > 1) && (curRowLength != (curRowIndex + 1))) {
            curRow.remove();
            var i;
            for (i = curRowIndex; i < curRowLength; i++) {
                curTable.find('tbody > tr:nth-child(' + i + ') input').each(function () {
                    var curFieldNameArray = $(this).attr('name').split('_');
                    $(this).attr('name', curFieldNameArray[0] + '_' + i);
                });
                curTable.find('tbody > tr:nth-child(' + i + ') textarea').each(function () {
                    var curFieldNameArray = $(this).attr('name').split('_');
                    $(this).attr('name', curFieldNameArray[0] + '_' + i);
                });
                curTable.find('tbody > tr:nth-child(' + i + ') select').each(function () {
                    var curFieldNameArray = $(this).attr('name').split('_');
                    $(this).attr('name', curFieldNameArray[0] + '_' + i);
                });
                curTable.find('tbody > tr:nth-child(' + i + ') > td:first').text(i);
            }
            netCahsPaymentAmt();
            cashPaymentRowCountUpdator();
        }
    });
    // Updating row count in cash receipt table
    function cashReceiptRowCountUpdator() {
        var curForm = $('#cash_receipt'),
            curTable = curForm.find('#tbl_cash_receipt');
        curForm.find('input#cash_receipt_row_count').val(curTable.find('tbody > tr').length);
    }

    // Adding multiple rows in cash receipt table
    $(document).on('click', '#add_cash_receipt_row', function (e) {
        e.preventDefault();
        var curAddBtn = $(this),
            curTable = curAddBtn.parents('table#tbl_cash_receipt'),
            curRowLength = curTable.find('tbody > tr').length,
            appendTr = $('#cash_receipt_common_row table > tbody').html();
        curTable.find('tbody').append(appendTr);
        curTable.find('tbody > tr:last td:first').text(curRowLength + 1);
        curTable.find('tbody > tr:last input').each(function () {
            var curFieldNameArray = $(this).attr('name').split('_');
            $(this).attr('name', curFieldNameArray[0] + '_' + (curRowLength + 1));
        });
        curTable.find('tbody > tr:last textarea').each(function () {
            var curFieldNameArray = $(this).attr('name').split('_');
            $(this).attr('name', curFieldNameArray[0] + '_' + (curRowLength + 1));
        });
        curTable.find('tbody > tr:last select').each(function () {
            var curFieldNameArray = $(this).attr('name').split('_');
            $(this).attr('name', curFieldNameArray[0] + '_' + (curRowLength + 1));
        });
        curTable.find('tbody > tr:last input[data-dp="datepick"]').each(function () {
            $(this).datepicker({
                dateFormat: "dd-mm-yy",
                changeMonth: true,
                changeYear: true
            });
        });
        cashReceiptRowCountUpdator();
    });
    // Delete row in cash receipt table
    $(document).on('click', '#tbl_cash_receipt > tbody > tr > td > button[data-action="del_cash_receipt_row"]', function (e) {
        e.preventDefault();
        var curDelBtn = $(this),
            curRow = curDelBtn.parents('tr'),
            curTable = curDelBtn.parents('table#tbl_cash_receipt'),
            curRowIndex = curRow.index(),
            curRowLength = curTable.find('tbody > tr').length,
            appendTr = $('#cash_receipt_common_row table > tbody').html();
        if ((curRowLength == 1) || (curRowLength == (curRowIndex + 1))) {
            curRow.remove();
            /*curTable.find('tbody').append(appendTr);
             curTable.find('tbody > tr:last input').each(function () {
             $(this).attr('name', $(this).attr('name') + '_1');
             });
             curTable.find('tbody > tr:last textarea').each(function () {
             $(this).attr('name', $(this).attr('name') + '_1');
             });
             curTable.find('tbody > tr:last select').each(function () {
             $(this).attr('name', $(this).attr('name') + '_1');
             });
             curTable.find('tbody > tr:last input[data-dp="datepick"]').each(function () {
             $(this).datepicker({
             dateFormat: "dd-mm-yy",
             changeMonth: true,
             changeYear: true
             });
             });*/
            netCahsReceiptAmt();
            cashReceiptRowCountUpdator();
        } /*else if ((curRowLength > 1) && (curRowLength == (curRowIndex + 1))) {
         curRow.remove();
         cashReceiptRowCountUpdator();
         } */else if ((curRowLength > 1) && (curRowLength != (curRowIndex + 1))) {
            curRow.remove();
            var i;
            for (i = curRowIndex; i < curRowLength; i++) {
                curTable.find('tbody > tr:nth-child(' + i + ') input').each(function () {
                    var curFieldNameArray = $(this).attr('name').split('_');
                    $(this).attr('name', curFieldNameArray[0] + '_' + i);
                });
                curTable.find('tbody > tr:nth-child(' + i + ') textarea').each(function () {
                    var curFieldNameArray = $(this).attr('name').split('_');
                    $(this).attr('name', curFieldNameArray[0] + '_' + i);
                });
                curTable.find('tbody > tr:nth-child(' + i + ') select').each(function () {
                    var curFieldNameArray = $(this).attr('name').split('_');
                    $(this).attr('name', curFieldNameArray[0] + '_' + i);
                });
                curTable.find('tbody > tr:nth-child(' + i + ') > td:first').text(i);
            }
            netCahsReceiptAmt();
            cashReceiptRowCountUpdator();
        }
    });
    $(document).on('keyup input', '#cash_receipt #tbl_cash_receipt tbody > tr > td input[data-action="trigger_cash_calculation"]', function () {
        netCahsReceiptAmt();
    });
    $(document).on('keyup','.aj_account_from input[type="text"]', function (e) {
        var curSearchBox = $(this),
            curForm = curSearchBox.closest('form'),
            curSearchKey = curSearchBox.val(),
            curLiveList = curSearchBox.siblings('ul.aj_live_items'),
            curKey = e.keyCode,
            curLiveSelected = curLiveList.find('li').filter('.selected'),
            current;
        if (curKey != 40 && curKey != 38 && curKey != 13 && curSearchKey.length > 0) {
            $.ajax({
                type: 'POST',
                url: '../../services/account_from_live.php',
                data: {
                    apiKey: "api123",
                    searchKey: curSearchKey
                },
                success: function (result) {
                    if (result.status == "ok") {
                        curLiveList.empty();
                        $.each(result.acc_details, function(k, v){
                            curLiveList.append('<li data-parent_id="'+v.id+'" data-acc_name="'+ v.acc_name +'">'+ v.acc_name+'<span class="pass_mobile"><span></span>'+ v.id +'</span></li>');
                        });
                    } else if (result.status == "failure") {
                        curLiveList.empty();
                    }
                }
            });
        } else if (curSearchKey.length == 0) {
            curLiveList.empty();
        } else if (curKey == 40) {
            curLiveList.find('li').removeClass('selected');
            if (!curLiveSelected.length || curLiveSelected.is(':last-child')) {
                current = curLiveList.find('li').eq(0);
            } else {
                current = curLiveSelected.next();
            }
            $(current).addClass('selected');
        } else if (curKey == 38) {
            curLiveList.find('li').removeClass('selected');
            if (!curLiveSelected.length || curLiveSelected.is(':first-child')) {
                current = curLiveList.find('li').last();
            } else {
                current = curLiveSelected.prev();
            }
            $(current).addClass('selected');
        } else if (curKey == 13) {
            var curPassenger = curLiveList.find('li:selected').attr('data-acc_name'),
                parentId = curLiveList.find('li.selected').attr('data-parent_id');
            curForm.find('#accountFromName').val(curPassenger);
            curForm.find('#accountFrom').val(parentId);
            curLiveList.empty();
        }
    });

    $(document).on('click', '.aj_account_from .aj_live_items li', function () {
        var curAccTo = $(this),
            curAccName = curAccTo.attr('data-acc_name'),
            curAccId = curAccTo.attr('data-parent_id'),
            curForm = curAccTo.closest('form');
        curForm.find('#accountFromName').val(curAccName);
        curForm.find('#accountFrom').val(curAccId);
        curAccTo.parents('.aj_live_items').empty();
    });

    $(document).on('keyup', '#cash_receipt #accountFromName', function() {
        if ($(this).val() != "") {
            $(this).nextAll('p.validation_mes').removeClass('active');
        }
    });


    $(document).on('click', '#cash_receipt #get_bill_details', function (e) {
        e.preventDefault();
        var curBillBtn = $(this),
            curForm = curBillBtn.parents('form#cash_receipt'),
            curAcFrom = curForm.find('input[name="accountFrom"]'),
            curPop = $(curBillBtn.attr('data-target')),
            flag = false;
        if (curAcFrom.val().length == 0 && isNaN(parseInt(curAcFrom))) {
            flag = true;
            curAcFrom.nextAll('p.validation_mes').addClass('active');
        }
        if (flag == true) {
            triggerError("error", "Please provide valid details");
        } else {
            var customerID = curAcFrom.val();
            $.ajax({
                type: 'POST',
                url: '../../services/cash_receipt_live.php',
                data: {
                    apiKey: "api123",
                    custID: customerID
                },
                success: function (result) {
                    if (result.status == "ok" && result.bill_count > 0) {
                        curPop.find('#bill_list tbody').empty();
                        $.each(result.bill_list, function (k, v){
                            curPop.find('#bill_list tbody').append('<tr>'+
                                '<td class="text-center">'+ '<input type="checkbox"></td>'+
                                '<td>'+ v.inv_prefix + v.invoice_no +'</td>'+
                                '<td>'+ v.invoice_date +'</td>'+
                                '<td>'+ v.inv_type +'</td>'+
                                '<td>'+ v.details +'</td>'+
                                '<td>'+ v.net_amount +'</td>'+
                                '<td>'+ v.paid_amount +'</td>'+
                                '<td>'+ (v.net_amount - v.paid_amount)  +'</td>'+
                                '</tr>');
                        });
                        curPop.modal('show');
                    } else if (result.status == "ok" && result.bill_count == 0) {
                        triggerError("error", "No bills found unpaid.")
                    }
                },
                error: function (error) {
                    triggerError("error", "Please provide valid details");
                }
            });
        }
    });

    // Passing details of selected bills in cash Receipt
    $(document).on('click', '#bill_list_pop_receipt #trigger_cash_payment', function () {
        if ($(this).parents('#bill_list_pop_receipt').find('tbody input[type="checkbox"]:checked').length == 0) {
            triggerError("error", "Please select atleast one bill to pay.");
        } else {
            var curPop = $(this).parents('#bill_list_pop_receipt'),
                curPopTable = curPop.find('table'),
                curCashTable = $('#cash_receipt #tbl_cash_receipt');
            curCashTable.find('tbody').empty();
            curPopTable.find('tbody > tr').each(function () {
                var curReceiptRow = $(this);
                if (curReceiptRow.find('td:first input[type="checkbox"]').prop('checked') == true) {
                    curCashTable.find('tbody').append('<tr>'+
                        '<td></td>'+
                        '<td><input type="text" name="invoiceNo" data-rel="bill_live" value="'+ curReceiptRow.find('td:nth-child(2)').text() +'" readonly></td>'+
                        '<td><input type="text" data-dp="datepick" name="invoiceDate" value="'+ curReceiptRow.find('td:nth-child(3)').text() +'" readonly></td>'+
                        '<td><input type="text"name="invoiceType" value="'+ curReceiptRow.find('td:nth-child(4)').text() +'" readonly></td>'+
                        '<td><input type="text" name="total" value="'+ curReceiptRow.find('td:nth-child(6)').text() +'" readonly></td>'+
                        '<td><input type="text" name="totalPaid" value="'+ curReceiptRow.find('td:nth-child(7)').text() +'" readonly></td>'+
                        '<td><input type="text" data-action="trigger_cash_calculation" name="payingAmount" value="'+ curReceiptRow.find('td:nth-child(8)').text() +'"></td>'+
                        '<td><input type="text" data-action="trigger_cash_calculation" name="discount" value="0"></td>'+
                        '<td><textarea name="remark"></textarea></td>'+
                        '<td><button class="bd_btn bd_btn_red" type="button" data-action="del_cash_receipt_row">Delete</button></td>'+
                        '</tr>');
                }
            });
            curCashTable.find('tbody > tr').each(function () {
                $(this).find('td:first').text($(this).index() + 1);
                $(this).find('input[type="text"]').each(function () {
                    $(this).attr('name', $(this).attr('name')+'_'+ ($(this).parents('tr').index() + 1));
                });
                $(this).find('textarea').each(function () {
                    $(this).attr('name', $(this).attr('name')+'_'+ ($(this).parents('tr').index() + 1));
                });
            });
            netCahsReceiptAmt();
            cashReceiptRowCountUpdator();
            curPop.modal('hide');
        }
    });
    // Live bill info for cash Receipt
    /*$(document).on('focusout', '#tbl_cash_receipt input[data-rel="bill_live"]', function () {
     var curBillNoBox = $(this),
     curBillNo = curBillNoBox.val(),
     curRow = curBillNoBox.parents('tr'),
     curRowIndex = curRow.index(),
     curTable = curBillNoBox.parents('table'),
     curInvType = curRow.find('select[name="invoiceType_' + (curRowIndex + 1) + '"]').val();
     if (typeof parseInt(curBillNo) == "number") {
     $.ajax({
     type: 'POST',
     url: '../../services/cash_receipt_live.php',
     data: {
     apiKey: "api123",
     billNo: curBillNo,
     invType: curInvType
     },
     success: function (billDetails) {
     console.log(billDetails);
     if (billDetails.status == "ok") {
     curRow.find('input[name="invoiceDate_' + (curRowIndex + 1) + '"]').val(billDetails.bill_details.invoice_date);
     curRow.find('input[name="total_' + (curRowIndex + 1) + '"]').val(parseFloat(billDetails.bill_details.net_amount));
     curRow.find('input[name="totalPaid_' + (curRowIndex + 1) + '"]').val(parseFloat(billDetails.bill_details.paid_amount));
     curRow.find('input[name="payingAmount_' + (curRowIndex + 1) + '"]').val((parseFloat(billDetails.bill_details.net_amount) - parseFloat(billDetails.bill_details.paid_amount)).toFixed(2));
     netCahsReceiptAmt();
     } else if (billDetails.status == "failure") {
     triggerError('error', billDetails.error);
     }
     }
     });
     }
     });*/
    // Adding dynamic form elements in package
    $(document).on('click', '.package_serv_controls input[type="checkbox"]', function () {
        var curCheckBox = $(this),
            curServTarget = curCheckBox.attr('data-append'),
            curServTargetLayout = $('.package_dynamic_source .' + curServTarget)[0].outerHTML,
            appendTarget = $('#package_dynamic');
        if (curCheckBox.prop('checked') == true) {
            appendTarget.append(curServTargetLayout);
        } else {
            appendTarget.find('.' + curServTarget).remove();
        }
        appendTarget.find('.pack_dynamic_airline input[data-dp="date_pick"], .pack_dynamic_hotel input[data-dp="date_pick"]').addClass('user_date').datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true
        });
        netPackageCreationAmt();
    });

    // Inserting dynamic forms into the modal when selecting package in Package Invoice
    $(document).on('change', '#invoice_package select[name="package"]', function () {
        var curselect = $(this),
            curForm = curselect.parents('form'),
            curPackOption = curselect.find('option:selected'),
            curPackageID = curselect.val(),
            curTargetInPop = $('#inv_package_passenger_pop #inv_pack_dynamic_form_target');
        $('#inv_pack_pop_passenger_details > .pass_to_table').each(function () {
            $(this).val("").removeAttr('value');
        });
        curTargetInPop.empty();
        $('#inv_package_paasenger_table > tbody').empty();
        if (typeof curPackOption.attr('data-dynamic_form') !== typeof undefined && curPackOption.attr('data-dynamic_form') !== false) {
            var curService = curPackOption.attr('data-dynamic_form'),
                curServiceArray = curService.split(','),
                curServiceObj = {
                    airline: '.inv_pack_dynamic_airline',
                    hotel: '.inv_pack_dynamic_hotel',
                    visa: '.inv_pack_dynamic_visa',
                    transportation: '.inv_pack_dynamic_transportation'
                },
                i;
            if (curServiceArray.length > 0) {
                for (i = 0; i < curServiceArray.length; i++) {
                    curTargetInPop.append($(curServiceObj[curServiceArray[i]])[0].outerHTML);
                }
                curTargetInPop.find('input[data-dp="date_pic"]').each(function () {
                    $(this).addClass('user_date').datepicker({
                        dateFormat: "dd-mm-yy",
                        changeMonth: true,
                        changeYear: true
                    });
                });
                $.ajax({
                    type: 'POST',
                    url: '../../services/inv_package_rates.php',
                    data: {
                        apiKey: "api123",
                        services: curService,
                        packageID: curPackageID
                    },
                    success: function (rateResult) {
                        if (rateResult.status == "ok") {
                            $('.pop_overlay').fadeIn();
                            var packageRate = parseFloat(rateResult.general_charge);
                            if (curServiceArray.indexOf('airline') != -1) {
                                curTargetInPop.find('.inv_pack_dynamic_airline input[name="airInvoice"]').attr('data-adult_total', rateResult.rates.airline.rate.adult_total).attr('data-child_total', rateResult.rates.airline.rate.child_total).attr('data-infant_total', rateResult.rates.airline.rate.infant_total).val(rateResult.rates.airline.rate.adult_total);
                                curTargetInPop.find('.inv_pack_dynamic_airline input[name="airPurchase"]').attr('data-adult_total', rateResult.rates.airline.purchase.adult_total).attr('data-child_total', rateResult.rates.airline.purchase.child_total).attr('data-infant_total', rateResult.rates.airline.purchase.infant_total).val(rateResult.rates.airline.purchase.adult_total);
                                packageRate = packageRate + parseFloat(rateResult.rates.airline.rate.adult_total);
                            }
                            if (curServiceArray.indexOf('visa') != -1) {
                                curTargetInPop.find('.inv_pack_dynamic_visa input[name="visaInvoice"]').val(rateResult.rates.visa.inv_total);
                                curTargetInPop.find('.inv_pack_dynamic_visa input[name="visaPurchase"]').val(rateResult.rates.visa.pur_total);
                                packageRate = packageRate + parseFloat(rateResult.rates.visa.inv_total);
                            }
                            if ((curServiceArray.indexOf('transportation') != -1) && (curTargetInPop.find('.inv_pack_dynamic_transportation input[name="seatStatus"]').prop('checked') == false)) {
                                curTargetInPop.find('.inv_pack_dynamic_transportation input[name="transInvoice"]').val(0).attr('data-inv_total', rateResult.rates.transportation.inv_total);
                                curTargetInPop.find('.inv_pack_dynamic_transportation input[name="transPurchase"]').val(0).attr('data-pur_total', rateResult.rates.transportation.pur_total);
                                packageRate = packageRate + (parseFloat(rateResult.rates.transportation.inv_total));
                            } else if ((curServiceArray.indexOf('transportation') != -1) && (curTargetInPop.find('.inv_pack_dynamic_transportation input[name="seatStatus"]').prop('checked') == true)) {
                                curTargetInPop.find('.inv_pack_dynamic_transportation input[name="transInvoice"]').val(rateResult.rates.transportation.total).attr('data-inv_total', rateResult.rates.transportation.inv_total);
                                curTargetInPop.find('.inv_pack_dynamic_transportation input[name="transPurchase"]').val(rateResult.rates.transportation.purchase).attr('data-pur_total', rateResult.rates.transportation.pur_total);
                                packageRate = packageRate + (parseFloat(rateResult.rates.transportation.inv_total));
                            }
                            if (curServiceArray.indexOf('hotel') != -1) {
                                //curTargetInPop.find('.inv_pack_dynamic_hotel input[name="hotInvoice"]').attr('data-adult_total', rateResult.rates.hotel.rate.adult_total).attr('data-child_wb_total', rateResult.rates.hotel.rate.child_wb_total).attr('data-child_wob_total', rateResult.rates.hotel.rate.child_wob_total);
                                //curTargetInPop.find('.inv_pack_dynamic_hotel input[name="hotPurchase"]').attr('data-adult_total', rateResult.rates.hotel.purchase.adult_total).attr('data-child_wb_total', rateResult.rates.hotel.purchase.child_wb_total).attr('data-child_wob_total', rateResult.rates.hotel.purchase.child_wob_total);
                                //curTargetInPop.find('.inv_pack_dynamic_hotel select[name="roomType"]').append('<option value="">Select</option>');
                                $.each(rateResult.rates.hotel, function (k, v) {
                                    curTargetInPop.find('.inv_pack_dynamic_hotel select[name="roomType"]').append('<option value="' + v.room_id + '" data-room_rate="' + v.room_inv_total + '" data-room_pur_total="' + v.room_pur_total + '">' + v.room_type + '</option>');
                                });
                                packageRate = packageRate + parseFloat(rateResult.rates.hotel[0].room_inv_total);
                            }
                            curTargetInPop.parents('#inv_package_passenger_pop').attr('data-general_amt', rateResult.general_charge).attr('data-additional_extra', rateResult.extra).attr('data-additional_discount', rateResult.discount);
                            curForm.find('#package_rate_show').text(packageRate + parseFloat(rateResult.extra) - parseFloat(rateResult.discount));
                            $('.pop_overlay').hide();
                        }
                    },
                    error: function (error) {
                        triggerError("error", "Something went wrong. Please try again.");
                    }
                });
            }
        }
    });

    // Updating hotel rates in package invoice according to room type
    /*$(document).on('change', '#inv_package_passenger_pop_form select[name="roomType"]', function () {
     var curSelect = $(this),
     curOption = curSelect.find('option:selected'),
     curInvRate = parseFloat(curOption.attr('data-room_rate')),
     curPurRate = parseFloat(curOption.attr('data-room_pur_total'));
     curSelect.parents('.inv_pack_dynamic_hotel').find('input[name="hotInvoice"]').val(curInvRate);
     curSelect.parents('.inv_pack_dynamic_hotel').find('input[name="hotPurchase"]').val(curPurRate);
     });*/

    // Updating transportation rates with seat status in transportation in invoice package
    $(document).on('click', '#inv_package_passenger_pop .inv_pack_dynamic_transportation input[name="seatStatus"]', function (e) {
        var curseatCheck = $(this);
        if ($(this).prop('checked') == true) { curseatCheck.parents('.inv_pack_dynamic_transportation').find('input[name="transInvoice"]').val(curseatCheck.parents('.inv_pack_dynamic_transportation').find('input[name="transInvoice"]').attr('data-inv_total'));
            curseatCheck.parents('.inv_pack_dynamic_transportation').find('input[name="transPurchase"]').val(curseatCheck.parents('.inv_pack_dynamic_transportation').find('input[name="transPurchase"]').attr('data-pur_total'));
        } else {
            curseatCheck.parents('.inv_pack_dynamic_transportation').find('input[name="transInvoice"]').val(0);
            curseatCheck.parents('.inv_pack_dynamic_transportation').find('input[name="transPurchase"]').val(0);
        }
    });

    // Updating hotel rate according to age
    //$('#inv_package_passenger_pop .inv_pack_dynamic_hotel input[type="checkbox"]')
    $(document).on('click', '#inv_package_passenger_pop .inv_pack_dynamic_hotel input[type="checkbox"]', function () {
        var curCheck = $(this),
            curHotelParent = curCheck.parents('.inv_pack_dynamic_hotel');
        if ((!curCheck[0].hasAttribute('disabled')) && (curCheck.parents('#inv_package_passenger_pop').find('input[name="age"]').val() < 18) && (curCheck.prop('checked') == true)) {
            curHotelParent.find('input[name="hotInvoice"]').val(curHotelParent.find('input[name="hotInvoice"]').attr('data-child_wb_total'));
            curHotelParent.find('input[name="hotPurchase"]').val(curHotelParent.find('input[name="hotPurchase"]').attr('data-child_wb_total'));
        } else if ((!curCheck[0].hasAttribute('disabled')) && (curCheck.parents('#inv_package_passenger_pop').find('input[name="age"]').val() < 18) && (curCheck.prop('checked') == false)) {
            curHotelParent.find('input[name="hotInvoice"]').val(curHotelParent.find('input[name="hotInvoice"]').attr('data-child_wob_total'));
            curHotelParent.find('input[name="hotPurchase"]').val(curHotelParent.find('input[name="hotPurchase"]').attr('data-child_wob_total'));
        }
    });

    $(document).on('change', '#inv_package_passenger_pop .inv_pack_dynamic_hotel select[name="roomType"]', function () {
        var curSelect = $(this);
        if (curSelect.find('option:selected')[0].hasAttribute('data-room_rate')) {
            console.log
            var curOption = curSelect.find('option:selected').attr('data-room_rate');
            curSelect.parents('.inv_pack_dynamic_hotel').find('input[name="hotInvoice"]').val(curOption);
            curSelect.parents('.inv_pack_dynamic_hotel').find('input[name="hotPurchase"]').val(curOption);
        }
    });
    // Triggering add new passenger details button in package invoice
    $(document).on('click', '#inv_pack_modal_trigger', function (e) {
        e.preventDefault();
        var popModal = $($(this).attr('data-modal_target'));
        popModal.find('#trigger_pass_table').addClass('add_to_pass_table');
        $('#inv_pack_pop_passenger_details .pass_to_table').each(function () {
            $(this).val("").removeAttr('value');
        });
        //popModal.find('.inv_pack_dynamic_airline input').val("").removeAttr('value');
        //popModal.find('.inv_pack_dynamic_hotel input').val("").removeAttr('value');
        //popModal.find('.inv_pack_dynamic_visa .inv_pack_visa_basics input').val("").removeAttr('value');
        //popModal.find('.inv_pack_dynamic_hotel input[type="checkbox"]').prop('checked', false);
        //popModal.find('.inv_pack_dynamic_hotel select[name="roomType"] option').not(popModal.find('.inv_pack_dynamic_hotel select[name="roomType"] option:first')).removeAttr('selected');
        //popModal.find('.inv_pack_dynamic_hotel select[name="roomType"] option:first').attr('selected', 'selected');
        popModal.find('.inv_pack_dynamic_transportation input[name="seatStatus"]').prop('checked', true);
        popModal.find('.inv_pack_dynamic_transportation input[name="transInvoice"]').val(popModal.find('.inv_pack_dynamic_transportation input[name="transInvoice"]').attr('data-inv_total'));
        popModal.find('.inv_pack_dynamic_transportation input[name="transPurchase"]').val(popModal.find('.inv_pack_dynamic_transportation input[name="transPurchase"]').attr('data-pur_total'));
        popModal.modal('show');
    });
    // Adding passenger row in package invoice
    $(document).on('click', '#inv_package_passenger_pop .add_to_pass_table', function (e) {
        e.preventDefault();
        var curCloseBtn = $(this),
            curPopUp = curCloseBtn.parents('.modal'),
            curPassTable = $('#inv_package_paasenger_table'),
            curRowCount = curPassTable.find('tbody > tr').length,
            passengerFields = {},
            curPassInvTotal = 0,
            curPassPurTotal = 0,
            curGeneralAmt = parseFloat(curPopUp.attr('data-general_amt')),
            curAdditionalExtra = parseFloat(curPopUp.attr('data-additional_extra')),
            curAdditionalDiscount = parseFloat(curPopUp.attr('data-additional_discount'));
        curPopUp.find('.pass_to_table').each(function () {
            var fieldName = $(this).attr('name'),
                fieldValue = $(this).val();
            passengerFields[fieldName] = fieldValue;
        });
        if (Object.keys(passengerFields).length > 0 && curRowCount == 0) {
            curPassTable.find('tbody').append('<tr data-additional_extra="' + curAdditionalExtra + '" data-additional_discount="' + curAdditionalDiscount + '">' +
                '<td><span class="sl_no">1</span></td>' +
                '<td>' + passengerFields.passengerName + '</td>' +
                '<td>' + passengerFields.age + '</td>' +
                '<td>' + passengerFields.passportNo + '</td>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td>' +
                '<a class="show_table_lnk show_table_lnk_view" href="#">View</a>' +
                '<a class="show_table_lnk show_table_lnk_edit" href="#">Edit</a>' +
                '<a class="show_table_lnk show_table_lnk_del" href="#">Delete</a>' +
                '</td>' +
                '</tr>');
            $.each(passengerFields, function (k, v) {
                curPassTable.find('tbody tr:first > td:first').append('<input type="hidden" data-field_name="' + k + '" name="pass1_' + k + '" value="' + v + '" />');
            });
            if (curPopUp.find('.inv_pack_dynamic_airline').length > 0) {
                var airInvoiceTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_airline input[name="airInvoice"]').val()),
                    airPurchaseTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_airline input[name="airPurchase"]').val());
                curPassTable.find('tbody > tr:first > td:nth-child(5)').text(curPopUp.find('.inv_pack_dynamic_airline input[name="ticketNo"]').val());
                curPassTable.find('tbody > tr:first > td:nth-child(6)').text(airInvoiceTotal);
                curPassInvTotal = curPassInvTotal + airInvoiceTotal;
                curPassPurTotal = curPassPurTotal + airPurchaseTotal;
            }
            if (curPopUp.find('.inv_pack_dynamic_hotel').length > 0) {
                var hotelInvoiceTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_hotel input[name="hotInvoice"]').val()),
                    hotelPurchaseTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_hotel input[name="hotPurchase"]').val());
                curPassTable.find('tbody > tr:first > td:nth-child(7)').text(hotelInvoiceTotal);
                curPassInvTotal = curPassInvTotal + hotelInvoiceTotal;
                curPassPurTotal = curPassPurTotal + hotelPurchaseTotal;
                if (curPopUp.find('.inv_pack_dynamic_hotel input[type="checkbox"]').prop('checked') == true) {
                    curPassTable.find('tbody > tr:first').attr('data-hotel_room_status', "wb");
                    curPassTable.find('tbody > tr:first td:nth-child(2)').append('<input type="hidden" name="pass1_bedStatus" value="1" />');
                } else {
                    curPassTable.find('tbody > tr:first').attr('data-hotel_room_status', "wob");
                    curPassTable.find('tbody > tr:first td:nth-child(2)').append('<input type="hidden" name="pass1_bedStatus" value="0" />');
                }
            }
            if (curPopUp.find('.inv_pack_dynamic_visa').length > 0) {
                var visaInvoiceTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_visa input[name="visaInvoice"]').val()),
                    visaPurchaseTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_visa input[name="visaPurchase"]').val());
                curPassTable.find('tbody > tr:first > td:nth-child(8)').text(visaInvoiceTotal);
                curPassInvTotal = curPassInvTotal + visaInvoiceTotal;
                curPassPurTotal = curPassPurTotal + visaPurchaseTotal;
            }
            if (curPopUp.find('.inv_pack_dynamic_transportation').length > 0) {
                var transInvoiceTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_transportation input[name="transInvoice"]').val()),
                    transPurchaseTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_transportation input[name="transPurchase"]').val());
                curPassTable.find('tbody > tr:first > td:nth-child(9)').text(transInvoiceTotal);
                curPassInvTotal = curPassInvTotal + transInvoiceTotal;
                curPassPurTotal = curPassPurTotal + transPurchaseTotal;
                if (curPopUp.find('.inv_pack_dynamic_transportation input[name="seatStatus"]').prop('checked') == true) {
                    curPassTable.find('tbody > tr:first').attr('data-seat_status', "on");
                    curPassTable.find('tbody > tr:first td:nth-child(2)').append('<input type="hidden" name="pass1_seatStatus" value="1" />');
                } else {
                    curPassTable.find('tbody > tr:first').attr('data-seat_status', "off");
                    curPassTable.find('tbody > tr:first td:nth-child(2)').append('<input type="hidden" name="pass1_seatStatus" value="0" />');
                }
            }
            curPassTable.find('tbody > tr:first > td:nth-child(2)').append('<input type="hidden" name="pass1_invoiceTotal" value="' + (curPassInvTotal + curGeneralAmt) + '" />');
            curPassTable.find('tbody > tr:first > td:nth-child(2)').append('<input type="hidden" name="pass1_purchaseTotal" value="' + curPassPurTotal + '" />');
            curPassTable.find('tbody > tr:first > td:nth-child(10)').text(curPassInvTotal + curGeneralAmt);
            netPackageInvoiceAmt();
            getInvPackParrRowCount();
            curPassTable.parents('form#invoice_package').find('#passCountdiv').empty();
            curPopUp.modal('hide');
        } else if (Object.keys(passengerFields).length > 0 && curRowCount > 0) {
            curPassTable.find('tbody').append('<tr data-additional_extra="' + curAdditionalExtra + '" data-additional_discount="' + curAdditionalDiscount + '">' +
                '<td><span class="sl_no">' + (curRowCount + 1) + '</span></td>' +
                '<td>' + passengerFields.passengerName + '</td>' +
                '<td>' + passengerFields.age + '</td>' +
                '<td>' + passengerFields.passportNo + '</td>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td>' +
                '<a class="show_table_lnk show_table_lnk_view" href="#">View</a>' +
                '<a class="show_table_lnk show_table_lnk_edit" href="#">Edit</a>' +
                '<a class="show_table_lnk show_table_lnk_del" href="#">Delete</a>' +
                '</td>' +
                '</tr>');
            $.each(passengerFields, function (k, v) {
                curPassTable.find('tbody tr:nth-child(' + (curRowCount + 1) + ') > td:first').append('<input type="hidden" data-field_name="' + k + '" name="pass' + (curRowCount + 1) + '_' + k + '" value="' + v + '" />');
            });
            if (curPopUp.find('.inv_pack_dynamic_airline').length > 0) {
                var airInvoiceTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_airline input[name="airInvoice"]').val()),
                    airPurchaseTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_airline input[name="airPurchase"]').val());
                curPassTable.find('tbody > tr:nth-child(' + (curRowCount + 1) + ') > td:nth-child(5)').text(curPopUp.find('.inv_pack_dynamic_airline input[name="ticketNo"]').val());
                curPassTable.find('tbody > tr:nth-child(' + (curRowCount + 1) + ') > td:nth-child(6)').text(airInvoiceTotal);
                curPassInvTotal = curPassInvTotal + airInvoiceTotal;
                curPassPurTotal = curPassPurTotal + airPurchaseTotal;
            }
            if (curPopUp.find('.inv_pack_dynamic_hotel').length > 0) {
                var hotelInvoiceTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_hotel input[name="hotInvoice"]').val()),
                    hotelPurchaseTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_hotel input[name="hotPurchase"]').val());
                curPassTable.find('tbody > tr:nth-child(' + (curRowCount + 1) + ') > td:nth-child(7)').text(hotelInvoiceTotal);
                curPassInvTotal = curPassInvTotal + hotelInvoiceTotal;
                curPassPurTotal = curPassPurTotal + hotelPurchaseTotal;
                if (curPopUp.find('.inv_pack_dynamic_hotel input[type="checkbox"]').prop('checked') == true) {
                    curPassTable.find('tbody > tr:nth-child(' + (curRowCount + 1) + ')').attr('data-hotel_room_status', "wb");
                    curPassTable.find('tbody > tr:nth-child(' + (curRowCount + 1) + ') td:nth-child(2)').append('<input type="hidden" name="pass' + (curRowCount + 1) + '_bedStatus" value="1" />');
                } else {
                    curPassTable.find('tbody > tr:nth-child(' + (curRowCount + 1) + ')').attr('data-hotel_room_status', "wob");
                    curPassTable.find('tbody > tr:nth-child(' + (curRowCount + 1) + ') td:nth-child(2)').append('<input type="hidden" name="pass' + (curRowCount + 1) + '_bedStatus" value="0" />');
                }
            }
            if (curPopUp.find('.inv_pack_dynamic_visa').length > 0) {
                var visaInvoiceTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_visa input[name="visaInvoice"]').val()),
                    visaPurchaseTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_visa input[name="visaPurchase"]').val());
                curPassTable.find('tbody > tr:nth-child(' + (curRowCount + 1) + ') > td:nth-child(8)').text(visaInvoiceTotal);
                curPassInvTotal = curPassInvTotal + visaInvoiceTotal;
                curPassPurTotal = curPassPurTotal + visaPurchaseTotal;
            }
            if (curPopUp.find('.inv_pack_dynamic_transportation').length > 0) {
                var transInvoiceTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_transportation input[name="transInvoice"]').val()),
                    transPurchaseTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_transportation input[name="transPurchase"]').val());
                curPassTable.find('tbody > tr:nth-child(' + (curRowCount + 1) + ') > td:nth-child(9)').text(transInvoiceTotal);
                curPassInvTotal = curPassInvTotal + transInvoiceTotal;
                curPassPurTotal = curPassPurTotal + transPurchaseTotal;
                if (curPopUp.find('.inv_pack_dynamic_transportation input[name="seatStatus"]').prop('checked') == true) {
                    curPassTable.find('tbody > tr:nth-child(' + (curRowCount + 1) + ')').attr('data-seat_status', "on");
                    curPassTable.find('tbody > tr:nth-child(' + (curRowCount + 1) + ') td:nth-child(2)').append('<input type="hidden" name="pass' + (curRowCount + 1) + '_seatStatus" value="1" />');
                } else {
                    curPassTable.find('tbody > tr:nth-child(' + (curRowCount + 1) + ')').attr('data-seat_status', "off");
                    curPassTable.find('tbody > tr:nth-child(' + (curRowCount + 1) + ') td:nth-child(2)').append('<input type="hidden" name="pass' + (curRowCount + 1) + '_seatStatus" value="0" />');
                }
            }
            curPassTable.find('tbody > tr:nth-child(' + (curRowCount + 1) + ') > td:nth-child(2)').append('<input type="hidden" name="pass' + (curRowCount + 1) + '_invoiceTotal" value="' + (curPassInvTotal + curGeneralAmt) + '" />');
            curPassTable.find('tbody > tr:nth-child(' + (curRowCount + 1) + ') > td:nth-child(2)').append('<input type="hidden" name="pass' + (curRowCount + 1) + '_purchaseTotal" value="' + curPassPurTotal + '" />');
            curPassTable.find('tbody > tr:nth-child(' + (curRowCount + 1) + ') > td:nth-child(10)').text(curPassInvTotal + curGeneralAmt);
            //console.log(curPassInvTotal);
            getInvPackParrRowCount();
            netPackageInvoiceAmt();
            curPopUp.modal('hide');
        }
    });

    // View action for passenger table in package Invoice
    $(document).on('click', '#inv_package_paasenger_table > tbody > tr > td > a.show_table_lnk_view', function (e) {
        e.preventDefault();
        var curViewBtn = $(this),
            curRow = curViewBtn.parents('tr'),
            curRowIndex = curRow.index(),
            curTable = curViewBtn.parents('table'),
            popupTarget = $('#inv_package_passenger_pop'),
            curPassObj = {},
            curAge = curRow.find('input[data-field_name="age"]').val();
        popupTarget.find('.pass_to_table').attr('disabled', 'disabled');
        curRow.find('td:first input[type="hidden"]').each(function () {
            var fieldName = $(this).attr('data-field_name'),
                fieldValue = $(this).val();
            curPassObj[fieldName] = fieldValue;
        });
        $.each(curPassObj, function (k, v) {
            popupTarget.find('.pass_to_table[name="' + k + '"]').val(v);
        });
        if ((curRow.attr('data-hotel_room_status') == "wb") && (popupTarget.find('.inv_pack_dynamic_hotel').length > 0)) {
            popupTarget.find('.inv_pack_dynamic_hotel input[type="checkbox"]').prop('checked', true);
        } else if ((curRow.attr('data-hotel_room_status') == "wob") && (popupTarget.find('.inv_pack_dynamic_hotel').length > 0)) {
            popupTarget.find('.inv_pack_dynamic_hotel input[type="checkbox"]').prop('checked', false);
        }
        if ((curRow.attr('data-seat_status') == "on") && (popupTarget.find('.inv_pack_dynamic_transportation').length > 0)) {
            popupTarget.find('.inv_pack_dynamic_transportation input[name="seatStatus"]').prop('checked', true);
        } else if ((curRow.attr('data-seat_status') == "off") && (popupTarget.find('.inv_pack_dynamic_transportation').length > 0)) {
            popupTarget.find('.inv_pack_dynamic_transportation input[name="seatStatus"]').prop('checked', false);
        }
        popupTarget.find('#trigger_pass_table').addClass('clear_pass_pop');
        popupTarget.modal('show');
    });
    $(document).on('click', '#inv_package_passenger_pop .clear_pass_pop', function (e) {
        e.preventDefault();
        $('#inv_package_passenger_pop').modal('hide');
    });
    // Edit action for passenger table in package invoice
    $(document).on('click', '#inv_package_paasenger_table > tbody > tr > td > a.show_table_lnk_edit', function () {
        var curEdtBtn = $(this),
            curRow = curEdtBtn.parents('tr'),
            curRowIndex = curRow.index(),
            popTarget = $('#inv_package_passenger_pop'),
            curPassObj = {};
        curRow.find('td:first input[type="hidden"]').each(function () {
            var fieldName = $(this).attr('data-field_name'),
                fieldValue = $(this).val();
            curPassObj[fieldName] = fieldValue;
        });
        $.each(curPassObj, function (k, v) {
            popTarget.find('.pass_to_table[name="' + k + '"]').val(v);
        });
        if ((curRow.attr('data-hotel_room_status') == "wb") && (popTarget.find('.inv_pack_dynamic_hotel').length > 0)) {
            popTarget.find('.inv_pack_dynamic_hotel input[type="checkbox"]').prop('checked', true);
        } else if ((curRow.attr('data-hotel_room_status') == "wob") && (popTarget.find('.inv_pack_dynamic_hotel').length > 0)) {
            popTarget.find('.inv_pack_dynamic_hotel input[type="checkbox"]').prop('checked', false);
        }
        if ((curRow.attr('data-seat_status') == "on") && (popTarget.find('.inv_pack_dynamic_transportation').length > 0)) {
            popTarget.find('.inv_pack_dynamic_transportation input[name="seatStatus"]').prop('checked', true);
        } else if ((curRow.attr('data-seat_status') == "off") && (popTarget.find('.inv_pack_dynamic_transportation').length > 0)) {
            popTarget.find('.inv_pack_dynamic_transportation input[name="seatStatus"]').prop('checked', false);
        }
        popTarget.find('#trigger_pass_table').addClass('edit_pass_det');
        popTarget.modal('show');
        popTarget.attr('data-edit_row_index', curRowIndex);
    });
    $(document).on('click', '#inv_package_passenger_pop .edit_pass_det', function (e) {
        e.preventDefault();
        var curEdtBtn = $(this),
            curPopUp = curEdtBtn.parents('.modal'),
            curPassTable = $('#inv_package_paasenger_table'),
            edtRowIndex = parseInt(curPopUp.attr('data-edit_row_index')),
            passengerFields = {},
            curPassInvTotal = 0,
            curPassPurTotal = 0,
            curGeneralAmt = parseFloat(curPopUp.attr('data-general_amt')),
            curAdditionalExtra = parseFloat(curPopUp.attr('data-additional_extra')),
            curAdditionalDiscount = parseFloat(curPopUp.attr('data-additional_discount'));

        curPopUp.find('.pass_to_table').each(function () {
            var fieldName = $(this).attr('name'),
                fieldValue = $(this).val();
            passengerFields[fieldName] = fieldValue;
        });
        //console.log(passengerFields);
        curPassTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ')').empty();
        curPassTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ')').append('<td><span class="sl_no">' + (edtRowIndex + 1) + '</span></td>' +
            '<td>' + passengerFields.passengerName + '</td>' +
            '<td>' + passengerFields.age + '</td>' +
            '<td>' + passengerFields.passportNo + '</td>' +
            '<td></td>' +
            '<td></td>' +
            '<td></td>' +
            '<td></td>' +
            '<td></td>' +
            '<td></td>' +
            '<td>' +
            '<a class="show_table_lnk show_table_lnk_view" href="#">View</a>' +
            '<a class="show_table_lnk show_table_lnk_edit" href="#">Edit</a>' +
            '<a class="show_table_lnk show_table_lnk_del" href="#">Delete</a>' +
            '</td>');
        $.each(passengerFields, function (k, v) {
            curPassTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') td:first').append('<input type="hidden" data-field_name="' + k + '" name="pass' + (edtRowIndex + 1) + '_'+ k +'" value="' + v + '"/>');
        });
        if (curPopUp.find('.inv_pack_dynamic_airline').length > 0) {
            var airInvoiceTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_airline input[name="airInvoice"]').val()),
                airPurchaseTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_airline input[name="airPurchase"]').val());
            curPassTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') > td:nth-child(5)').text(curPopUp.find('.inv_pack_dynamic_airline input[name="ticketNo"]').val());
            curPassTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') > td:nth-child(6)').text(airInvoiceTotal);
            curPassInvTotal = curPassInvTotal + airInvoiceTotal;
            curPassPurTotal = curPassPurTotal + airPurchaseTotal;
        }
        if (curPopUp.find('.inv_pack_dynamic_hotel').length > 0) {
            var hotelInvoiceTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_hotel input[name="hotInvoice"]').val()),
                hotelPurchaseTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_hotel input[name="hotPurchase"]').val());
            curPassTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') > td:nth-child(7)').text(hotelInvoiceTotal);
            curPassInvTotal = curPassInvTotal + hotelInvoiceTotal;
            curPassPurTotal = curPassPurTotal + hotelPurchaseTotal;
            if (curPopUp.find('.inv_pack_dynamic_hotel input[type="checkbox"]').prop('checked') == true) {
                curPassTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ')').attr('data-hotel_room_status', "wb");
                curPassTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') td:nth-child(2)').append('<input type="hidden" name="pass' + (edtRowIndex + 1) + '_bedStatus" value="1" />');
            } else {
                curPassTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ')').attr('data-hotel_room_status', "wob");
                curPassTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') td:nth-child(2)').append('<input type="hidden" name="pass' + (edtRowIndex + 1) + '_bedStatus" value="0" />');
            }
        }
        if (curPopUp.find('.inv_pack_dynamic_visa').length > 0) {
            var visaInvoiceTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_visa input[name="visaInvoice"]').val()),
                visaPurchaseTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_visa input[name="visaPurchase"]').val());
            curPassTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') > td:nth-child(8)').text(visaInvoiceTotal);
            curPassInvTotal = curPassInvTotal + visaInvoiceTotal;
            curPassPurTotal = curPassPurTotal + visaPurchaseTotal;
        }
        if (curPopUp.find('.inv_pack_dynamic_transportation').length > 0) {
            var transInvoiceTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_transportation input[name="transInvoice"]').val()),
                transPurchaseTotal = parseFloat(curPopUp.find('.inv_pack_dynamic_transportation input[name="transPurchase"]').val());
            curPassTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') > td:nth-child(9)').text(transInvoiceTotal);
            curPassInvTotal = curPassInvTotal + transInvoiceTotal;
            curPassPurTotal = curPassPurTotal + transPurchaseTotal;
            if (curPopUp.find('.inv_pack_dynamic_transportation input[name="seatStatus"]').prop('checked') == true) {
                curPassTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ')').attr('data-seat_status', "on");
                curPassTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') td:nth-child(2)').append('<input type="hidden" name="pass' + (edtRowIndex + 1) + '_seatStatus" value="1" />');
            } else {
                curPassTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ')').attr('data-seat_status', "off");
                curPassTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') td:nth-child(2)').append('<input type="hidden" name="pass' + (edtRowIndex + 1) + '_seatStatus" value="0" />');
            }
        }
        curPassTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') > td:nth-child(2)').append('<input type="hidden" name="pass' + (edtRowIndex + 1) + '_invoiceTotal" value="' + (curPassInvTotal + curGeneralAmt) + '" />');
        curPassTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') > td:nth-child(2)').append('<input type="hidden" name="pass' + (edtRowIndex + 1) + '_purchaseTotal" value="' + curPassPurTotal + '" />');
        curPassTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ') > td:nth-child(10)').text(curPassInvTotal + curGeneralAmt);
        curPassTable.find('tbody > tr:nth-child(' + (edtRowIndex + 1) + ')').attr('data-additional_extra', curAdditionalExtra).attr('data-additional_discount', curAdditionalDiscount);
        getInvPackParrRowCount();
        netPackageInvoiceAmt();
        curPopUp.modal('hide');
    });

    // Delete action for passenger row in package invoice
    $(document).on('click', '#inv_package_paasenger_table > tbody > tr > td > a.show_table_lnk_del', function (e) {
        e.preventDefault();
        var curDelBtn = $(this),
            curRow = curDelBtn.parents('tr'),
            curRowIndex = curRow.index(),
            curTable = curDelBtn.parents('table'),
            curRowCount = curTable.find('tbody > tr').length,
            i;
        if ((curRowCount == 1) || ((curRowIndex + 1) == curRowCount)) {
            curRow.remove();
            getInvPackParrRowCount();
            netPackageInvoiceAmt();
        } else {
            curRow.remove();
            for (i = curRowIndex; i < curRowCount; i++) {
                curTable.find('tbody > tr:nth-child(' + i + ') input[type="hidden"]').each(function () {
                    var fieldArray = $(this).attr('name').split('_');
                    $(this).attr('name', 'pass' + i + '_' + fieldArray[1]);
                });
                curTable.find('tbody > tr:nth-child(' + i + ') > td:first > span.sl_no').text(i);
            }
            getInvPackParrRowCount();
            netPackageInvoiceAmt();
        }
    });
    // Triggering live calculations in on keyup in package invoice
    $(document).on('keyup', '#invoice_package input[name="tds"]', function () {
        netPackageInvoiceAmt();
    });
    $(document).on('keyup', '#invoice_package input[name="mainProcCharge"]', function () {
        netPackageInvoiceAmt();
    });
    $(document).on('keyup', '#invoice_package input[name="discount"]', function () {
        netPackageInvoiceAmt();
    });
    $(document).on('keyup', '#invoice_package input[name="mainTax"]', function () {
        netPackageInvoiceAmt();
    });
    $(document).on('keyup', '#invoice_package input[name="mainOtherCharge"]', function () {
        netPackageInvoiceAmt();
    });


    // Triggering age based calculation on change of age field
    $(document).on('focusout', '#inv_package_passenger_pop input[name="age"]', function () {
        var curAgeField = $(this);
        if (curAgeField.val() != "" && parseInt(curAgeField.val()) > 0) {
            ageBasedRateTrigger(parseInt(curAgeField.val()));
        }
    });
    $(document).on('change', '#inv_package_passenger_pop input[name="dob"]', function () {
        var dobField = $(this),
            dob = dobField.val(),
            today = new Date(),
            dd = today.getDate(),
            mm = today.getMonth() + 1,
            yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        today = dd + '-' + mm + '-' + yyyy;
        var dobSplit = dob.split('-'),
            todaySplit = today.split('-'),
            dobLast = new Date(dobSplit[2], +dobSplit[1] - 1, dobSplit[0]),
            todayLast = new Date(todaySplit[2], +todaySplit[1] - 1, todaySplit[0]),
            dateDiff = (todayLast.getTime() - dobLast.getTime()) / (1000 * 60 * 60 * 24),
            age = Math.round(dateDiff / 365);
        dobField.parents('.modal').find('input[name="age"]').val(age);
        ageBasedRateTrigger(age);
    });

    $('#inv_package_passenger_pop').on('hidden.bs.modal', function () {
        var invPackPop = $('#inv_package_passenger_pop');
        invPackPop.removeAttr('data-edit_row_index');
        invPackPop.find('#trigger_pass_table').addClass('inv_pack_pop_add');
        $('#inv_pack_pop_passenger_details > .pass_to_table').each(function () {
            $(this).val("").removeAttr('value');
        });
        invPackPop.find('.inv_pack_dynamic_airline input').val("").removeAttr('value');
        invPackPop.find('.inv_pack_dynamic_hotel input').val("").removeAttr('value');
        invPackPop.find('.pass_to_table').removeAttr('disabled');
        $(this).find('#trigger_pass_table').removeClass('add_to_pass_table clear_pass_pop edit_pass_det');
    });
    // Passing airline row count in package registeration
    function updateAirRowCountPackReg() {
        $('#pack_reg_air_row_count').val($('#package_reg_form .pack_dynamic_airline table > tbody > tr').length);
    }
    // Passing visa row count in package registeration
    function updateVisaRowCountPackReg() {
        $('#pack_reg_visa_row_count').val($('#package_reg_form .pack_dynamic_visa table > tbody > tr').length);
    }
    // Passing transportation row count in package registeration
    function updateTransportationRowCountPackReg() {
        $('#pack_reg_transportation_row_count').val($('#package_reg_form .pack_dynamic_transportation table > tbody > tr').length);
    }

    // Adding dynamic rows in airline table in package registeration
    $(document).on('click', '#package_reg_form .add_package_airline_reg_row', function (e) {
        e.preventDefault();
        var curAddBtn = $(this),
            curTable = curAddBtn.parents('table'),
            curRowLength = curTable.find('tbody > tr').length,
            appendTr = $('.package_dynamic_source .pack_dynamic_airline table > tbody').html();
        curTable.find('tbody').append(appendTr);
        curTable.find('tbody tr:last input').each(function () {
            var curNameArray = $(this).attr('name').split('_');
            $(this).attr('name', curNameArray[0] + '_' + (curRowLength + 1));
            if ($(this).attr('data-dp') == "date_pick") {
                $(this).addClass('user_date').datepicker({
                    dateFormat: "dd-mm-yy",
                    changeMonth: true,
                    changeYear: true
                });
            }
        });
        curTable.find('tbody tr:last select').each(function () {
            var curNameArray = $(this).attr('name').split('_');
            $(this).attr('name', curNameArray[0] + '_' + (curRowLength + 1));
        });
        curTable.find('tbody > tr:last > td:first').text(curRowLength + 1);

        updateAirRowCountPackReg();
    });
    // Deleting a row in airline table in package registeration form
    $(document).on('click', '#package_reg_form button[data-action="del_pack_reg_airline_row"]', function (e) {
        e.preventDefault();
        var curDelBtn = $(this),
            curRow = curDelBtn.parents('tr'),
            curRowIndex = curRow.index(),
            curTable = curDelBtn.parents('table'),
            curRowLength = curTable.find('tbody > tr').length,
            appendTr = $('.package_dynamic_source .pack_dynamic_airline table > tbody').html(),
            i;
        if (curRowLength == 1) {
            curRow.remove();
            curTable.find('tbody').append(appendTr);
            updateAirRowCountPackReg();
            curTable.find('tbody input').each(function () {
                if ($(this).attr('data-dp') == "date_pick") {
                    $(this).addClass('user_date').datepicker({
                        dateFormat: "dd-mm-yy",
                        changeMonth: true,
                        changeYear: true
                    });
                }
            });
        } else if ((curRowLength > 1) && ((curRowIndex + 1) == curRowLength)) {
            curRow.remove();
            updateAirRowCountPackReg();
        } else if ((curRowLength > 1) && ((curRowIndex + 1) != curRowLength)) {
            curRow.remove();
            for (i = curRowIndex; i < curRowLength; i++) {
                curTable.find('tbody tr:nth-child(' + i + ') input').each(function () {
                    var curNameArray = $(this).attr('name').split('_');
                    $(this).attr('name', curNameArray[0] + '_' + i);
                });
                curTable.find('tbody tr:nth-child(' + i + ') select').each(function () {
                    var curNameArray = $(this).attr('name').split('_');
                    $(this).attr('name', curNameArray[0] + '_' + i);
                });
                curTable.find('tbody tr:nth-child(' + i + ') td:first').text(i);
            }
            updateAirRowCountPackReg();
        }
    });

    // Passing airline row count in package registeration
    function updateHotelRowCountPackReg() {
        $('#pack_reg_hotel_row_count').val($('#package_reg_form .pack_dynamic_hotel table > tbody:first > tr').length);
    }

    // Adding dynamic rows in hotel room table in package registerartion
    $(document).on('click', '#package_reg_form .add_package_hotel_reg_row', function (e) {
        e.preventDefault();
        var curAddBtn = $(this),
            curTable = curAddBtn.parents('table'),
            curRowLength = curTable.find('tbody:first > tr').length,
            appendTr = $('.package_dynamic_source .pack_dynamic_hotel table.hotel_outer_table > tbody').html();
        curTable.find('tbody:first').append(appendTr);
        curTable.find('tbody:first > tr:last input').each(function () {
            var curField = $(this),
                curNameArray = curField.attr('name').split('_');
            if (curNameArray.length == 2) {
                curField.attr('name', 'hot' + (curRowLength + 1) + '_' + curNameArray[1]);
            } else if (curNameArray.length == 3) {
                curField.attr('name', 'hot' + (curRowLength + 1) + '_' + curNameArray[1] + '_' + curNameArray[2]);
            }
            if (curField.attr('data-dp') == "date_pick") {
                curField.addClass('user_date');
                curField.datepicker({
                    dateFormat: "dd-mm-yy",
                    changeMonth: true,
                    changeYear: true
                });
            }
        });
        curTable.find('tbody:first > tr:last select').each(function () {
            var curNameArray = $(this).attr('name').split('_');
            if (curNameArray.length == 2) {
                $(this).attr('name', 'hot' + (curRowLength + 1) + '_' + curNameArray[1]);
            } else if (curNameArray.length == 3) {
                $(this).attr('name', 'hot' + (curRowLength + 1) + '_' + curNameArray[1] + '_' + curNameArray[2]);
            }
        });
        curTable.find('tbody:first > tr:last > td:first').text(curRowLength + 1);

        updateHotelRowCountPackReg();
    });
    // Deleting a row in hotel room table in package registration form
    $(document).on('click', '#package_reg_form button[data-action="del_pack_reg_hotel_row"]', function (e) {
        e.preventDefault();
        var curDelBtn = $(this),
            curRow = curDelBtn.parents('tr'),
            curRowIndex = curRow.index(),
            curTable = curDelBtn.parents('table'),
            curRowLength = curTable.find('tbody:first > tr').length,
            appendTr = $('.package_dynamic_source .pack_dynamic_hotel table.hotel_outer_table > tbody').html(),
            i;
        if (curRowLength == 1) {
            curRow.remove();
            curTable.find('tbody').append(appendTr);
            netCahsPaymentAmt()
            updateHotelRowCountPackReg();
        } else if ((curRowLength > 1) && ((curRowIndex + 1) == curRowLength)) {
            curRow.remove();
            updateHotelRowCountPackReg();
        } else if ((curRowLength > 1) && ((curRowIndex + 1) != curRowLength)) {
            curRow.remove();
            for (i = curRowIndex; i < curRowLength; i++) {
                curTable.find('tbody:first > tr:nth-child(' + i + ') input').each(function () {
                    var curNameArray = $(this).attr('name').split('_');
                    if (curNameArray.length == 2) {
                        $(this).attr('name', 'hot' + i + '_' + curNameArray[1]);
                    } else if (curNameArray.length == 3) {
                        $(this).attr('name', 'hot' + i + '_' + curNameArray[1] + '_' + curNameArray[2]);
                    }
                });
                curTable.find('tbody:first > tr:nth-child(' + i + ') select').each(function () {
                    var curNameArray = $(this).attr('name').split('_');
                    if (curNameArray.length == 2) {
                        $(this).attr('name', 'hot' + i + '_' + curNameArray[1]);
                    } else if (curNameArray.length == 3) {
                        $(this).attr('name', 'hot' + i + '_' + curNameArray[1] + '_' + curNameArray[2]);
                    }
                });
                curTable.find('tbody:first > tr:nth-child(' + i + ') td:first').text(i);
            }
            updateHotelRowCountPackReg();
        }
    });
    // Calculating number of days in hotel table in package registration
    function getNoDays (trIndex) {
        var curTable = $('#package_dynamic .pack_dynamic_hotel .hotel_outer_table'),
            curTr = curTable.find('tbody:first > tr:nth-child('+ (trIndex + 1) +')'),
            curInField = curTr.find('input[name="hot'+ (trIndex + 1) +'_hotelcheckinDate"]'),
            curOutField = curTr.find('input[name="hot'+ (trIndex + 1) +'_hotelcheckoutDate"]'),
            curNoDayField = curTr.find('input[name="hot'+ (trIndex + 1) +'_noOfDays"]');
        if (curInField.val() && curOutField.val()) {
            var curInDate = curInField.val().split('-'),
                curOutDate = curOutField.val().split('-'),
                inDate = new Date(curInDate[2], curInDate[1], curInDate[0]),
                outDate = new Date(curOutDate[2], curOutDate[1], curOutDate[0]),
                oneDay = 24 * 60 * 60 * 1000;
            if (outDate > inDate) {
                var noDays = Math.round(Math.abs((inDate.getTime() - outDate.getTime())/(oneDay)));
                curNoDayField.val(noDays);
            } else if (outDate.getTime() === inDate.getTime()) {
                curNoDayField.val(1);
            } else if (outDate < inDate) {
                triggerError("error", "Please select valid dates!");
                curOutField.val("");
            }
        }
    }
    $(document).on('change', '#package_dynamic .pack_dynamic_hotel .hotel_outer_table input[data-dp="date_pick"]', function () {
        var curTr = $(this).parents('tr'),
            curTrIndex = curTr.index();
        getNoDays(curTrIndex);
    })

    // Adding dynamic rows in visa table in package registration
    $(document).on('click', '#package_reg_form .add_package_visa_reg_row', function (e) {
        e.preventDefault();
        var curAddBtn = $(this),
            curTable = curAddBtn.parents('table'),
            curRowLength = curTable.find('tbody > tr').length,
            appendTr = $('.package_dynamic_source .pack_dynamic_visa table > tbody').html();
        curTable.find('tbody').append(appendTr);
        curTable.find('tbody tr:last input').each(function () {
            var curNameArray = $(this).attr('name').split('_');
            $(this).attr('name', curNameArray[0] + '_' + (curRowLength + 1));
        });
        curTable.find('tbody tr:last select').each(function () {
            var curNameArray = $(this).attr('name').split('_');
            $(this).attr('name', curNameArray[0] + '_' + (curRowLength + 1));
        });
        curTable.find('tbody > tr:last > td:first').text(curRowLength + 1);

        updateVisaRowCountPackReg();
    });
    // Deleting a row in visa table in package registration form
    $(document).on('click', '#package_reg_form button[data-action="del_pack_reg_visa_row"]', function (e) {
        e.preventDefault();
        var curDelBtn = $(this),
            curRow = curDelBtn.parents('tr'),
            curRowIndex = curRow.index(),
            curTable = curDelBtn.parents('table'),
            curRowLength = curTable.find('tbody > tr').length,
            appendTr = $('.package_dynamic_source .pack_dynamic_visa table > tbody').html(),
            i;
        if (curRowLength == 1) {
            curRow.remove();
            curTable.find('tbody').append(appendTr);
            updateVisaRowCountPackReg();
        } else if ((curRowLength > 1) && ((curRowIndex + 1) == curRowLength)) {
            curRow.remove();
            updateVisaRowCountPackReg();
        } else if ((curRowLength > 1) && ((curRowIndex + 1) != curRowLength)) {
            curRow.remove();
            for (i = curRowIndex; i < curRowLength; i++) {
                curTable.find('tbody tr:nth-child(' + i + ') input').each(function () {
                    var curNameArray = $(this).attr('name').split('_');
                    $(this).attr('name', curNameArray[0] + '_' + i);
                });
                curTable.find('tbody tr:nth-child(' + i + ') select').each(function () {
                    var curNameArray = $(this).attr('name').split('_');
                    $(this).attr('name', curNameArray[0] + '_' + i);
                });
                curTable.find('tbody tr:nth-child(' + i + ') td:first').text(i);
            }
            updateVisaRowCountPackReg();
        }
    });


    // Adding dynamic rows in transportation table in package registration
    $(document).on('click', '#package_reg_form .add_package_transportation_reg_row', function (e) {
        e.preventDefault();
        var curAddBtn = $(this),
            curTable = curAddBtn.parents('table'),
            curRowLength = curTable.find('tbody > tr').length,
            appendTr = $('.package_dynamic_source .pack_dynamic_transportation table > tbody').html();
        curTable.find('tbody').append(appendTr);
        curTable.find('tbody tr:last input').each(function () {
            var curNameArray = $(this).attr('name').split('_');
            $(this).attr('name', curNameArray[0] + '_' + (curRowLength + 1));
        });
        curTable.find('tbody tr:last select').each(function () {
            var curNameArray = $(this).attr('name').split('_');
            $(this).attr('name', curNameArray[0] + '_' + (curRowLength + 1));
        });
        curTable.find('tbody > tr:last > td:first').text(curRowLength + 1);

        updateTransportationRowCountPackReg();
    });
    // Deleting a row in transportation table in package registration form
    $(document).on('click', '#package_reg_form button[data-action="del_pack_reg_transportation_row"]', function (e) {
        e.preventDefault();
        var curDelBtn = $(this),
            curRow = curDelBtn.parents('tr'),
            curRowIndex = curRow.index(),
            curTable = curDelBtn.parents('table'),
            curRowLength = curTable.find('tbody > tr').length,
            appendTr = $('.package_dynamic_source .pack_dynamic_transportation table > tbody').html(),
            i;
        if (curRowLength == 1) {
            curRow.remove();
            curTable.find('tbody').append(appendTr);
            updateTransportationRowCountPackReg();
        } else if ((curRowLength > 1) && ((curRowIndex + 1) == curRowLength)) {
            curRow.remove();
            updateTransportationRowCountPackReg();
        } else if ((curRowLength > 1) && ((curRowIndex + 1) != curRowLength)) {
            curRow.remove();
            for (i = curRowIndex; i < curRowLength; i++) {
                curTable.find('tbody tr:nth-child(' + i + ') input').each(function () {
                    var curNameArray = $(this).attr('name').split('_');
                    $(this).attr('name', curNameArray[0] + '_' + i);
                });
                curTable.find('tbody tr:nth-child(' + i + ') select').each(function () {
                    var curNameArray = $(this).attr('name').split('_');
                    $(this).attr('name', curNameArray[0] + '_' + i);
                });
                curTable.find('tbody tr:nth-child(' + i + ') td:first').text(i);
            }
            updateTransportationRowCountPackReg();
        }
    });
    $(document).on('keyup input', '#cash_payment #tbl_cash_payment tbody > tr > td input[data-action="trigger_cash_calculation"]', function () {
        netCahsPaymentAmt();
    });
    // Live bill info for cash payment
    $(document).on('click', '#cash_payment #get_bill_details', function (e) {
        e.preventDefault();
        var curBillBtn = $(this),
            curForm = curBillBtn.parents('form#cash_payment'),
            curAcTo = curForm.find('input[name="accountTo"]'),
            curPop = $(curBillBtn.attr('data-target')),
            flag = false;
        if (curAcTo.val().length == 0 || isNaN(parseInt(curAcTo.val()))) {
            flag = true;
            curAcTo.nextAll('p.validation_mes').addClass('active');
        }
        if (flag == true) {
            triggerError("error", "Please provide valid details");
        } else {
            var supplierID = curAcTo.val()
            $.ajax({
                type: 'POST',
                url: '../../services/cash_payment_live.php',
                data: {
                    apiKey: "api123",
                    supID: supplierID
                },
                success: function (cashPaymentResult) {
                    if (cashPaymentResult.status == "ok" && cashPaymentResult.bill_count > 0) {
                        var i;
                        curPop.find('#bill_list tbody').empty();
                        $.each(cashPaymentResult.bill_list, function (k, v){
                            curPop.find('#bill_list tbody').append('<tr>'+
                                '<td class="text-center">'+ '<input type="checkbox"></td>'+
                                '<td>'+ v.inv_prefix + v.invoice_no +'</td>'+
                                '<td>'+ v.invoice_date +'</td>'+
                                '<td>'+ v.inv_type +'</td>'+
                                '<td>'+ v.details +'</td>'+
                                '<td>'+ v.net_amount +'</td>'+
                                '<td>'+ v.paid_amount +'</td>'+
                                '<td>'+ (v.net_amount - v.paid_amount)  +'</td>'+
                                '</tr>');
                        });
                        curPop.modal('show');
                    } else if (cashPaymentResult.status == "ok" && cashPaymentResult.bill_count == 0) {
                        triggerError("error", "No bills found unpaid.")
                    }
                },
                error: function (error) {
                    triggerError("error", "Please provide valid details");
                }
            });
        }

    });
    $(document).on('keyup', '#cash_payment #accountToName', function() {
        if ($(this).val() != "") {
            $(this).nextAll('p.validation_mes').removeClass('active');
        }
    });
    // Passing details of selected bills in cash payment
    $(document).on('click', '#bill_list_pop #trigger_cash_payment', function () {
        if ($(this).parents('#bill_list_pop').find('tbody input[type="checkbox"]:checked').length == 0) {
            triggerError("error", "Please select atleast one bill to pay.");
        } else {
            var curPop = $(this).parents('#bill_list_pop'),
                curPopTable = curPop.find('table'),
                curCashTable = $('#cash_payment #tbl_cash_payment');
            curCashTable.find('tbody').empty();
            curPopTable.find('tbody > tr').each(function () {
                var curPaymentRow = $(this);
                if (curPaymentRow.find('td:first input[type="checkbox"]').prop('checked') == true) {
                    curCashTable.find('tbody').append('<tr>'+
                        '<td></td>'+
                        '<td><input type="text" name="invoiceNo" data-rel="bill_live" value="'+ curPaymentRow.find('td:nth-child(2)').text() +'" readonly></td>'+
                        '<td><input type="text" data-dp="datepick" name="invoiceDate" value="'+ curPaymentRow.find('td:nth-child(3)').text() +'" readonly></td>'+
                        '<td><input type="text" name="invoiceType" value="'+ curPaymentRow.find('td:nth-child(4)').text() +'" readonly></td>'+
                        '<td><input type="text" name="total" value="'+ curPaymentRow.find('td:nth-child(6)').text() +'" readonly></td>'+
                        '<td><input type="text" name="totalPaid" value="'+ curPaymentRow.find('td:nth-child(7)').text() +'" readonly></td>'+
                        '<td><input type="text" data-action="trigger_cash_calculation" name="payingAmount" value="'+ curPaymentRow.find('td:nth-child(8)').text() +'"></td>'+
                        '<td><input type="text" data-action="trigger_cash_calculation" name="discount" value="0"></td>'+
                        '<td><textarea name="remark"></textarea></td>'+
                        '<td><button class="bd_btn bd_btn_red" type="button" data-action="del_cash_payment_row">Delete</button></td>'+
                        '</tr>');
                }
            });
            curCashTable.find('tbody > tr').each(function () {
                $(this).find('td:first').text($(this).index() + 1);
                $(this).find('input[type="text"]').each(function () {
                    $(this).attr('name', $(this).attr('name')+'_'+ ($(this).parents('tr').index() + 1));
                });
                $(this).find('textarea').each(function () {
                    $(this).attr('name', $(this).attr('name')+'_'+ ($(this).parents('tr').index() + 1));
                });
            });
            netCahsPaymentAmt();
            cashPaymentRowCountUpdator();
            curPop.modal('hide');
        }
    });

    // Live search for accTo in cash payment
    $(document).on('keyup', '.aj_account_to input[type="text"]', function (e) {
        var curSearchBox = $(this),
            curForm = curSearchBox.closest('form'),
            curSearchKey = curSearchBox.val(),
            curLiveList = curSearchBox.siblings('ul.aj_live_items'),
            curKey = e.keyCode,
            curLiveSelected = curLiveList.find('li').filter('.selected'),
            current;
        if (curKey != 40 && curKey != 38 && curKey != 13 && curSearchKey.length > 0) {
            $.ajax({
                type: 'POST',
                url: '../../services/account_to_live.php',
                data: {
                    apiKey: "api123",
                    searchKey: curSearchKey
                },
                success: function (result) {
                    if (result.status == "ok") {
                        curLiveList.empty();
                        $.each(result.acc_details, function(k, v){
                            curLiveList.append('<li data-parent_id="'+v.id+'" data-acc_name="'+ v.acc_name +'">'+ v.acc_name+'<span class="pass_mobile"><span></span>'+ v.id +'</span></li>');
                        });
                    } else if (result.status == "failure") {
                        curLiveList.empty();
                    }
                }
            });
        } else if (curSearchKey.length == 0) {
            curLiveList.empty();
        } else if (curKey == 40) {
            curLiveList.find('li').removeClass('selected');
            if (!curLiveSelected.length || curLiveSelected.is(':last-child')) {
                current = curLiveList.find('li').eq(0);
            } else {
                current = curLiveSelected.next();
            }
            $(current).addClass('selected');
        } else if (curKey == 38) {
            curLiveList.find('li').removeClass('selected');
            if (!curLiveSelected.length || curLiveSelected.is(':first-child')) {
                current = curLiveList.find('li').last();
            } else {
                current = curLiveSelected.prev();
            }
            $(current).addClass('selected');
        } else if (curKey == 13) {
            var curPassenger = curLiveList.find('li:selected').attr('data-acc_name'),
                parentId = curLiveList.find('li.selected').attr('data-parent_id');
            //curSearchBox.val(curPassenger);
            //curSearchBox.siblings('input[name="accountTo"]').val(parentId);
            curForm.find('#accountToName').val(curPassenger);
            curForm.find('#accountTo').val(parentId);
            curLiveList.empty();
        }
    });
    $(document).on('click', '.aj_account_to .aj_live_items li', function () {
        var curAccTo = $(this),
            curAccName = curAccTo.attr('data-acc_name'),
            curAccId = curAccTo.attr('data-parent_id'),
            curForm = curAccTo.closest('form');
        //curAccTo.parents('.aj_live_items').siblings('input#accountToName').val(curAccName);
        //curAccTo.parents('.aj_live_items').siblings('input[name="accountTo"]').val(curAccId);
        curForm.find('#accountToName').val(curAccName);
        curForm.find('#accountTo').val(curAccId);
        curAccTo.parents('.aj_live_items').empty();
    });
    $(document).on('focusout', '.aj_rel_box input[type="text"]', function () {
        $(this).siblings('.aj_live_items').delay(300).fadeOut();
    });
    $(document).on('focus', '.aj_rel_box input[type="text"]', function () {
        $(this).siblings('.aj_live_items').show();
    });





    // Updating row count in bank payment table
    function bankPaymentRowCountUpdator() {
        var curForm = $('#bank_payment'),
            curTable = curForm.find('#tbl_bank_payment');
        curForm.find('input#bank_payment_row_count').val(curTable.find('tbody > tr').length);
    }
    // Live bill info for bank payment
    $(document).on('click', '#bank_payment #get_bill_details', function (e) {
        e.preventDefault();
        var curBillBtn = $(this),
            curForm = curBillBtn.parents('form#bank_payment'),
            curAcTo = curForm.find('input[name="accountTo"]'),
            curPop = $(curBillBtn.attr('data-target')),
            flag = false;
        if (curAcTo.val().length == 0 || isNaN(parseInt(curAcTo.val()))) {
            flag = true;
            curAcTo.nextAll('p.validation_mes').addClass('active');
        }
        if (flag == true) {
            triggerError("error", "Please provide valid details");
        } else {
            var supplierID = curAcTo.val()
            $.ajax({
                type: 'POST',
                url: '../../services/bank_payment_live.php',
                data: {
                    apiKey: "api123",
                    supID: supplierID
                },
                success: function (bankPaymentResult) {
                    if (bankPaymentResult.status == "ok" && bankPaymentResult.bill_count > 0) {
                        var i;
                        curPop.find('#bill_list tbody').empty();
                        $.each(bankPaymentResult.bill_list, function (k, v){
                            curPop.find('#bill_list tbody').append('<tr>'+
                                '<td class="text-center">'+ '<input type="checkbox"></td>'+
                                '<td>'+ v.inv_prefix + v.invoice_no +'</td>'+
                                '<td>'+ v.invoice_date +'</td>'+
                                '<td>'+ v.inv_type +'</td>'+
                                '<td>'+ v.details +'</td>'+
                                '<td>'+ v.net_amount +'</td>'+
                                '<td>'+ v.paid_amount +'</td>'+
                                '<td>'+ (v.net_amount - v.paid_amount)  +'</td>'+
                                '</tr>');
                        });
                        curPop.modal('show');
                    } else if (bankPaymentResult.status == "ok" && bankPaymentResult.bill_count == 0) {
                        triggerError("error", "No bills found unpaid.")
                    }
                },
                error: function (error) {
                    triggerError("error", "Please provide valid details");
                }
            });
        }

    });
    // Passing details of selected bills in bank payment
    $(document).on('click', '#bill_list_pop #trigger_bank_payment', function () {
        if ($(this).parents('#bill_list_pop').find('tbody input[type="checkbox"]:checked').length == 0) {
            triggerError("error", "Please select atleast one bill to pay.");
        } else {
            var curPop = $(this).parents('#bill_list_pop'),
                curPopTable = curPop.find('table'),
                curCashTable = $('#bank_payment #tbl_bank_payment');
            curCashTable.find('tbody').empty();
            curPopTable.find('tbody > tr').each(function () {
                var curPaymentRow = $(this);
                if (curPaymentRow.find('td:first input[type="checkbox"]').prop('checked') == true) {
                    curCashTable.find('tbody').append('<tr>'+
                        '<td></td>'+
                        '<td><input type="text" name="invoiceNo" data-rel="bill_live" value="'+ curPaymentRow.find('td:nth-child(2)').text() +'" readonly></td>'+
                        '<td><input type="text" data-dp="datepick" name="invoiceDate" value="'+ curPaymentRow.find('td:nth-child(3)').text() +'" readonly></td>'+
                        '<td><input type="text"name="invoiceType" value="'+ curPaymentRow.find('td:nth-child(4)').text() +'" readonly></td>'+
                        '<td><input type="text" name="total" value="'+ curPaymentRow.find('td:nth-child(6)').text() +'" readonly></td>'+
                        '<td><input type="text" name="totalPaid" value="'+ curPaymentRow.find('td:nth-child(7)').text() +'" readonly></td>'+
                        '<td><input type="text" data-action="trigger_cash_calculation" name="payingAmount" value="'+ curPaymentRow.find('td:nth-child(8)').text() +'"></td>'+
                        '<td><input type="text" data-action="trigger_cash_calculation" name="discount" value="0"></td>'+
                        '<td><textarea name="remark"></textarea></td>'+
                        '<td><button class="bd_btn bd_btn_red" type="button" data-action="del_cash_receipt_row">Delete</button></td>'+
                        '</tr>');
                }
            });
            curCashTable.find('tbody > tr').each(function () {
                $(this).find('td:first').text($(this).index() + 1);
                $(this).find('input[type="text"]').each(function () {
                    $(this).attr('name', $(this).attr('name')+'_'+ ($(this).parents('tr').index() + 1));
                });
                $(this).find('textarea').each(function () {
                    $(this).attr('name', $(this).attr('name')+'_'+ ($(this).parents('tr').index() + 1));
                });
            });
            netBankPaymentAmt();
            bankPaymentRowCountUpdator();
            curPop.modal('hide');
        }
    });
    // Updating row count in bank receipt table
    function bankReceiptRowCountUpdator() {
        var curForm = $('#bank_receipt'),
            curTable = curForm.find('#tbl_bank_receipt');
        curForm.find('input#bank_receipt_row_count').val(curTable.find('tbody > tr').length);
    }
    // Live bill info for bank receipt
    $(document).on('click', '#bank_receipt #get_bill_details', function (e) {
        e.preventDefault();
        var curBillBtn = $(this),
            curForm = curBillBtn.parents('form#bank_receipt'),
            curAcFrom = curForm.find('input[name="accountFrom"]'),
            curPop = $(curBillBtn.attr('data-target')),
            flag = false;
        if (curAcFrom.val().length == 0 || isNaN(parseInt(curAcFrom.val()))) {
            flag = true;
            curAcFrom.nextAll('p.validation_mes').addClass('active');
        }
        if (flag == true) {
            triggerError("error", "Please provide valid details!");
        } else {
            var customerID = curAcFrom.val();
            $.ajax({
                type: 'POST',
                url: '../../services/bank_receipt_live.php',
                data: {
                    apiKey: "api123",
                    custID: customerID
                },
                success: function (bankReceiptResult) {
                    console.log(bankReceiptResult);
                    if (bankReceiptResult.status == "ok" && bankReceiptResult.bill_count > 0) {
                        var i;
                        curPop.find('#bill_list tbody').empty();
                        $.each(bankReceiptResult.bill_list, function (k, v){
                            curPop.find('#bill_list tbody').append('<tr>'+
                                '<td class="text-center">'+ '<input type="checkbox"></td>'+
                                '<td>'+ v.inv_prefix + v.invoice_no +'</td>'+
                                '<td>'+ v.invoice_date +'</td>'+
                                '<td>'+ v.inv_type +'</td>'+
                                '<td>'+ v.details +'</td>'+
                                '<td>'+ v.net_amount +'</td>'+
                                '<td>'+ v.paid_amount +'</td>'+
                                '<td>'+ (v.net_amount - v.paid_amount)  +'</td>'+
                                '</tr>');
                        });
                        curPop.modal('show');
                    } else if (bankReceiptResult.status == "ok" && bankReceiptResult.bill_count == 0) {
                        triggerError("error", "No bills found unpaid.")
                    }
                },
                error: function (error) {
                    triggerError("error", "Please provide valid details");
                }
            });
        }

    });
    // Passing details of selected bills in bank receipt
    $(document).on('click', '#bill_list_pop #trigger_bank_receipt', function () {
        if ($(this).parents('#bill_list_pop').find('tbody input[type="checkbox"]:checked').length == 0) {
            triggerError("error", "Please select atleast one bill to pay.");
        } else {
            var curPop = $(this).parents('#bill_list_pop'),
                curPopTable = curPop.find('table'),
                curCashTable = $('#bank_receipt #tbl_bank_receipt');
            curCashTable.find('tbody').empty();
            curPopTable.find('tbody > tr').each(function () {
                var curPaymentRow = $(this);
                if (curPaymentRow.find('td:first input[type="checkbox"]').prop('checked') == true) {
                    curCashTable.find('tbody').append('<tr>'+
                        '<td></td>'+
                        '<td><input type="text" name="invoiceNo" data-rel="bill_live" value="'+ curPaymentRow.find('td:nth-child(2)').text() +'" readonly></td>'+
                        '<td><input type="text" data-dp="datepick" name="invoiceDate" value="'+ curPaymentRow.find('td:nth-child(3)').text() +'" readonly></td>'+
                        '<td><input type="text"name="invoiceType" value="'+ curPaymentRow.find('td:nth-child(4)').text() +'" readonly></td>'+
                        '<td><input type="text" name="total" value="'+ curPaymentRow.find('td:nth-child(6)').text() +'" readonly></td>'+
                        '<td><input type="text" name="totalPaid" value="'+ curPaymentRow.find('td:nth-child(7)').text() +'" readonly></td>'+
                        '<td><input type="text" data-action="trigger_cash_calculation" name="payingAmount" value="'+ curPaymentRow.find('td:nth-child(8)').text() +'"></td>'+
                        '<td><input type="text" data-action="trigger_cash_calculation" name="discount" value="0"></td>'+
                        '<td><textarea name="remark"></textarea></td>'+
                        '<td><button class="bd_btn bd_btn_red" type="button" data-action="del_cash_receipt_row">Delete</button></td>'+
                        '</tr>');
                }
            });
            curCashTable.find('tbody > tr').each(function () {
                $(this).find('td:first').text($(this).index() + 1);
                $(this).find('input[type="text"]').each(function () {
                    $(this).attr('name', $(this).attr('name')+'_'+ ($(this).parents('tr').index() + 1));
                });
                $(this).find('textarea').each(function () {
                    $(this).attr('name', $(this).attr('name')+'_'+ ($(this).parents('tr').index() + 1));
                });
            });
            netBankReceiptAmt();
            bankReceiptRowCountUpdator();
            curPop.modal('hide');
        }
    });


    $(document).on('keyup input', '#bank_payment #tbl_bank_payment tbody > tr > td input[data-action="trigger_cash_calculation"]', function () {
        netBankPaymentAmt();
    });
    $(document).on('keyup input', '#bank_receipt #tbl_bank_receipt tbody > tr > td input[data-action="trigger_cash_calculation"]', function () {
        netBankReceiptAmt();
    });

    // Getting passenger details in visa invoice
    $(document).on('click', '#get_visa_passengers', function (e) {
        e.preventDefault();
        var curBtn = $(this),
            curPop = $(curBtn.attr('data-modal_target')),
            curForm = curBtn.parents('form'),
            curCustField = curForm.find('input#customerId'),
            flag = false;
        if (!curCustField.val() && isNaN(parseInt(curCustField.val()))) {
            flag = true;
        }
        if (flag == true) {
            triggerError("error", "Please select a customer");
        } else {
            $.ajax({
                type: 'POST',
                url: '../../services/visa_passenger_list.php',
                data: {
                    apiKey: "api123",
                    parentID: curCustField.val()
                },
                success: function (result) {
                    if (result.status == "ok") {
                        curPop.find('#visa_passenger_check_list > tbody').empty();
                        $.each(result.passenger_details, function (k, v) {
                            if ($('#visa_passenger_table > tbody > tr[data-pass_id="' + v.id + '"]').length == 0) {
                                curPop.find('#visa_passenger_check_list > tbody').append('<tr data-pass_id="' + v.id + '">' +
                                    '<td><input type="checkbox"></td>' +
                                    '<td>' + v.name + '</td>' +
                                    '<td>' + v.dob + '</td>' +
                                    '<td>' + v.gender + '</td>' +
                                    '<td>' + v.passport_no + '</td>' +
                                    '</tr>');
                            }
                        });
                        curPop.modal('show');
                    } else if (result.status == "failure") {
                        triggerError("error", result.error);
                    }
                },
                error: function (error) {
                    triggerError("error", "Some thing went wrong. Please try again!");
                }
            });
        }
    });
    // Passing passenger details to visa passenger table in visa invoice
    $(document).on('click', '#trigger_visa_passenger', function (e) {
        e.preventDefault();
        var curBtn = $(this),
            curPop = curBtn.parents('.modal'),
            curTable = curPop.find('#visa_passenger_check_list'),
            curVisaTable = $('#visa_passenger_table'),
            curTableRowLength = curVisaTable.find('tbody > tr').length;
        if (curTable.find('tbody input[type="checkbox"]:checked').length == 0) {
            triggerError("error", "Please select atleast one passenger");
        } else {
            //curVisaTable.find('tbody').empty();
            var passingRowCount = curTable.find('tbody input[type="checkbox"]:checked').length;
            curTable.find('tbody > tr').each(function () {
                var curPopRow = $(this);
                if (curPopRow.find('td:first > input[type="checkbox"]').prop('checked') == true) {
                    curVisaTable.find('tbody').append('<tr data-pass_id="' + curPopRow.attr('data-pass_id') + '">' +
                        '<td></td>' +
                        '<td><input type="text" name="passengerName" readonly value="'+ curPopRow.find('td:nth-child(2)').text() +'"></td>' +
                        '<td><input type="text" name="dob" readonly value="'+ curPopRow.find('td:nth-child(3)').text() +'"></td>' +
                        '<td><input type="text" name="gender" readonly value="'+ curPopRow.find('td:nth-child(4)').text() +'"></td>' +
                        '<td><input type="text" name="passportNo" readonly value="'+ curPopRow.find('td:nth-child(5)').text() +'"></td>' +
                        '<td><button class="bd_btn bd_btn_red" type="button" data-action="del_visa_pass_row">Delete</button></td>' +
                        '</tr>');
                }
            });
            for (var i = (curTableRowLength + 1); i <= (curTableRowLength + passingRowCount); i++) {
                curVisaTable.find('tbody > tr:nth-child(' + i + ') > td:first').text(i);
                curVisaTable.find('tbody > tr:nth-child(' + i + ') input[type="text"]').each(function () {
                    $(this).attr('name', 'pass' + (i) + '_' + $(this).attr('name'));
                });
            }
            /*curVisaTable.find('tbody > tr').each(function () {
             var curRowindex = $(this).index();
             $(this).find('td:first').text(curRowindex + 1);
             $(this).find('input[type="text"]').each(function () {
             $(this).attr('name', 'pass' + (curRowindex + 1) + '_' + $(this).attr('name'));
             });
             });*/
            curPop.modal('hide');
            getVisaPassengerRowCount();
        }
    });
    // Deleting passenger row in visa invoice
    $(document).on('click', '#visa_passenger_table > tbody > tr button[data-action="del_visa_pass_row"]', function (e) {
        e.preventDefault();
        var curPassDelBtn = $(this),
            curRow = curPassDelBtn.parents('tr'),
            curRowIndex = curRow.index(),
            trCount = $('#visa_passenger_table > tbody > tr').length;
        if ((trCount == 1) || ((trCount - 1) == curRowIndex)) {
            curRow.remove();
            getVisaPassengerRowCount();
        } else {
            curRow.remove();
            var newTrCount = trCount - 1;
            var i;
            for (i = curRowIndex; i <= (newTrCount - 1); i++) {
                var hiddenFieldList = {},
                    slNo = parseInt($('#visa_passenger_table > tbody > tr:nth-child(' + (i + 1) + ') > td:first').text()) - 1;
                $('#visa_passenger_table > tbody > tr:nth-child(' + (i + 1) + ') > td > input[type="text"]').each(function () {
                    var fieldNameArray = $(this).attr('name').split('_');
                    $(this).attr('name', 'pass' + (i + 1) + '_' + fieldNameArray[1]);
                });
                $('#visa_passenger_table > tbody > tr:nth-child(' + (i + 1) + ') > td > select').each(function () {
                    var fieldNameArray = $(this).attr('name').split('_');
                    $(this).attr('name', 'pass' + (i + 1) + '_' + fieldNameArray[1]);
                });
                $('#visa_passenger_table > tbody > tr:nth-child(' + (i + 1) + ') > td:first').text(slNo);
            }
            getVisaPassengerRowCount();
        }
    });

    // Adding manual rows in passenger table in visa invoice
    $(document).on('click', '#add_manulal_row_visa', function (e) {
        e.preventDefault();
        var btn = $(this),
            targetTable = $('#visa_passenger_table'),
            curRowCount = targetTable.find('tbody > tr').length;
        targetTable.find('tbody').append('<tr>' +
            '<td>' + (curRowCount + 1) + '</td>' +
            '<td><input type="text" name="pass' + (curRowCount + 1) + '_passengerName"></td>' +
            '<td><input type="text" name="pass' + (curRowCount + 1) + '_dob" data-dp="date_pick"></td>' +
            '<td>' +
            '<select name="pass' + (curRowCount + 1) + '_gender">' +
            '<option value="M" selected>M</option>' +
            '<option value="F">F</option>' +
            '</select>' +
            '</td>' +
            '<td><input type="text" name="pass' + (curRowCount + 1) + '_passportNo"></td>' +
            '<td><button class="bd_btn bd_btn_red" type="button" data-action="del_visa_pass_row">Delete</button></td>' +
            '</tr>');
        targetTable.find('tbody > tr:nth-child(' + (curRowCount + 1) + ') > td input[data-dp="date_pick"]').datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true
        });
        getVisaPassengerRowCount();
    });



    // Live calculation for airline section in package creation
    $(document).on('keyup input', '#package_reg_form .pack_dynamic_airline input[data-cal="air_live_cal"]', function () {
        var airlineDyn = $(this).closest('.pack_dynamic_airline'),
            adultFare = 0,
            childFare = 0,
            infantFare = 0,
            yq = 0,
            procCharge = 0,
            taxText = airlineDyn.find('input[name="airTax"]').val(),
            adultTotal = 0,
            childTotal = 0,
            infantTotal = 0;
        if (airlineDyn.find('input[name="adultChargeAir"]').val() && !isNaN(parseFloat(airlineDyn.find('input[name="adultChargeAir"]').val()))) {
            adultFare = parseFloat(airlineDyn.find('input[name="adultChargeAir"]').val());
        }
        if (airlineDyn.find('input[name="childChargeAir"]').val() && !isNaN(parseFloat(airlineDyn.find('input[name="childChargeAir"]').val()))) {
            childFare = parseFloat(airlineDyn.find('input[name="childChargeAir"]').val());
        }
        if (airlineDyn.find('input[name="infantChargeAir"]').val() && !isNaN(parseFloat(airlineDyn.find('input[name="infantChargeAir"]').val()))) {
            infantFare = parseFloat(airlineDyn.find('input[name="infantChargeAir"]').val());
        }
        if (airlineDyn.find('input[name="airYq"]').val() && !isNaN(parseFloat(airlineDyn.find('input[name="airYq"]').val()))) {
            yq = parseFloat(airlineDyn.find('input[name="airYq"]').val());
        }
        if (airlineDyn.find('input[name="airProCharge"]').val() && !isNaN(parseFloat(airlineDyn.find('input[name="airProCharge"]').val()))) {
            procCharge = parseFloat(airlineDyn.find('input[name="airProCharge"]').val());
        }
        var adultWithoutTax = adultFare + yq + procCharge;
        var childWithoutTax = childFare + yq + procCharge;
        var infantWithoutTax = infantFare + yq + procCharge;
        if (taxText.toString().length > 1 && taxText.toString().indexOf('%') != -1) {
            var taxTextLength = taxText.length,
                taxPerc = taxText.toString().substr(0, (taxTextLength - 1));
            adultTotal = adultWithoutTax + ((parseFloat(taxPerc) / 100) * adultWithoutTax);
            childTotal = childWithoutTax + ((parseFloat(taxPerc) / 100) * childWithoutTax);
            infantTotal = infantWithoutTax + ((parseFloat(taxPerc) / 100) * infantWithoutTax);
        } else if (taxText.length > 0 && !isNaN(parseFloat(taxText))) {
            adultTotal = adultWithoutTax + parseFloat(taxText);
            childTotal = childWithoutTax + parseFloat(taxText);
            infantTotal = infantWithoutTax + parseFloat(taxText);
        } else if (taxText.length == 0) {
            adultTotal = adultWithoutTax;
            childTotal = childWithoutTax;
            infantTotal = infantWithoutTax;
        }
        airlineDyn.find('input[name="adultTotalAir"]').val(adultTotal);
        airlineDyn.find('input[name="childTotalAir"]').val(childTotal);
        airlineDyn.find('input[name="infantTotalAir"]').val(infantTotal);
        netPackageCreationAmt();
    });

    // Live calculation for hotel room type in package registration
    $(document).on('keyup input', '#package_reg_form .pack_dynamic_hotel table.hotel_inner_table input[data-cal="room_live_cal"]', function () {
        var curTr = $(this).closest('tr'),
            curTrIndex = curTr.index(),
            curCharge = 0,
            curOtherCharge = 0,
            curTotal = 0;
        if (curTr.find('input[data-cal_field="charge"]').val() && !isNaN(parseFloat(curTr.find('input[data-cal_field="charge"]').val()))) {
            curCharge = parseFloat(curTr.find('input[data-cal_field="charge"]').val());
        }
        if (curTr.find('input[data-cal_field="other_charge"]').val() && !isNaN(parseFloat(curTr.find('input[data-cal_field="other_charge"]').val()))) {
            curOtherCharge = parseFloat(curTr.find('input[data-cal_field="other_charge"]').val());
        }
        curTr.find('input[data-cal_field="room_total"]').val(curCharge + curOtherCharge);
        netPackageCreationAmt();
    });

    // Live calculation for visa table in package creation
    $(document).on('keyup input', '#package_reg_form .pack_dynamic_visa table input[data-cal="visa_live_cal"]', function () {
        var curTr = $(this).closest('tr'),
            curTrIndex = curTr.index(),
            visaFee = 0,
            vfs = 0,
            ddCharge = 0,
            serviceCharge = 0,
            otherCharge = 0,
            courierCharge = 0,
            total = 0;
        if (curTr.find('input[name="visaFee_'+ (curTrIndex + 1) +'"]').val() && !isNaN(parseFloat(curTr.find('input[name="visaFee_'+ (curTrIndex + 1) +'"]').val()))) {
            visaFee = parseFloat(curTr.find('input[name="visaFee_'+ (curTrIndex + 1) +'"]').val());
        }
        if (curTr.find('input[name="vfs_'+ (curTrIndex + 1) +'"]').val() && !isNaN(parseFloat(curTr.find('input[name="vfs_'+ (curTrIndex + 1) +'"]').val()))) {
            vfs = parseFloat(curTr.find('input[name="vfs_'+ (curTrIndex + 1) +'"]').val());
        }
        if (curTr.find('input[name="ddCharge_'+ (curTrIndex + 1) +'"]').val() && !isNaN(parseFloat(curTr.find('input[name="ddCharge_'+ (curTrIndex + 1) +'"]').val()))) {
            ddCharge = parseFloat(curTr.find('input[name="ddCharge_'+ (curTrIndex + 1) +'"]').val());
        }
        if (curTr.find('input[name="serviceCharge_'+ (curTrIndex + 1) +'"]').val() && !isNaN(parseFloat(curTr.find('input[name="serviceCharge_'+ (curTrIndex + 1) +'"]').val()))) {
            serviceCharge = parseFloat(curTr.find('input[name="serviceCharge_'+ (curTrIndex + 1) +'"]').val());
        }
        if (curTr.find('input[name="visaOtherCharge_'+ (curTrIndex + 1) +'"]').val() && !isNaN(parseFloat(curTr.find('input[name="visaOtherCharge_'+ (curTrIndex + 1) +'"]').val()))) {
            otherCharge = parseFloat(curTr.find('input[name="visaOtherCharge_'+ (curTrIndex + 1) +'"]').val());
        }
        if (curTr.find('input[name="courierCharge_'+ (curTrIndex + 1) +'"]').val() && !isNaN(parseFloat(curTr.find('input[name="courierCharge_'+ (curTrIndex + 1) +'"]').val()))) {
            courierCharge = parseFloat(curTr.find('input[name="courierCharge_'+ (curTrIndex + 1) +'"]').val());
        }
        total = visaFee + vfs + ddCharge + serviceCharge + otherCharge + courierCharge;
        curTr.find('input[name="visaTotal_'+ (curTrIndex + 1) +'"]').val(total);
        netPackageCreationAmt();
    });
    $(document).on('keyup input', '#package_reg_form input[data-cal="package_reg_live_cal"]', function () {
        netPackageCreationAmt();
    });

    // Redirect to update do page from package status
    $(document).on('click', '#package_status_table > tbody > tr > td a[data-action="update_passenger"]', function (e) {
        e.preventDefault();
        var curBtn = $(this),
            curRow = curBtn.closest('tr'),
            curPackageId = curRow.find('input[data-field="package_id"]').val(),
            curPassengerId = curRow.find('input[data-field="passenger_id"]').val(),
            curTicketNo = "",
            curSubDate = "",
            curCollDate = "",
            curDisDate = "";
        if (curRow.find('input[data-field="ticketNo"]').val()) {
            curTicketNo = curRow.find('input[data-field="ticketNo"]').val();
        }
        if (curRow.find('input[data-field="subDate"]').val()) {
            curSubDate = curRow.find('input[data-field="subDate"]').val();
        }
        if (curRow.find('input[data-field="collDate"]').val()) {
            curCollDate = curRow.find('input[data-field="collDate"]').val();
        }
        if (curRow.find('input[data-field="disDate"]').val()) {
            curDisDate = curRow.find('input[data-field="disDate	"]').val();
        }
        window.location.replace('package_do.php?packageId=' + curPackageId + '&id=' + curPassengerId + '&ticketNo=' + curTicketNo + '&subDate=' + curSubDate + '&collDate=' + curCollDate + '&disDate=' + curDisDate+'&op=edit');
    });

    // Live search for agent in account creation
    $(document).on('keyup input', '.aj_agent input[type="text"]', function (e) {
        var searchBox = $(this),
            searchKey = searchBox.val(),
            form = searchBox.closest('form'),
            agentNameField = form.find('#agentName'),
            agendIdField = form.find('#agentId'),
            liveList = searchBox.siblings('.aj_live_items');
        if (searchKey) {
            $.ajax({
                type: 'POST',
                url: '../../services/agent_live.php',
                data: {
                    keyWord: searchKey
                },
                success: function (result) {
                    if (result.status == "ok") {
                        liveList.empty();
                        $.each(result.agent_list, function(k, v) {
                            liveList.append('<li data-agent_id="' + v.id + '" data-agent_name="'+ v.name + '">' + v.name + '<span class="pass_mobile"><span></span>' + v.id + '</span></li>');
                        });
                    } else {
                        liveList.empty();
                    }
                },
                failure: function (error) {
                    liveList.empty();
                }
            });
        } else {
            liveList.empty();
        }
    });
    $(document).on('click', '.aj_agent .aj_live_items > li', function () {
        var li = $(this),
            id = li.attr('data-agent_id'),
            name = li.attr('data-agent_name'),
            form = li.closest('form');
        form.find('#agentName').val(name);
        form.find('#agentId').val(id);
        li.parents('.aj_live_items').empty();
    });

    // Passing account details to form in account report
    $(document).on('click', '#acc_pop #pass_to_report', function () {
        var passBtn = $(this),
            popUp = passBtn.parents('.modal'),
            target = $('#search_report #accountGroupIds');
        if (popUp.find('table > tbody input[type="checkbox"]:checked').length == 0) {
            triggerError("error", "Please select atleast one account!");
        } else {
            var idArray = [];
            popUp.find('table > tbody > tr').each(function () {
                var tr = $(this);
                if (tr.find('input[type="checkbox"]').prop('checked') == true) {
                    idArray.push(tr.find('input[type="checkbox"]').attr('data-acc_id'));
                }
            });
            target.val(idArray.toString());
            popUp.modal('hide');
        }
    });

    // Live search in account report pop up table
    function quickSearch(field, table) {
        var searchBox = $(field),
            filterTable = $(table),
            keyWord = searchBox.val();
        if (keyWord) {
            filterTable.find('tbody > tr').each(function () {
                var tr = $(this);
                if (tr.find('td:last').text().search(new RegExp(keyWord, "i")) < 0) {
                    tr.hide();
                } else {
                    tr.show();
                }
            });
        } else {
            filterTable.find('tbody > tr').show();
        }
    }
    $(document).on('keyup input', '#account_report_search', function () {
        quickSearch('#account_report_search', '#acc_report_check_table');
    });

    // check all in account report table
    function checkAll (check, table) {
        var checkBox = $(check),
            checkTable = $(table);
        if (checkBox.prop('checked') == true) {
            checkTable.find('input[type="checkbox"]').prop('checked', true);
        } else {
            checkTable.find('input[type="checkbox"]').prop('checked', false);
        }
    }

    $(document).on('click', '#acc_report_check_table #check_all', function () {
        var checkBox = $('#acc_report_check_table #check_all'),
            checkTable = $('#acc_report_check_table');
        if (checkBox.prop('checked') == true) {
            checkTable.find('tbody > tr:visible input[type="checkbox"]').prop('checked', true);
        } else {
            checkTable.find('input[type="checkbox"]').prop('checked', false);
        }
    });

    // Print trigger
    $(document).on('click', '#print_form_trigger', function () {
        $('#print_form').submit();
    });

    /*$(document).on('click', '#acc_report_check_table > tbody > tr > td > input[type="checkbox"]', function () {
     var curCheck = $(this),
     checkBox = $('#acc_report_check_table #check_all'),
     checkTable = $('#acc_report_check_table'),
     trCount = checkTable.find('tbody > tr').length,
     checkCount = checkTable.find('input[type="checkbox"]:checked').length;
     if (curCheck.prop('checked') == false) {
     checkBox.prop('checked', false);
     } else if ((curCheck.prop('checked') == true) && (checkCount == trCount)) {
     checkBox.prop('checked', true);
     }
     });*/

    // getting accounts in account report
    $(document).on('change', '#company_select', function () {
        var compSelect = $(this),
            agentSelect = $('#agent_select');
        if (compSelect.val()) {
            var compId = compSelect.val();
            $.ajax({
                type: 'POST',
                url: '../../services/agent_under_company.php',
                data: {
                    company: compId
                },
                success: function (result) {
                    agentSelect.empty();
                    if (result.status == "ok") {
                        agentSelect.append('<option value="">Select Agent</option>');
                        $.each(result.agents, function (k, v){
                            agentSelect.append('<option value="' + v.id + '">' + v.accName + '</option>');
                        });
                    } else {
                        agentSelect.append('<option value="">Select Agent</option>');
                    }
                }
            });
        }
    });
    $(document).on('click', '#get_acc_details', function (e) {
        e.preventDefault();
        var acBtn = $(this),
            userType = acBtn.attr('data-travel_type'),
            compSelect = $('#company_select'),
            agSelect = $('#agent_select'),
            params = {user_type: userType};
        if (userType == "Admin" && !compSelect.val()) {
            triggerError("error", "Please select a company!");
        } else if (userType == "Admin" && compSelect.val()) {
            params.company = compSelect.val();
            if (agSelect.val()) {
                params.agent = agSelect.val();
            }
        } else if (userType == "Branch") {
            params.company = acBtn.attr('data-comp_id');
            if (agSelect.val()) {
                params.agent = agSelect.val();
            }
        } else if (userType == "Staff") {
            if (agSelect.val()) {
                params.agent = agSelect.val();
            }
        }
        $.ajax({
            type: 'POST',
            url: '../../services/accounts_for_account_report.php',
            data: params,
            success: function (result) {
                if (result.status == "ok") {
                    var popTarget = $(acBtn.attr('data-target')),
                        tableTarget = popTarget.find('#acc_report_check_table');
                    tableTarget.find('tbody').empty();
                    $.each(result.acc_list, function (k, v) {
                        tableTarget.find('tbody').append('<tr>' +
                            '<td><input type="checkbox" data-acc_id="' + v.id + '" data-acc_name="' + v.acc_name + '"></td>' +
                            '<td>' + v.id + '</td>' +
                            '<td>' + v.acc_name + '</td>' +
                            '</tr>')
                    });
                    popTarget.modal('show');
                } else if (result.status == "failure") {
                    triggerError("error", "No data found!");
                }
            }
        });
    });


});